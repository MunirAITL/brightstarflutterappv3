package com.mortgagemagic.ui.taskpostbywebview.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Message
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.webkit.*
import android.widget.Toast
import com.mortgagemagic.R
import com.mortgagemagic.data.managers.TaskManager
import com.mortgagemagic.data.network.MortgageCasePaymentInfo
import com.mortgagemagic.data.network.Task
import com.mortgagemagic.data.preferences.AppPreferenceHelper
import com.mortgagemagic.ui.base.view.BaseActivity
import com.mortgagemagic.ui.main.view.MainActivity
import com.mortgagemagic.ui.taskattachment.view.TaskAttachmentActivity
import com.mortgagemagic.ui.taskpostbywebview.interactor.TaskPostByWebViewMVPInteractor
import com.mortgagemagic.ui.taskpostbywebview.presenter.TaskPostByWebViewPresenter
import com.mortgagemagic.util.AppConstants
import com.mortgagemagic.util.CommonUtil
import com.mortgagemagic.util.DateUtil
import kotlinx.android.synthetic.main.activity_task_post_by_webview.*
import kotlinx.android.synthetic.main.app_bar_navigation.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


/**
 * Created by jyotidubey on 13/01/18.
 */
class TaskPostByWebViewActivity : BaseActivity(), TaskPostByWebViewMVPView, View.OnClickListener {

    @Inject
    internal lateinit var presenter: TaskPostByWebViewPresenter<TaskPostByWebViewMVPView, TaskPostByWebViewMVPInteractor>


    internal lateinit var layoutManager: LinearLayoutManager
    lateinit var appPreferenceHelper: AppPreferenceHelper


    // Start

    /*-- CUSTOMIZE --*/
    /*-- you can customize these options for your convenience --*/
    private val webview_url = "file:///android_res/raw/index.html"    // web address or local file location you want to open in webview
    private val file_type = "image/*"    // file types to be allowed for upload
    private val multiple_files = true         // allowing multiple file upload

    /*-- MAIN VARIABLES --*/
    //internal var webView: WebView? = null

    private val TAG = "TaskPostByWebView"

    private var cam_file_data: String? = null        // for storing camera file information
    private var file_data: ValueCallback<Uri>? = null       // data/header received after file selection
    private var file_path: ValueCallback<Array<Uri>>? = null     // received file(s) temp. location

    private val file_req_code = 1

    //End


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        setContentView(R.layout.activity_task_post_by_webview)
        presenter.onAttach(this)
        layoutManager = LinearLayoutManager(this)

        appPreferenceHelper = AppPreferenceHelper(this!!, AppConstants.PREF_NAME)

        val cookieManager = CookieManager.getInstance()
        cookieManager.setAcceptCookie(true)
        cookieManager.setCookie("https://mortgage-magic.co.uk", appPreferenceHelper.getAccessCookie())

        cookieManager.setAcceptThirdPartyCookies(webviewTaskPost, true)


        //   webviewTaskPost.getSettings().setJavaScriptEnabled(true) // enable javascript

        showProgress()



        webviewTaskPost.settings.javaScriptEnabled = true
        webviewTaskPost.setWebViewClient(object : WebViewClient() {

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                super.onReceivedError(view, request, error)
                hideProgress()
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url)
                hideProgress()
                return false // then it is not handled by default action
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                hideProgress()
            }

            override fun onFormResubmission(view: WebView?, dontResend: Message?, resend: Message?) {
                super.onFormResubmission(view, dontResend, resend)
            }


//            override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
//                Log.d("MyApplication", consoleMessage.message() + " -- From line "
//                        + consoleMessage.lineNumber() + " of "
//                        + consoleMessage.sourceId())
//                return super.onConsoleMessage(consoleMessage)
//            }
//            fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
//                //Toast.makeText(activity, description, Toast.LENGTH_SHORT).show()
//            }
//
//            @TargetApi(android.os.Build.VERSION_CODES.M)
//            fun onReceivedError(view: WebView, req: WebResourceRequest, rerr: WebResourceError) {
//                // Redirect to deprecated method, so you can use it in all SDK versions
//                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString())
//            }
        })


        val taskId = intent.getLongExtra("PUSHNOTIFICATION_TASKID", 0)

        if (taskId > 0) {

            TaskManager.task = Task()

            TaskManager.task!!.Id = taskId
            TaskManager.task!!.Title = intent.getStringExtra("PUSHNOTIFICATION_MESSAGE")
            TaskManager.task!!.TaskTitleUrl = intent.getStringExtra("PUSHNOTIFICATION_WEBURL")

            // buy-to-let-remortgage-70142
//            if (TaskManager.task!!.TaskTitleUrl != null) {
//                var startIndex = TaskManager.task!!.TaskTitleUrl!!.indexOf("tasks/")
//                TaskManager.task!!.TaskTitleUrl = TaskManager.task!!.TaskTitleUrl!!.substring(startIndex + 5) // buy-to-let-remortgage-70142
//            }

        }

        if (TaskManager.task != null && TaskManager.task!!.Id > 0) {

            var title = TaskManager.task!!.Title
            if (title == "Commercial Mortgages/Loans") {
                title = "Commercial Mortgages/Loans"
            }
            var url = "https://mortgage-magic.co.uk/apps/about-me/" + title + "-" + TaskManager.task!!.Id
            webviewTaskPost.loadUrl(url)

        } else {
            webviewTaskPost.loadUrl("https://mortgage-magic.co.uk/apps/about-me/")
        }
        //setContentView(webviewTaskPost)

        val ws = webviewTaskPost.settings
        ws.javaScriptEnabled = true
        ws.allowFileAccess = true
        ws.allowFileAccess = true

        webviewTaskPost.addJavascriptInterface(object : Any() {
            @JavascriptInterface           // For API 17+
            fun performClick(parameter: String) {

                if (parameter == "1") {
                    submittedCaseApplication()
                } else if (parameter == "2") {
                    showMyTaskList()
                }
                //Toast.makeText(this@MainActivity, strl, Toast.LENGTH_SHORT).show()
            }
        }, "ok")


//        webView.setWebChromeClient(new WebChromeClient() {
//            private String TAG;
//            // For Android 3.0+
//            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
//                mUploadMessage = uploadMsg;
//                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                i.addCategory(Intent.CATEGORY_OPENABLE);
//                i.setType("image/*");
//                startActivityForResult(Intent.createChooser(i, "File Chooser"),
//                        RESULT_CODE_ICE_CREAM);
//
//            }
//            // For Android 3.0+
//            public void openFileChooser(ValueCallback uploadMsg, String acceptType) {
//                mUploadMessage = uploadMsg;
//                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                i.addCategory(Intent.CATEGORY_OPENABLE);
//                i.setType("*/*");
//                startActivityForResult(Intent.createChooser(i, "File Browser"),
//                        RESULT_CODE_ICE_CREAM);
//            }
//            //For Android 4.1
//            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
//                mUploadMessage = uploadMsg;
//                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//                i.addCategory(Intent.CATEGORY_OPENABLE);
//                i.setType("image/*");
//                startActivityForResult(Intent.createChooser(i, "File Chooser"),
//                        RESULT_CODE_ICE_CREAM);
//
//            }
//            //For Android5.0+
//            public boolean onShowFileChooser(
//                    WebView webView, ValueCallback<Uri[]> filePathCallback,
//                    FileChooserParams fileChooserParams) {
//                if (mFilePathCallback != null) {
//                    mFilePathCallback.onReceiveValue(null);
//                }
//                mFilePathCallback = filePathCallback;
//                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                if (takePictureIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
//                    // Create the File where the photo should go
//                    File photoFile = null;
//                    try {
//                        photoFile = createImageFile();
//                        takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
//                    } catch (IOException ex) {
//                        // Error occurred while creating the File
//                        Log.e(TAG, "Unable to create Image File", ex);
//                    }
//                    // Continue only if the File was successfully created
//                    if (photoFile != null) {
//                        mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
//                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                                Uri.fromFile(photoFile));
//                    } else {
//                        takePictureIntent = null;
//                    }
//                }
//                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
//                contentSelectionIntent.setType("image/*");
//                Intent[] intentArray;
//                if (takePictureIntent != null) {
//                    intentArray = new Intent[]{takePictureIntent};
//                } else {
//                    intentArray = new Intent[0];
//                }
//                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
//                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
//                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
//                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
//
//                startActivityForResult(chooserIntent, REQUEST_CODE_LOLIPOP);
//
//                return true;
//            }
//        });
//

        webviewTaskPost.setWebChromeClient(object : WebChromeClient() {

            /*--
            openFileChooser is not a public Android API and has never been part of the SDK.
            handling input[type="file"] requests for android API 16+; I've removed support below API 21 as it was failing to work along with latest APIs.
            --*/
            /*    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                file_data = uploadMsg;
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
                i.setType(file_type);
                if (multiple_files) {
                    i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }
                startActivityForResult(Intent.createChooser(i, "File Chooser"), file_req_code);
            }
        */
            /*-- handling input[type="file"] requests for android API 21+ --*/
            override fun onShowFileChooser(webView: WebView, filePathCallback: ValueCallback<Array<Uri>>, fileChooserParams: WebChromeClient.FileChooserParams): Boolean {

                if (file_permission() && Build.VERSION.SDK_INT >= 21) {
                    file_path = filePathCallback
                    var takePictureIntent: Intent? = null
                    var takeVideoIntent: Intent? = null

                    var includeVideo = false
                    var includePhoto = false

                    /*-- checking the accept parameter to determine which intent(s) to include --*/
                    paramCheck@ for (acceptTypes in fileChooserParams.acceptTypes) {
                        val splitTypes = acceptTypes.split(", ?+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray() // although it's an array, it still seems to be the whole value; split it out into chunks so that we can detect multiple values
                        for (acceptType in splitTypes) {
                            when (acceptType) {
                                "*/*" -> {
                                    includePhoto = true
                                    includeVideo = true
                                    break@paramCheck
                                }
                                "image/*" -> includePhoto = true
                                "video/*" -> includeVideo = true
                            }
                        }
                    }

                    if (fileChooserParams.acceptTypes.size == 0) {   //no `accept` parameter was specified, allow both photo and video
                        includePhoto = true
                        includeVideo = true
                    }

                    if (includePhoto) {
                        takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (takePictureIntent.resolveActivity(this@TaskPostByWebViewActivity.getPackageManager()) != null) {
                            var photoFile: File? = null
                            try {
                                photoFile = create_image()
                                takePictureIntent.putExtra("PhotoPath", cam_file_data)
                            } catch (ex: IOException) {
                                Log.e(TAG, "Image file creation failed", ex)
                            }

                            if (photoFile != null) {
                                cam_file_data = "file:" + photoFile.absolutePath
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                            } else {
                                cam_file_data = null
                                takePictureIntent = null
                            }
                        }
                    }

                    if (includeVideo) {
                        takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
                        if (takeVideoIntent.resolveActivity(this@TaskPostByWebViewActivity.getPackageManager()) != null) {
                            var videoFile: File? = null
                            try {
                                videoFile = create_video()
                            } catch (ex: IOException) {
                                Log.e(TAG, "Video file creation failed", ex)
                            }

                            if (videoFile != null) {
                                cam_file_data = "file:" + videoFile.absolutePath
                                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile))
                            } else {
                                cam_file_data = null
                                takeVideoIntent = null
                            }
                        }
                    }

                    val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                    contentSelectionIntent.type = file_type
                    if (multiple_files) {
                        contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                    }

                    var intentArray: Array<Intent>? = null
                    if (takePictureIntent != null && takeVideoIntent != null) {
                        intentArray = arrayOf(takePictureIntent, takeVideoIntent)
                    } else if (takePictureIntent != null) {
                        intentArray = arrayOf(takePictureIntent)
                    } else if (takeVideoIntent != null) {
                        intentArray = arrayOf(takeVideoIntent)
                    } else {
                        //intentArray = ()
                    }

                    val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                    chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                    chooserIntent.putExtra(Intent.EXTRA_TITLE, "File chooser")
                    if (intentArray != null && intentArray.count() > 0) {
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                    }
                    startActivityForResult(chooserIntent, file_req_code)
                    return true
                } else {
                    return false
                }
            }
        })

    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    override fun onStart() {
        super.onStart()
        appPreferenceHelper = AppPreferenceHelper(this, AppConstants.PREF_NAME)
        initializeWidgets()
        fillTaskInformation()
    }

    private fun initializeWidgets() {


    }


    private fun fillTaskInformation() {
    }


    override fun onClick(v: View?) {

    }

    override fun setUpNavigation() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.new_case) + " - " + TaskManager.task?.Title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material)
        toolbar.setNavigationOnClickListener {
            showMyTaskList()
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base))
    }

//    override fun showWebViewActivity(task: Task) {
//        TaskManager.task = task
//        val intent = Intent(this.context, TaskPostByWebViewActivity::class.java)
//        startActivity(intent)
//    }

    private fun submittedCaseApplication() {

        var mortgageCasePaymentInfo = MortgageCasePaymentInfo()


        mortgageCasePaymentInfo.Id = 0
        mortgageCasePaymentInfo.UserId = appPreferenceHelper.getCurrentUserId()!!
        mortgageCasePaymentInfo.Status = AppConstants.TASK_STATUS_ACTIVE
        mortgageCasePaymentInfo.TaskId = TaskManager.task!!.Id
        mortgageCasePaymentInfo.CreationDate = DateUtil.getCurrentDate()
        mortgageCasePaymentInfo.UpdatedDate = DateUtil.getCurrentDate()
        mortgageCasePaymentInfo.VersionNumber = 0
        mortgageCasePaymentInfo.DefineSolution = ""
        mortgageCasePaymentInfo.LenderProvider = ""
        mortgageCasePaymentInfo.Term = ""
        mortgageCasePaymentInfo.LoanAmount = 0.0
        mortgageCasePaymentInfo.Rate = 0.0
        mortgageCasePaymentInfo.Value = 0.0
        mortgageCasePaymentInfo.FixedPeriodEnds = ""
        mortgageCasePaymentInfo.ProcFees = 0.0
        mortgageCasePaymentInfo.AdviceFee = 0.0
        mortgageCasePaymentInfo.IntroducerFeeShare = 0.0
        mortgageCasePaymentInfo.IntroducerPaymentMade = ""
        mortgageCasePaymentInfo.ReasonForRecommendation = ""
        mortgageCasePaymentInfo.AnyOtherComments = ""
        mortgageCasePaymentInfo.Remarks = ""
        mortgageCasePaymentInfo.MonthlyPaymentAmount = 0.0
        mortgageCasePaymentInfo.OtherChargeAmount = 0.0
        mortgageCasePaymentInfo.RentalIncomeAmount = 0.0
        mortgageCasePaymentInfo.ValuationDate = ""
        mortgageCasePaymentInfo.FileUrl = ""


        val onClickListenerConfirm = View.OnClickListener {
            presenter.onMortgageCasePaymentInfo(mortgageCasePaymentInfo)
        }

        val onClickListenerCancel = View.OnClickListener {
            // presenter.onMortgageCasePaymentInfo(mortgageCasePaymentInfo)
        }

        CommonUtil.showCaseSubmissionDialog(this, onClickListenerConfirm, onClickListenerCancel)

    }

    override fun showMyTaskList() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("tab_position", 1)
        startActivity(intent)
        finish()
    }


    override fun onBackPressed() {
        showMyTaskList()

    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.post_case_menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        val id = item!!.itemId
//
//        if (id == R.id.action_attach) {
//            openTaskAttachmentActivity()
//        }
//
////        else if (id == R.id.action_discard) {
////            showDeleteDialog()
////        } else if (id == R.id.action_next) {
////            showNextPage()
////        }
//
//        return super.onOptionsItemSelected(item)
//    }

    private fun openTaskAttachmentActivity() {
        val intent = Intent(this, TaskAttachmentActivity::class.java)
        startActivity(intent)
    }


    // File Uplaod
    /*-- callback reporting if error occurs --*/
    inner class Callback : WebViewClient() {
        override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
            Toast.makeText(applicationContext, "Failed loading app!", Toast.LENGTH_SHORT).show()
        }
    }

    /*-- checking and asking for required file permissions --*/
    fun file_permission(): Boolean {
        if (Build.VERSION.SDK_INT >= 23 && (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), 1)
            return false
        } else {
            return true
        }
    }

    /*-- creating new image file here --*/
    @Throws(IOException::class)
    private fun create_image(): File {
        @SuppressLint("SimpleDateFormat") val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "img_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".jpg", storageDir)
    }

    /*-- creating new video file here --*/
    @Throws(IOException::class)
    private fun create_video(): File {
        @SuppressLint("SimpleDateFormat")
        val file_name = SimpleDateFormat("yyyy_mm_ss").format(Date())
        val new_name = "file_" + file_name + "_"
        val sd_directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(new_name, ".3gp", sd_directory)
    }

    /*-- back/down key handling --*/
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (webviewTaskPost.canGoBack()) {
                    webviewTaskPost.goBack()
                } else {
                    finish()
                }
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (Build.VERSION.SDK_INT >= 21) {
            var results: Array<Uri?>? = null

            /*-- if file request cancelled; exited camera. we need to send null value to make future attempts workable --*/
            if (resultCode == Activity.RESULT_CANCELED) {
                if (requestCode == file_req_code) {
                    file_path?.onReceiveValue(null)
                    return
                }
            }

            /*-- continue if response is positive --*/
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == file_req_code) {
                    if (null == file_path) {
                        return
                    }

                    var clipData: ClipData?
                    var stringData: String?
                    try {
                        clipData = intent!!.clipData
                        stringData = intent.dataString
                    } catch (e: Exception) {
                        clipData = null
                        stringData = null
                    }

                    if (clipData == null && stringData == null && cam_file_data != null) {
                        results = arrayOf(Uri.parse(cam_file_data))
                    } else {
                        if (clipData != null) { // checking if multiple files selected or not
                            val numSelectedFiles = clipData.itemCount

                            // results : Array<Uri> = arrayOfNulls<Uri>(numSelectedFiles)
                            results = arrayOfNulls<Uri>(numSelectedFiles) // IntArray<Uri>(numSelectedFiles)  //arrayOfNulls<Uri>(numSelectedFiles)
                            for (i in 0 until clipData.itemCount) {
                                results[i] = clipData.getItemAt(i).uri
                            }
                        } else {
                            results = arrayOf(Uri.parse(stringData))
                        }
                    }
                }
            }
            file_path?.onReceiveValue(results as Array<Uri>)
            file_path = null
        } else {
            if (requestCode == file_req_code) {
                if (null == file_data) return
                val result = if (intent == null || resultCode != RESULT_OK) null else intent.data
                file_data?.onReceiveValue(result)
                file_data = null
            }
        }
    }
}