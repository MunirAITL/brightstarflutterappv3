import 'package:flutter/material.dart';

class Server {
  static const bool isTest = false;
  static const bool isOtp = true; // if you want otp skip than set to false

  //  BaseUrl
  static const String BASE_URL = (isTest)
      ? "https://brightstar.mortgage-magic.co.uk"
      : "https://brightstar.mortgage-magic.co.uk";

  static const String MISSING_IMG =
      BASE_URL + "/api/content/media/default_avatar.png";

  //  Device Info & APNS Stuff
  static const String FCM_DEVICE_INFO_URL =
      BASE_URL + "/api/fcmdeviceinfo/post";

  //  sendemailandnotification
  static const String CASE_EMAI_NOTI_GET_URL =
      BASE_URL + "/api/task/sendemailandnotification/#caseId#";

  static const String MORTGAGE_CASEINFO_LOCATIONTASKUSERID_GET = BASE_URL +
      "/api/mortgagecaseinfo/getmortgagecaseinfowithlocationbytaskidanduserid?TaskId=#caseId#&UserId=#userId#";

  //  Company Account
  static const String COMP_ACC_GET_URL =
      BASE_URL + "/api/users/get/getcompanyaccountsbyuserid?UserId=#userId#";

  //  Login
  static const String LOGIN_URL = BASE_URL + "/api/authentication/login";

  //  Login by Mobile OTP
  static const String LOGIN_MOBILE_OTP_POST_URL =
      BASE_URL + "/api/userotp/post";
  static const String SEND_OTP_NOTI_URL =
      BASE_URL + "/api/userotp/sendotpnotification?otpId=#otpId#";
  static const String LOGIN_MOBILE_OTP_PUT_URL = BASE_URL + "/api/userotp/put";
  static const String LOGIN_REG_OTP_FB_URL =
      BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Login by FB Native

  //  Login by Gmail Native

  //  Login by Apple Native

  // Change password
  static const String CHANGE_PWD_URL =
      BASE_URL + "/api/users/put/change-password";

  //  Forgot
  static const String FORGOT_URL =
      BASE_URL + "/api/authentication/forgotpassword";

  //  Register
  static const String REG_URL = BASE_URL + "/api/authentication/register";
  static const REG_USER_CASE_APPLICANT_LEAD_URL =
      BASE_URL + "/api/users/register";

  //  Deactivate Profile
  static const String DEACTIVATE_PROFILE_URL =
      BASE_URL + "/api/users/deactivebyuserownerbyreason";

  //  Dashboard Action Alert and Cases
  static const String USERNOTEBYENTITY_URL = BASE_URL +
      "/api/usernote/getusernotebyentityidandentitynameanduseridandstatus?EntityId=#entityId#&EntityName=#entityName#&Status=#status#&UserId=#userId#";

  //  User Note Popup Put on Done
  static const String USERNOTE_PUT_URL = BASE_URL + "/api/usernote/put";

  //  New Case
  /*static const String NEWCASE_URL = BASE_URL +
      "/api/task/taskinformationbysearch" +
      "/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#";
*/
  //  New Case
  static const String NEWCASE_URL = BASE_URL +
      "/api/task/taskinformationbysearchformobileapplication/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#UserCompanyId#";

  //  Create Case
  static const String POSTCASE_URL = BASE_URL + "/api/task/post";

  static const SENDCASEEMAILNOTIFY4BSBROKER_URL = BASE_URL +
      "/api/taskbidding/sendemailandnotificationselfcompletedadviserassignedandnonassignedforbrighstarbroker?taskId=#caseId#";

  static const GET_CASE_URL = BASE_URL + "/api/task/get/#caseId#";

  //  Edit Case
  static const String EDITCASE_URL = BASE_URL + "/api/task/put";

  //  Notification
  static const String NOTI_URL =
      BASE_URL + "/api/notifications/get?userId=#userId#";

  static const String NOTI_DELETE_URL =
      BASE_URL + "/api/notifications/delete/#notiId#";

  //  Timeline
  //static const String TIMELINE_URL = BASE_URL +
  // "/api/timeline/get?IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&Count=#count#&CustomerId=#customerId#&Page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_ADVISOR_URL = BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridforprivatemessage?UserId=#userId#&CommunityId=#communityId#&UserCompanyId=#companyId#";
  static const String TIMELINE_MESSAGE_TYPE_URL = BASE_URL +
      "/api/casereport/get/caselistforprivatemessagedata?UserCompanyId=#UserCompanyId#&CustomerId=#CustomerId#&AdviserOrIntroducerId=#AdviserOrIntroducerId#";
  static const String TASKBIDDING_URL =
      BASE_URL + "/api/taskbidding/get?taskId=#taskId#";
  static const String TIMELINE_URL = BASE_URL +
      "/api/timeline/gettimelinebyapp?Count=#count#&count=#count#&IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&customerId=#customerId#&page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_POST_URL = BASE_URL + "/api/timeline/post";

  //  More::Help->Support->Send Email  + Attachments
  static const String MEDIA_UPLOADFILES_URL =
      BASE_URL + "/api/media/uploadpictures";
  static const String RESOLUTION_URL = BASE_URL + "/api/resolution/post";

  //  More::Settings->Edit Profile
  static const String EDIT_PROFILE_URL = BASE_URL + "/api/users/put";
  static const String Get_PROFILE_INFO_URL =
      BASE_URL + "/api/users/getuserprofiledata/#UserID#";
  //BASE_URL + "/api/users/get/#UserID#";

  //  More::Settings->User Notification Settings
  static const String NOTI_SETTINGS_URL =
      BASE_URL + "/api/usernotificationsetting/get?userId=#userId#";
  static const String NOTI_SETTINGS_POST_URL =
      BASE_URL + "/api/usernotificationsetting/post";
  static const String FCM_TEST_NOTI_URL =
      BASE_URL + "/api/notifications/sendtestpushnotificationtouser/#userId#";

  //  More::Badge
  static const String BADGE_USER_GET_URL =
      BASE_URL + "/api/userBadge/get?UserId=#userId#";
  static const String BADGE_EMAIL_URL =
      BASE_URL + "/api/userBadge/postemailbadge";
  static const String BADGE_PHOTOID_URL = BASE_URL + "/api/userBadge/post";
  static const String BADGE_PHOTOID_DEL_URL =
      BASE_URL + "/api/userBadge/delete/#badgeId#";

  //  case review, action required, agreement, digital sign
  static const String CASE_REVIEW = BASE_URL +
      "/api/mortgagecaseinfo/getbyuseridandcompanyidandclientagreementstatus?UserId=#UserId#&UserCompanyInfoId=#UserCompanyInfoId#&ClientAgreementStatus=#ClientAgreementStatus#";
  static const String TERMS_POLICY = BASE_URL +
      "/api/termsprivacynoticesetup/getbycompanyid?UserCompanyId=#UserCompanyId#";

  static const String CaseDigitalSignByCustomerURL =
      BASE_URL + "/apps/case-digital-sign-by-customer/-#CaseID#";
  static const String CLIENTAGREEMENTURL =
      BASE_URL + "/apps/client-agreement/-#CaseID#";

  //  create Case
  static const String SUBMITCASE_URL =
      BASE_URL + "/api/mortgagecasepaymentinfo/post";

  //  WEBVIEW::
  //  Case Details WebView
  static const String CASEDETAILS_WEBVIEW_URL =
      BASE_URL + "/apps/about-me/#title#-#taskId#";
  static const String BASE_URL_NOTI_WEB = BASE_URL + "/apps/about-me";

  //  Misc
  static const String TC_URL =
      "https://brightstarhub.co.uk/legal/privacy-notice-direct-clients/";
  static const String PRIVACY_URL =
      "https://brightstarhub.co.uk/legal/privacy-notice-direct-clients/";
  static const String FAQ_URL = "https://app.mortgage-magic.co.uk/faq-customer";
  static const String EID_URL = BASE_URL + "/mrg/mark-eid-mobile-verification";
}
