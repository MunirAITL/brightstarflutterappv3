import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';

class MyTheme {
  static final bool isBoxDeco = false;
  static final double appbarElevation = 0;
  static final double txtSize = 2.2;
  static final double logoWidth = 40;
  static final double txtLineSpace = 1.3;
  static final String fontFamily = "Arial";

  static Color cyanColor = HexColor.fromHex("#BCEFE6");
  static Color brandColor = HexColor.fromHex('#320B42');
  static final Color greyColor = HexColor.fromHex("#F1F1F4");
  static final Color dgreyColor = HexColor.fromHex("#dac5c5");
  static Color loginBGColor = HexColor.fromHex('#E1E1E5');
  static Color lPurpleColor = HexColor.fromHex("#f0c0c0");
  static Color appbarColor = MyTheme.themeData.accentColor; //.withAlpha(200);
  static final Color appbarProgColor = brandColor;
  static final Color l3BlueColor = HexColor.fromHex("#cad7dc");

  //  lead introducer
  static final Color bgColor = MyTheme.themeData.accentColor;
  static final Color bgColor2 = MyTheme.themeData.accentColor;
  static final titleColor = MyTheme.themeData.accentColor;
  static final title2Color = Color(0xFF6E6E82);
  static final inputColor = Color(0xFF2D4665);
  static final statusBarColor = HexColor.fromHex("#2D4665");
  static final Color leadSubTitle = HexColor.fromHex("#151522");
  static final dBlueAirColor = Color(0xFF252551);
  static final purpleColor = Color(0xFF9c27b0);
  static final radioBGColor = Color(0xFF252551);

  //  timeline
  static final Color timelineTitleColor = HexColor.fromHex("#1F3548");
  static final Color timelinePostCallerNameColor = HexColor.fromHex("#2D2D2D");
  // mycases
  static final Color mycasesNFBtnColor = HexColor.fromHex("#B4B4B4");

  static final radioThemeData = ThemeData(
    unselectedWidgetColor: MyTheme.purpleColor,
    disabledColor: MyTheme.purpleColor,
    selectedRowColor: MyTheme.purpleColor,
    indicatorColor: MyTheme.purpleColor,
    toggleableActiveColor: MyTheme.purpleColor,
  );

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.white,
    accentColor: HexColor.fromHex("#F5F5F7"),

    //iconTheme: IconThemeData(color: Colors.white),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    iconTheme: IconThemeData(color: Colors.white),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  //static final BoxDecoration boxDeco = BoxDecoration(
  //color: Colors.white,
  //);

  static final boxDeco = BoxDecoration(
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);

  static final picEmboseCircleDeco =
      BoxDecoration(color: Colors.grey, shape: BoxShape.circle, boxShadow: [
    BoxShadow(
      color: Colors.grey.shade500,
      blurRadius: 5.0,
      spreadRadius: 2.0,
    ),
  ]);
}
