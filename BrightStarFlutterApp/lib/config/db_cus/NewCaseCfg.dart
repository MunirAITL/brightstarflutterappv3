import 'package:flutter/cupertino.dart';

import '../Server.dart';

class NewCaseCfg {
  static const int ALL = 901;
  static const int IN_PROGRESS = 902;
  static const int SUBMITTED = 903;
  static const int FMA_SUBMITTED = 904;
  static const int COMPLETED = 905;

/*  static const List<String> listSliderImages = [
    Server.DOMAIN + "/assets/img/slider/1.jpg",
    Server.DOMAIN + "/assets/img/slider/2.jpg",
    Server.DOMAIN + "/assets/img/slider/3.jpg",
  ];*/

  static final List<Map<String, dynamic>> listCreateNewCase = [
    {
      "index": 0,
      "title": "Residential Mortgage",
      "url": "assets/images/screens/home/new_case/ic_case_1.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": residentialMortgageSubList,
    },
    {
      "index": 1,
      "title": "Residential Remortgage",
      "url": "assets/images/screens/home/new_case/ic_case_2.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": residentialRemortgageSubList,
    },
    {
      "index": 2,
      "title": "Second Charge - Residential",
      "url": "assets/images/screens/home/new_case/ic_case_3.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": secondChargeResidentialSubList,
    },
    {
      "index": 3,
      "title": "Buy to Let Mortgage",
      "url": "assets/images/screens/home/new_case/ic_case_4.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": buyToLetMortgageSubList,
    },
    {
      "index": 4,
      "title": "Buy to Let Remortgage",
      "url": "assets/images/screens/home/new_case/ic_case_5.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": buyToLetRemortgageSubList,
    },
    {
      "index": 5,
      "title": "Business Lending",
      "url": "assets/images/screens/home/new_case/ic_case_6.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": businessLendingSubList,
    },
    {
      "index": 6,
      "title": "Second Charge - Buy to Let & Commercial",
      "url": "assets/images/screens/home/new_case/ic_case_7.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": secondChargeBuyToLetCommercialSubList,
    },
    {
      "index": 7,
      "title": "Commercial Mortgages/ Loans",
      "url": "assets/images/screens/home/new_case/ic_case_8.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": commercialMortgagesOrLoansSubList,
    },
    {
      "index": 8,
      "title": "Development Finance",
      "url": "assets/images/screens/home/new_case/ic_case_9.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": developmentFinanceSubList,
    },
    {
      "index": 9,
      "title": "Let to Buy",
      "url": "assets/images/screens/home/new_case/ic_case_10.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": letToBuySubList,
    },
    {
      "index": 10,
      "title": "Bridging Loan",
      "url": "assets/images/screens/home/new_case/ic_case_12.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": bridgingLoanSubList,
    },
    {
      "index": 11,
      "title": "Others",
      "url": "assets/images/screens/home/new_case/ic_case_11.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": [],
    },
  ];

  static final List<Map<String, dynamic>> generalInsuranceSubList = [
    {"index": 0, "title": "Buildings & Contents"},
    {"index": 1, "title": "Building Only"},
    {
      "index": 2,
      "title": "Contents Only",
    },
  ];
  static final List<Map<String, dynamic>> bridgingLoanSubList = [
    {
      "index": 0,
      "title": "MCOB regulated - introducer advised",
    },
    {
      "index": 1,
      "title": "Non regulated - introducer advised",
    },
    {
      "index": 2,
      "title": "MCOB regulated - client handover",
    },
    {
      "index": 3,
      "title": "Non regulated - client handover",
    },
    {
      "index": 4,
      "title": "Self build / renovation",
    },
    {
      "index": 5,
      "title": "Auction",
    },
    {
      "index": 6,
      "title": "Other",
    },
  ];
  static final List<Map<String, dynamic>> equeityReleaseSubList = [
    {
      "index": 0,
      "title": "Equity Release",
    },
    {
      "index": 1,
      "title": "Home Reversion",
    },
  ];
  static final List<Map<String, dynamic>> residentialMortgageSubList = [
    {
      "index": 0,
      "title": "Home mover",
    },
    {
      "index": 1,
      "title": "First time buyer",
    },
    {
      "index": 2,
      "title": "Help to buy mortgage",
    },
    {
      "index": 3,
      "title": "Right to buy",
    },
    {
      "index": 4,
      "title": "Shared ownership",
    },
    {
      "index": 5,
      "title": "Self build",
    },
    {
      "index": 6,
      "title": "Other",
    },
  ];
  static final List<Map<String, dynamic>> residentialRemortgageSubList = [
    {
      "index": 0,
      "title": "Right to buy",
    },
    {
      "index": 1,
      "title": "Shared ownership",
    },
    {
      "index": 2,
      "title": "Standard remortgage",
    },
  ];
  static final List<Map<String, dynamic>> secondChargeResidentialSubList = [
    {
      "index": 0,
      "title": "MCOB regulated - introducer advised",
    },
    {
      "index": 1,
      "title": "Non regulated - introducer advised",
    },
    {
      "index": 2,
      "title": "MCOB regulated - client handover",
    },
    {
      "index": 3,
      "title": "Non regulated - client handover",
    },
    {
      "index": 4,
      "title": "Other",
    },
  ];

  static final List<Map<String, dynamic>> buyToLetMortgageSubList = [
    {
      "index": 0,
      "title": "Experienced landlord",
    },
    {
      "index": 1,
      "title": "First time landlord",
    },
    {
      "index": 2,
      "title": "Consumer buy to Let",
    },
    {
      "index": 3,
      "title": "HMO",
    },
    {
      "index": 4,
      "title": "Multi unit",
    },
    {
      "index": 5,
      "title": "Student let",
    },
    {
      "index": 6,
      "title": "Other",
    },
  ];
  static final List<Map<String, dynamic>> buyToLetRemortgageSubList = [
    {
      "index": 0,
      "title": "Experienced landlord",
    },
    {
      "index": 1,
      "title": "Consumer buy to Let",
    },
    {
      "index": 2,
      "title": "Let to buy",
    },
    {
      "index": 3,
      "title": "Other",
    },
  ];
  static final List<Map<String, dynamic>> businessLendingSubList = [];
  static final List<Map<String, dynamic>>
      secondChargeBuyToLetCommercialSubList = [
    {
      "index": 0,
      "title": "Commercial",
    },
    {
      "index": 1,
      "title": "Buy to let",
    },
    {
      "index": 2,
      "title": "Other",
    },
  ];
  static final List<Map<String, dynamic>> commercialMortgagesOrLoansSubList = [
    {
      "index": 0,
      "title": "Purchase",
    },
    {
      "index": 1,
      "title": "Re-finance",
    },
    {
      "index": 2,
      "title": "Trading business",
    },
    {
      "index": 3,
      "title": "Investment",
    },
    {
      "index": 4,
      "title": "Unsecured",
    },
    {
      "index": 5,
      "title": "Other",
    },
  ];

  static final List<Map<String, dynamic>> developmentFinanceSubList = [
    {
      "index": 0,
      "title": "Care home",
    },
    {
      "index": 1,
      "title": "Hotel",
    },
    {
      "index": 2,
      "title": "Land purchase",
    },
    {
      "index": 3,
      "title": "Mezzanine",
    },
    {
      "index": 4,
      "title": "Other",
    },
  ];
  static final List<Map<String, dynamic>> letToBuySubList = [];

  addOtherTitleInCase() {
    if (listCreateNewCase.length == 9) {
      listCreateNewCase.add({
        "index": 9,
        "title": "Others",
        "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
        "isOtherApplicant": false,
        "isSPV": false,
        "subItem": "",
      });
    } else {
      listCreateNewCase[9] = {
        "index": 9,
        "title": "Others",
        "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
        "isOtherApplicant": false,
        "isSPV": false,
        "subItem": "",
      };
    }
  }

  getCaseType(String name) {
    for (final map in listCreateNewCase) {
      if (map["title"].toString().toLowerCase() == name.toLowerCase()) {
        return map;
      }
    }
    return null;
  }
}
