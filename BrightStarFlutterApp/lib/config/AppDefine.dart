class AppDefine {
  //  DEFINE STUFF START HERE...
  static const APP_NAME = "Easy source";
  static const SUPPORT_EMAIL = "helpdesk@brightstarhub.co.uk";
  static const SUPPORT_CALL = "01277 561 116";
  static const COUNTRY_CODE = "+44";
  static const String CUR_SIGN = "£";
  static const geo_code = "uk";
}
