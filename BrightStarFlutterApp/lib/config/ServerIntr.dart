import 'Server.dart';

class ServerIntr {
  //  case lead dashboard
  static const String CASELEAD_DB_INTR1_GET_URL = Server.BASE_URL +
      "/api/mortgagecasepaymentinfo/getuserpaymentsummaryreport?UserId=#userId#";

  static const String CASELEAD_DB_INT2_GET_URL = Server.BASE_URL +
      "/api/casereport/get/getleadtatusintroducerwisereportdatabyintroducer?UserCompanyId=#companyId#&Criteria=#criteria#&Status=#status#&IsSpecificDate=#isSpecificDate#&FromDateTime=#fromDateTime#&ToDateTime=#toDateTime#&AdviserId=#advisorId#&IntroducerId=#introducerId#&Title=#title#";

  //  case lead navigator
  static const String CASELEAD_NAVIGATOR_GET_URL = Server.BASE_URL +
      "/api/users/get/negotiatoruserbyintroducerid?IntroducerId=#introducerId#";

  //  case review
  static const String LEAD_LIST_GET_URL =
      Server.BASE_URL + "/api/resolution/get/getleadinfodatabyusercompanyid?";
  //"/api/resolution/getallresolutionsbycompanyidbyintroducer?";

  //  Get Case Income Monthly Summary Report By Introducer
  static const GET_CASE_INCOME_MONTHLY_REPORT_INTR_URL = Server.BASE_URL +
      "/api/casereport/get/getcaseincomemonthlywisesummaryreportbyintroducer?UserCompanyId=#userCompanyId#&Criteria=#criteria#&FromDateTime=#fromDT#&ToDateTime=#toDT#&IntroducerId=#intrId#";

  //  Get Case Count Monthly Summary Report For Introducer By Introducer
  static const GET_CASE_COUNT_REPORT_INTR_BY_INTR_URL = Server.BASE_URL +
      "/api/casereport/get/getcasecountmonthlywisesummaryreportforintroducerbyintroducer?UserCompanyId=#userCompanyId#&Criteria=#criteria#&FromDateTime=#fromDT#&ToDateTime=#toDT#&IntroducerId=#introId#";

  //  Get user by comunity id, and companyuserid and introducer id for app
  static const GET_USER_BY_COMID_COMPID_INTRID_FOR_APP_URL = Server.BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridandintroducerforapp?UserId=#userId#&CommunityId=#communityId#&UserCompanyId=#userCompanyId#";

  // resolution
  static const POST_RESOLUTION_URL = Server.BASE_URL + "/api/resolution/post";
  static const PUT_RESOLUTION_URL = Server.BASE_URL + "/api/resolution/put";
  static const DEL_RESOLUTION_URL =
      Server.BASE_URL + "/api/resolution/delete/#resId#";

  //  post lead -> lead generator
  static const POST_LEAD_GEN_URL =
      Server.BASE_URL + "/api/leadgenerator/postlead";

  //  Change Stage
  static const PUT_LEADSTATUSCONVERSION_URL =
      Server.BASE_URL + "/api/resolution/put/leadstatusconversion";

  static const GET_CASE_TYPE_USERID_URL = Server.BASE_URL +
      "/api/userdepartment/getcasetypebyuserid?UserId=#userId#";

  static const GET_USERDETAILS_AUTOSUG = Server.BASE_URL +
      "/api/users/get/getuserdetailsdatabyusercompanyid?Address=&CommunityId=#communityId#&Count=30&FromDateTime=#fromDateTime#&Gender=&IsSpecificDate=All&OrderBy=&Page=1&SearchBadge=&SearchText=#searchText#&Skill=&TaskAlertKeywords=&ToDateTime=#toDateTime#&UserCompanyId=#userCompanyId#&UserNoteTitle=&UserTaskCategoryId=0&UserType=";

  static const GET_INVITATION_EMAIL_URL = Server.BASE_URL +
      "/api/users/sendinvitationemail?UserId=#userId#&UserCompanyId=#userCompanyId#";

  static const GET_USER_TASK_CATEGORY_URL =
      Server.BASE_URL + "/api/usertaskcategory/get";

  static const GET_USER_NOTE_BY_LEADID_URL =
      Server.BASE_URL + "/api/usernote/getusernotebyleadid?LeadId=#leadId#";

  static const GET_TWILIO_PHONENUMBER_URL =
      Server.BASE_URL + "/api/twiliophonenumber/getbyuserid?UserId=#userId#";

  static const GET_ALL_RES_SUPPORT_ADMINID_URL = Server.BASE_URL +
      "/api/resolution/getallresolutionsbysupportadminid?SupportAdminId=#SupportAdminId#&IsLockAutoCall=#IsLockAutoCall#";
  static const GET_USERBYCOMMUNITYID_COMPUSERID_FOR_SELECT_OPT_ADVISOR_URL = Server
          .BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridforselectoptionByAdviser?CommunityId=#communityId#&&UserCompanyId=#userCompanyId#";

  static const PUT_UPDATE_CASEOWNER_TO_ME_URL =
      Server.BASE_URL + "/api/resolution/updateassigntome";

  static const GET_LEADBYRES_COMPANYID_URL =
      Server.BASE_URL + "/api/resolution/getleadbyidandcompanyid/#resId#";

  static const POST_USERNOTE_URL = Server.BASE_URL + "/api/usernote/post";

  static const GET_USERNOTE_EMAILSYNC = Server.BASE_URL +
      "/api/usernote/getusernoteemailsyncbyentityidandentitynameandusercompanyidandtype";

  static const GET_EMAILSMS_TEMPLATE_URL = Server.BASE_URL +
      "/api/emailandsmstemplate/emailandsmstemplatebyusercompanyinfoidandcategoryandtype";

  //  MY CASES

  static const GET_CASEINFOBYCOMPANYADMINSEARCH_URL =
      Server.BASE_URL + "/api/task/caseinfobycompanyadminsearch/get";

  //  Refer Client
  static const POST_REFER_CLIENTR_URL =
      Server.BASE_URL + "/api/resolution/sendreferclientmail";
}
