import 'package:aitl/view_model/generic/enum_gen.dart';
import 'package:flutter/material.dart';

enum eLeadStage { Added, Qualifying, Processing, Converted, Junk }

enum eStageName {
  Added,
  Qualifying,
  Processing,
  Converted,
  JunkLead,
  Junk,
  Junk_to_Qualifying,
}

class IntroCfg {
  static const INTRO_COMMUNITYID = 19;

  static const ALL = 901;
  static const SUBMITTED = 101;
  static const RECOMMENDTION = 117;
  static const FMA = 102;
  static const COMPLETED = 103;
  static const ONGOING = 903;
  static const AIP = 114;
  static const VALUATION_INSTRUCTED = 115;
  static const VALUATION_SATISFIED = 112;
  static const OFFERED = 113;
  static const CANCELLED = 104;
  static const DECLINED = 116;

  static final listCaseOwnerCommunityID = ["9", "18"];

  static final stageColor = {
    EnumGen.getEnum2Str(eLeadStage.Added): Colors.blue,
    EnumGen.getEnum2Str(eLeadStage.Qualifying): Color(0XFFFF8C00),
    EnumGen.getEnum2Str(eLeadStage.Processing): Color(0xFFFFFF00),
    EnumGen.getEnum2Str(eLeadStage.Converted): Color(0xFF66ff00),
    EnumGen.getEnum2Str(eLeadStage.Junk): Colors.red[800],
  };

  getStatusName(int status) {
    var stutusName = "";
    try {
      switch (status) {
        case IntroCfg.ALL:
          stutusName = "All cases";
          break;
        case IntroCfg.SUBMITTED:
          stutusName = "Submitted";
          break;
        case IntroCfg.RECOMMENDTION:
          stutusName = "Recommendation";
          break;
        case IntroCfg.FMA:
          stutusName = "FMA";
          break;
        case IntroCfg.COMPLETED:
          stutusName = "Completed";
          break;
        case IntroCfg.ONGOING:
          stutusName = "Ongoing";
          break;
        case IntroCfg.AIP:
          stutusName = "AIP";
          break;
        case IntroCfg.VALUATION_INSTRUCTED:
          stutusName = "Valuation instructed";
          break;
        case IntroCfg.VALUATION_SATISFIED:
          stutusName = "Valuation satisfied";
          break;
        case IntroCfg.OFFERED:
          stutusName = "Offered";
          break;
        case IntroCfg.CANCELLED:
          stutusName = "Cancelled";
          break;
        case IntroCfg.DECLINED:
          stutusName = "Declined";
          break;
        default:
          break;
      }
    } catch (e) {}
    return stutusName;
  }

  /*static final listCaseTree = [
    {
      "index": 0,
      "name": "Residential Mortgage",
      "level": [
        "Home Mover",
        "First time buyer",
        "Help to Buy Mortgage",
        "Right to Buy",
        "Shared Ownership",
      ]
    },
    {
      "index": 1,
      "name": "Residential Remortgage",
      "level": [
        "Right to Buy",
        "Shared Ownership",
        "Standard Remortgage",
      ]
    },
    {"index": 2, "name": "Second Charge - Residential", "level": []},
    {
      "index": 3,
      "name": "Buy to Let Mortgage",
      "level": [
        "Experienced Landlord",
        "First time Landlord",
        "Consumer Buy to Let",
      ]
    },
    {
      "index": 4,
      "name": "Buy to Let Remortgage",
      "level": [
        "Experienced Landlord",
        "Consumer buy to let",
        "Let to buy",
        "Other"
      ]
    },
    {
      "index": 5,
      "name": "Second Charge - Buy to Let & Commercial",
      "level": []
    },
    {"index": 6, "name": "Commercial Mortgages/Loans", "level": []},
    {"index": 7, "name": "Business Lending", "level": []},
    {
      "index": 8,
      "name": "Development Finance",
      "level": [
        "Full Development Project",
        "Conversion Project",
        "Heavy Refurbishment",
        "Light Refurbishment",
      ]
    },
    {"index": 9, "name": "Let to Buy", "level": []},
    {
      "index": 10,
      "name": "Bridging Loan",
      "level": [
        "Auction Purchase",
        "Standard Bridging Loan",
        "Semi Commercial Bridging Loan",
        "Commercial Bridging Loan",
        "Regulated Bridging Loan",
        "Structured Short Term Finance",
      ]
    },
    {"index": 11, "name": "Protection Hub", "level": []},
    {"index": 12, "name": "Equity Release", "level": []},
    {"index": 13, "name": "General Insurance", "level": []},
    {"index": 14, "name": "Others", "level": null},
  ];*/
}
