class AppConfig {
  //static const
  static const int page_limit_dashboard = 4;
  static const int page_limit = 30;
  static const int pageAnimationMilliSecond = 500;
  static const AlertDismisSec = 3;
}
