import 'package:aitl/mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefMgr with Mixin {
  static final PrefMgr shared = PrefMgr._internal();
  factory PrefMgr() {
    return shared;
  }

  PrefMgr._internal();

  //
  setPrefStr(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(key, val);
    } catch (e) {}
  }

  getPrefStr(key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    } catch (e) {}
    return null;
  }

  setPrefInt(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setInt(key, val);
    } catch (e) {}
  }

  Future<int> getPrefInt(key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getInt(key) ?? 0;
    } catch (e) {}
    return 0;
  }
}
