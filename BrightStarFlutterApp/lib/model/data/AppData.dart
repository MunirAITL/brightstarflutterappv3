class AppData {
  static final AppData _appData = new AppData._internal();
  factory AppData() {
    return _appData;
  }
  AppData._internal();

  bool isIntroducer;
}

final appData = AppData();
