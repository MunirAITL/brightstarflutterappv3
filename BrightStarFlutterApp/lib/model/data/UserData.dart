import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';

class UserData {
  static final UserData _appData = new UserData._internal();
  UserModel userModel;

  int communityId; //  for switching into cus/introducer dashboard

  factory UserData() {
    return _appData;
  }
  UserData._internal();

  setUserModel() async {
    userModel = await DBMgr.shared.getUserProfile();
    try {
      communityId = int.parse(userModel.communityID);
    } catch (e) {
      communityId = 1;
    }
  }
}

final userData = UserData();
