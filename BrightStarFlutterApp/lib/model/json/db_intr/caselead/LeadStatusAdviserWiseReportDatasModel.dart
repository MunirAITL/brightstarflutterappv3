class LeadStatusAdviserWiseReportDatasModel {
  int id;
  String name;
  String communityId;
  String communityName;
  String imageServerUrl;
  String thumbnailPath;
  String profileImageUrl;
  int userCompanyId;
  String companyName;
  int active;
  int totalLeads;
  int totalAddedLeads;
  int totalQualifyingLeads;
  int totalProcessingLeads;
  int totalConvertedLeads;
  int totalJunkLeads;
  int totalNotproceedingLeads;
  int totalCase;
  int leadConversionRate;
  int junkPercentageRate;
  int notProceedingPercentageRate;
  int caseSuccessRate;
  int totalCompletedCase;

  LeadStatusAdviserWiseReportDatasModel(
      {this.id,
      this.name,
      this.communityId,
      this.communityName,
      this.imageServerUrl,
      this.thumbnailPath,
      this.profileImageUrl,
      this.userCompanyId,
      this.companyName,
      this.active,
      this.totalLeads,
      this.totalAddedLeads,
      this.totalQualifyingLeads,
      this.totalProcessingLeads,
      this.totalConvertedLeads,
      this.totalJunkLeads,
      this.totalNotproceedingLeads,
      this.totalCase,
      this.leadConversionRate,
      this.junkPercentageRate,
      this.notProceedingPercentageRate,
      this.caseSuccessRate,
      this.totalCompletedCase});

  LeadStatusAdviserWiseReportDatasModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    name = json['Name'] ?? '';
    communityId = json['CommunityId'] ?? '';
    communityName = json['CommunityName'] ?? '';
    imageServerUrl = json['ImageServerUrl'] ?? '';
    thumbnailPath = json['ThumbnailPath'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    userCompanyId = json['UserCompanyId'] ?? 0;
    companyName = json['CompanyName'] ?? '';
    active = json['Active'] ?? 0;
    totalLeads = json['TotalLeads'] ?? 0;
    totalAddedLeads = json['TotalAddedLeads'] ?? 0;
    totalQualifyingLeads = json['TotalQualifyingLeads'] ?? 0;
    totalProcessingLeads = json['TotalProcessingLeads'] ?? 0;
    totalConvertedLeads = json['TotalConvertedLeads'] ?? 0;
    totalJunkLeads = json['TotalJunkLeads'] ?? 0;
    totalNotproceedingLeads = json['TotalNotproceedingLeads'] ?? 0;
    totalCase = json['TotalCase'] ?? 0;
    leadConversionRate = json['LeadConversionRate'] ?? 0;
    junkPercentageRate = json['JunkPercentageRate'] ?? 0;
    notProceedingPercentageRate = json['NotProceedingPercentageRate'] ?? 0;
    caseSuccessRate = json['CaseSuccessRate'] ?? 0;
    totalCompletedCase = json['TotalCompletedCase'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Name'] = this.name;
    data['CommunityId'] = this.communityId;
    data['CommunityName'] = this.communityName;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['UserCompanyId'] = this.userCompanyId;
    data['CompanyName'] = this.companyName;
    data['Active'] = this.active;
    data['TotalLeads'] = this.totalLeads;
    data['TotalAddedLeads'] = this.totalAddedLeads;
    data['TotalQualifyingLeads'] = this.totalQualifyingLeads;
    data['TotalProcessingLeads'] = this.totalProcessingLeads;
    data['TotalConvertedLeads'] = this.totalConvertedLeads;
    data['TotalJunkLeads'] = this.totalJunkLeads;
    data['TotalNotproceedingLeads'] = this.totalNotproceedingLeads;
    data['TotalCase'] = this.totalCase;
    data['LeadConversionRate'] = this.leadConversionRate;
    data['JunkPercentageRate'] = this.junkPercentageRate;
    data['NotProceedingPercentageRate'] = this.notProceedingPercentageRate;
    data['CaseSuccessRate'] = this.caseSuccessRate;
    data['TotalCompletedCase'] = this.totalCompletedCase;
    return data;
  }
}
