
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';

class SubmitCaseAPIModel {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;

  SubmitCaseAPIModel(
      {this.success, this.errorMessages, this.messages});

  factory SubmitCaseAPIModel.fromJson(Map<String, dynamic> j) {
    return SubmitCaseAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},

    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
      };
}

class _ErrorMessages {}

class _Messages {
  List<dynamic> task_post;
  _Messages({this.task_post});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      task_post: j['task_post'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'task_post': task_post,
      };
}


