class ChangePwdAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  Null responseData;

  ChangePwdAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  ChangePwdAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : [];
    messages =
        json['Messages'] != null ? new Messages.fromJson(json['Messages']) : [];
    responseData = json['ResponseData'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    data['ResponseData'] = this.responseData;
    return data;
  }
}

class ErrorMessages {
  List<String> changePassword;
  ErrorMessages({this.changePassword});
  ErrorMessages.fromJson(Map<String, dynamic> json) {
    try {
      changePassword = json['change_password'].cast<String>() ?? [];
    } catch (e) {
      changePassword = [];
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['change_password'] = this.changePassword;
    return data;
  }
}

class Messages {
  List<String> changePassword;
  Messages({this.changePassword});
  Messages.fromJson(Map<String, dynamic> json) {
    try {
      changePassword = json['change_password'].cast<String>() ?? [];
    } catch (e) {
      changePassword = [];
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['change_password'] = this.changePassword;
    return data;
  }
}
