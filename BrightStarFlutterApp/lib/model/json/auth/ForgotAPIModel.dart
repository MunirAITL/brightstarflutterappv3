class ForgotAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  _Messages messages;
  dynamic responseData;

  ForgotAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //LoginModel({this.success, this.errorMessages, this.messages});

  factory ForgotAPIModel.fromJson(Map<String, dynamic> j) {
    return ForgotAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']) ?? [],
      messages: _Messages.fromJson(j['Messages']) ?? [],
      responseData: j['ResponseData'] ?? null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  dynamic forgotpassword;
  _ErrorMessages({this.forgotpassword});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      forgotpassword: j['forgotpassword'] ?? {},
    );
  }
  Map<String, dynamic> toMap() => {
        'forgotpassword': forgotpassword,
      };
}

class _Messages {
  dynamic forgotpassword;
  _Messages({this.forgotpassword});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      forgotpassword: j['forgotpassword'] ?? {},
    );
  }
  Map<String, dynamic> toMap() => {
        'forgotpassword': forgotpassword,
      };
}
