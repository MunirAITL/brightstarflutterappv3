import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/welcome/WelcomeScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'config/AppDefine.dart';
import 'config/Server.dart';
import 'controller/network/NetworkMgr.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  //  device settings
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    print("FlutterErrorDetails" + details.toString());
  };

  EasyLoading.instance
    ..displayDuration = const Duration(seconds: 3)
    ..loadingStyle = EasyLoadingStyle.custom
    ..textColor = MyTheme.brandColor
    ..textStyle = TextStyle(
        fontFamily: "Fieldwork", fontSize: 17, color: MyTheme.brandColor)
    ..backgroundColor = MyTheme.bgColor2
    ..indicatorColor = MyTheme.brandColor
    ..maskColor = MyTheme.brandColor
    ..indicatorType = EasyLoadingIndicatorType.dualRing
    ..maskType = EasyLoadingMaskType.clear
    ..userInteractions = false;

  HttpOverrides.global = MyHttpOverrides();

  runApp(MyApp());
}

class MyApp extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return new GetMaterialApp(
        debugShowCheckedModeBanner: false,
        enableLog: Server.isTest,
        defaultTransition: Transition.fade,
        title: AppDefine.APP_NAME,
        theme: MyTheme.themeData,
        builder: (context, widget) {
          // do your initialization here
          widget = EasyLoading.init()(context, widget);
          return widget;
        },
        home: WelcomeScreen());
  }
}
