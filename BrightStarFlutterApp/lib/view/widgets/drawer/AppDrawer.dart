import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/api/db_cus/noti/DeleteNotification.dart';
import 'package:aitl/controller/api/db_cus/noti/NotiAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_noti/NotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiModel.dart';
import 'package:aitl/view/db_cus/more/badges/BadgeScreen.dart';
import 'package:aitl/view/db_cus/more/profile/ProfileScreen.dart';
import 'package:aitl/view/db_cus/more/settings/SettingsScreen.dart';
import 'package:aitl/view/db_cus/timeline/TimeLineTab.dart';
import 'package:aitl/view/widgets/btn/BtnOutlineAndBackground.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/images/ImgFade.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../images/MyNetworkImage.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AppDrawer extends StatefulWidget {
  final bool isNewCaseTab;
  final bool isMyCasesTab;
  final bool isTimelineTab;
  final bool isNotiTab;
  final bool isRightClose;

  AppDrawer({
    Key key,
    @required this.isRightClose,
    @required this.isNewCaseTab,
    @required this.isMyCasesTab,
    @required this.isTimelineTab,
    @required this.isNotiTab,
  }) : super(key: key);

  @override
  State createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> with Mixin {
  PackageInfo packageInfo;
  final StateProvider _stateProvider = StateProvider();
  final posKey = GlobalKey();
  bool isNotiViewClicked = false;

  bool isDialogHelpOpenned = false;
  List<String> listImgHelpTut = [
    "assets/images/helptut/newcase.png",
    "assets/images/helptut/mycases.png",
    "assets/images/helptut/timeline.png",
    "assets/images/helptut/noti.png",
    "assets/images/helptut/more.png",
  ];
  int indexHelpTut = 0;
  int unreadNotiCount = 0;

  //  noti tab start here
  List<NotiModel> listNotiModel = [];

//  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  onPageLoad() async {
    setState(() {
      isLoading = true;
    });

    try {
      NotiAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> notifications =
                      model.responseData.notifications;

                  if (notifications != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (notifications.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (NotiModel noti in notifications) {
                        listNotiModel.add(noti);
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }
                    myLog(listNotiModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(context: context, msg: "Notifications not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    listNotiModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listImgHelpTut = null;
    listNotiModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      packageInfo = await PackageInfo.fromPlatform();
      unreadNotiCount =
          await PrefMgr.shared.getPrefInt("unreadNotificationCount");
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return (isDialogHelpOpenned) ? drawHelpTutBG() : drawLayout();
  }

  drawHelpTutBG() {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: getW(context),
          height: getH(context),
          child: ImgFade(url: listImgHelpTut[indexHelpTut]),
        ),
      ),
    );
  }

  drawLayout() {
    return SizedBox(
      width: getW(context), //20.0,
      child: Drawer(
          child: Container(
        color: MyTheme.themeData.accentColor,
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10),
            drawHeader(),
            (!isNotiViewClicked) ? drawItems() : drawNotiView(),
          ],
        ),
      )),
    );
  }

  drawItems() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Row(
            children: [
              SizedBox(width: getWP(context, 9)),
              Container(
                width: getWP(context, 10),
                height: getWP(context, 10),
                decoration: MyTheme.picEmboseCircleDeco,
                child: CircleAvatar(
                  radius: 10,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl(userData.userModel.profileImageURL),
                  ),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Txt(
                    txt: userData.userModel.name,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 40, right: 40, bottom: 10),
            child: Container(color: Colors.black45, height: 0.5),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 20),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "New case",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () async {
                Navigator.pop(context);
                if (!widget.isNewCaseTab) {
                  //  notifier to back for dashboard load
                  _stateProvider
                      .notify(ObserverState.STATE_CHANGED_dashboard_reload);
                }
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "My cases",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () async {
                Get.back();
                if (!widget.isMyCasesTab) {
                  Get.to(
                    () => MyCaseTab(),
                  ).then((value) {
                    //callback(route);
                  });
                }
              },
            ),
          ),*/
          /*Padding(
            padding: const EdgeInsets.only(left: 40, top: 20),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Row(
                children: [
                  Txt(
                      txt: "Messages",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Badge(
                    showBadge: (userData.userModel.unreadMessageCount > 0)
                        ? true
                        : false,
                    position: BadgePosition.topEnd(top: -10, end: 20),
                    badgeContent: Txt(
                        txt: userData.userModel.unreadMessageCount.toString(),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ],
              ),
              onTap: () async {
                //Get.back();
                if (!widget.isTimelineTab) {
                  Get.to(
                    () => TimeLineTab(),
                  ).then((value) {
                    //callback(route);
                  });
                }
              },
            ),
          ),*/
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Row(
                children: [
                  Txt(
                      txt: "Notifications",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Badge(
                      showBadge: (widget.totalNoti > 0) ? true : false,
                      position: BadgePosition.topEnd(top: -10, end: 20),
                      badgeContent: Txt(
                        widget.totalNoti.toString(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10
                        ),
                      ))
                ],
              ),
              onTap: () async {
                Get.back();
                if (!widget.isNotiTab) {
                  Get.to(
                    () => NotiTab(),
                  ).then((value) {
                    //callback(route);
                  });
                }
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 10),
            child: ListTile(
              key: posKey,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Row(
                children: [
                  Txt(
                      txt: "Notifications",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Badge(
                    showBadge:
                        (userData.userModel.unreadNotificationCount > 0 &&
                                unreadNotiCount !=
                                    userData.userModel.unreadNotificationCount)
                            ? true
                            : false,
                    position: BadgePosition.topEnd(top: -10, end: 20),
                    badgeContent: Txt(
                        txt: userData.userModel.unreadNotificationCount
                            .toString(),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize - .6,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ],
              ),
              onTap: () async {
                try {
                  await PrefMgr.shared.setPrefInt("unreadNotificationCount",
                      userData.userModel.unreadNotificationCount);
                } catch (e) {}
                setState(() {
                  isNotiViewClicked = true;
                });
              },
              trailing: IconButton(
                  iconSize: 20,
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black54,
                  ),
                  onPressed: () async {
                    try {
                      await PrefMgr.shared.setPrefInt("unreadNotificationCount",
                          userData.userModel.unreadNotificationCount);
                    } catch (e) {}
                    setState(() {
                      isNotiViewClicked = true;
                    });
                  }),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 40,
              top: 20,
              bottom: 20,
            ),
            child: Txt(
                txt: "YOUR ACCOUNT",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize - 0.7,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 0),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "My profile",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => ProfileScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Summary",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => SummaryScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 10),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Settings",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => SettingsScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.only(left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "All Account",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(() => AllAccScreen(
                      isLoggedIn: true,
                      userModel: userModel,
                    ));
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 10),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Badges",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => BadgeScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
          /*     Padding(
            padding: const EdgeInsets.only(left: 40,top: 20),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Help",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize-.2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                Get.to(
                  () => HelpScreen(),
                ).then((isHelpTut) {
                  */ /*if (isHelpTut) {
                    if (!isDialogHelpOpenned) {
                      isDialogHelpOpenned = true;
                      Get.dialog(HelpTutDialog(
                        callback: (int index) {
                          //
                          indexHelpTut = index;
                          setState(() {});
                          myLog(index.toString());
                        },
                      )).then((value) {
                        isDialogHelpOpenned = false;
                        //Get.back();
                      });
                    }
                    setState(() {});
                  }*/ /*
                });
              },
            ),
          ),*/
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 10),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Privacy",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                if (mounted) {
                  PrivacyPolicyAPIMgr().wsOnLoad(
                    context: context,
                    callback: (model) {
                      if (model != null) {
                        try {
                          if (model.success) {
                            debugPrint(
                                "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                            for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                in model.responseData
                                    .termsPrivacyNoticeSetupsList) {
                              if (termsPrivacyPoliceSetups.type.toString() ==
                                  "Customer Privacy Notice") {
                                Get.to(
                                  () => PDFDocumentPage(
                                    title: "Privacy",
                                    url: termsPrivacyPoliceSetups.webUrl,
                                  ),
                                ).then((value) {
                                  //callback(route);
                                });
                                break;
                              }
                            }
                          } else {
                            showToast(
                                context: context,
                                msg: "Sorry, something went wrong");
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      } else {
                        myLog("dashboard screen not in");
                      }
                    },
                  );
                }
                /* Get.to(() => WebScreenPrivacyPdfView(
                      title: "Privacy",
                      url: Server.PRIVACY_URL,
                    ));*/
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 10),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              title: Txt(
                  txt: "Log out",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              onTap: () {
                confirmDialog(
                    msg: "Are you sure, you want to Log out?",
                    title: "Alert !",
                    callbackYes: () {
                      StateProvider()
                          .notify(ObserverState.STATE_CHANGED_logout);
                    },
                    callbackNo: () {
                      //Navigator.of(context, rootNavigator: true).pop();
                    },
                    context: context);
              },
            ),
          ),
          SizedBox(height: 20),
          packageInfo != null
              ? Padding(
                  padding:
                      const EdgeInsets.only(left: 20, right: 20, bottom: 30),
                  child: Container(
                    width: getW(context),
                    //color: Colors.green,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Txt(
                            txt: "Version",
                            txtColor: Colors.black87,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.bold,
                            isBold: false),
                        SizedBox(height: 7),
                        Txt(
                            txt: packageInfo.version,
                            txtColor: Colors.grey,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }

  drawHeader() {
    return (widget.isRightClose)
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Container(
                    width: getWP(context, MyTheme.logoWidth),
                    //height: getHP(context, 12),
                    child: Image.asset(
                      "assets/images/logo/logo.png",
                      //fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20, bottom: 10),
                child: GestureDetector(
                    child: Icon(
                      Icons.close,
                      color: MyTheme.brandColor,
                      size: 25,
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    }),
              ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, bottom: 10),
                child: IconButton(
                    iconSize: 35,
                    icon: Icon(
                      Icons.close,
                      color: Colors.black45,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Container(
                    width: getWP(context, 40),
                    //height: getHP(context, 12),
                    child: Image.asset(
                      "assets/images/logo/logo.png",
                      //fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
            ],
          );
  }

  drawNotiView() {
    try {
      return ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 40),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              dense: true,
              minLeadingWidth: 0,
              leading: IconButton(
                iconSize: 20,
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black54,
                ),
                onPressed: () {
                  setState(() {
                    isNotiViewClicked = false;
                  });
                },
              ),
              title: Transform.translate(
                offset: Offset(0, 0),
                child: Row(
                  children: [
                    Txt(
                        txt: "Notifications",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(width: 10),
                    Badge(
                      showBadge: (userData.userModel.unreadNotificationCount >
                                  0 &&
                              unreadNotiCount !=
                                  userData.userModel.unreadNotificationCount)
                          ? true
                          : false,
                      position: BadgePosition.topEnd(top: -10, end: 20),
                      badgeContent: Txt(
                          txt: userData.userModel.unreadNotificationCount
                              .toString(),
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - .6,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                  ],
                ),
              ),
              onTap: () async {
                unreadNotiCount =
                    await PrefMgr.shared.getPrefInt("unreadNotificationCount");
                setState(() {
                  isNotiViewClicked = false;
                });
              },
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(left: 40, right: 10, top: 10),
              child: Container(
                height: getHP(context, 75),
                //color: MyTheme.skyColor,
                //color: Colors.blue,
                child: (listNotiModel.length > 0)
                    ? NotificationListener(
                        onNotification: (scrollNotification) {
                          if (scrollNotification is ScrollStartNotification) {
                            //print('Widget has started scrolling');
                          } else if (scrollNotification
                              is ScrollEndNotification) {
                            if (!isPageDone) {
                              pageStart++;
                              onPageLoad();
                            }
                          }
                          return true;
                        },
                        child: RefreshIndicator(
                          color: Colors.white,
                          backgroundColor: MyTheme.brandColor,
                          onRefresh: _getRefreshData,
                          child: ListView.builder(
                            addAutomaticKeepAlives: true,
                            cacheExtent: AppConfig.page_limit.toDouble(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            primary: false,
                            itemCount: listNotiModel.length,
                            itemBuilder: (BuildContext context, int index) {
                              return drawNotiItem(index);
                            },
                          ),
                        ),
                      )
                    : (!isLoading)
                        ? Padding(
                            padding: const EdgeInsets.all(20),
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: getWP(context, 40),
                                    height: getHP(context, 20),
                                    child: Image.asset(
                                      'assets/images/screens/home/my_cases/case_nf.png',
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Txt(
                                    txt:
                                        "Looks like you haven't got any notification?",
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.center,
                                    isBold: false,
                                    txtLineSpace: 1.2,
                                  ),
                                  SizedBox(height: 20),
                                  GestureDetector(
                                    onTap: () {
                                      onPageLoad();
                                    },
                                    child: Txt(
                                        txt: "Refresh",
                                        txtColor: MyTheme.brandColor,
                                        txtSize: MyTheme.txtSize - .2,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Center(
                            child: CircularProgressIndicator(
                                backgroundColor: MyTheme.brandColor,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.white)),
                          ),
              ),
            ),
          ),
        ],
      );
    } catch (e) {
      myLog(e.toString());
      return Container();
    }
  }

  drawNotiItem(index) {
    try {
      NotiModel notiModel = listNotiModel[index];
      print("notification object = ${notiModel.toMap()}");

      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap = NotiHelper().getNotiMap(model: notiModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        // txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      String initiatorDisplayName = '';
      if ((txt.startsWith(notiModel.initiatorDisplayName))) {
        // txt = txt.replaceAll(notiModel.initiatorDisplayName.trim(), '');
        initiatorDisplayName = notiModel.initiatorDisplayName;
      }

      return GestureDetector(
        onTap: () async {},
        child: Card(
          color: MyTheme.appbarColor,
          elevation: 0,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //notification image
                  /*  Padding(
                    padding: const EdgeInsets.only(left: 0, top: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: MyTheme.picEmboseCircleDeco,
                          child: CircleAvatar(
                            radius: 25,
                            backgroundColor: Colors.transparent,
                            backgroundImage: new CachedNetworkImageProvider(
                              MyNetworkImage.checkUrl((notiModel != null)
                                  ? notiModel.initiatorImageUrl
                                  : Server.MISSING_IMG),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),*/
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, right: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Txt(
                            txt: notiModel.initiatorDisplayName,
                            txtColor: Colors.black,
                            txtSize: 2,
                            isBold: true,
                            txtAlign: TextAlign.start,
                            maxLines: 1,
                          ),
                          Txt(
                            txt: txt,
                            txtColor: Colors.black,
                            txtSize: 1.5,
                            isBold: false,
                            txtAlign: TextAlign.start,
                            maxLines: 2,
                          ),
                          SizedBox(height: 20),
                          Txt(
                            txt: notiMap['publishDateTime'],
                            txtColor: MyTheme.mycasesNFBtnColor,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                          SizedBox(height: 10),
                          Container(
                            width: getW(context),
                            height: 45,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Container(
                                    child: BtnOutlineAndBackground(
                                        backgroundColor:
                                            HexColor.fromHex("#320B42"),
                                        txt: "View Case",
                                        txtColor: MyTheme.greyColor,
                                        borderColor: MyTheme.appbarColor,
                                        callback: () {
                                          NotiHelper().setRoute(
                                              context: context,
                                              notiModel: notiModel,
                                              notiMap: notiMap,
                                              callback: () {});
                                        }),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  child: Container(
                                    child: BtnOutlineAndBackground(
                                      txt: "Delete",
                                      txtColor: Colors.white,
                                      borderColor: MyTheme.dgreyColor,
                                      backgroundColor:
                                          HexColor.fromHex("#FF5D4C"),
                                      callback: () {
                                        confirmDialog(
                                            callbackYes: () {
                                              NotiAPIMgr().wsDeleteNotification(
                                                  context: context,
                                                  id: notiModel.id.toString(),
                                                  callback:
                                                      (DeleteNotificationModel
                                                          model) {
                                                    if (model != null &&
                                                        mounted) {
                                                      try {
                                                        if (model.success) {
                                                          _getRefreshData();
                                                        }
                                                      } catch (e) {}
                                                    } else {}
                                                  });
                                            },
                                            callbackNo: () {},
                                            context: context,
                                            msg:
                                                "Press \'Delete\' to Remove this notification ?",
                                            title: "Delete Alert !");
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                  notiModel.isRead
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 3.0, right: 9.0, top: 30.0, bottom: 3.0),
                          child: Container(
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                            width: 15,
                            height: 15,
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(
                              left: 3.0, right: 9.0, top: 3.0, bottom: 3.0),
                          child: Container(
                            decoration: new BoxDecoration(
                              color: Colors.blueAccent,
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                            width: 15,
                            height: 15,
                          ),
                        )
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                width: getW(context),
                color: Colors.grey[300],
                height: 1,
              )
            ],
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
    return SizedBox();
  }

/*
  drawNotiItem(index) {
    try {
      NotiModel notiModel = listNotiModel[index];
      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap = NotiHelper().getNotiMap(model: notiModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      final doubletik = (notiModel.isRead) ? "check2_icon.png" : "check1_icon.png";

      return GestureDetector(
        onTap: () async {
          if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                notiModel: notiModel,
                notiMap: notiMap,
                callback: () async {
                  //  go to timeline as per condition
                  Get.to(
                    () => TimeLineTab(),
                  ).then((value) {
                    //callback(route);
                  });
                });
          }
        },
        child: Container(
          //height: getHP(context, 25),
          //color: Colors.transparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl((notiModel != null) ? notiModel.initiatorImageUrl : MyDefine.MISSING_IMG),
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //SizedBox(height: 20),
                      BoldTxt(text1: txt ?? '', text2: eventName),
                      SizedBox(height: 10),
                      Center(
                        child: Txt(txt: notiMap['publishDateTime'].toString(), txtColor: Colors.grey, txtSize: MyTheme.txtSize - .8, txtAlign: TextAlign.start, isBold: false),
                      ),
                      //SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Container(
                    child: Image.asset(
                      "assets/images/icons/" + doubletik,
                      width: 20,
                      height: 20,
                      color: (notiModel.isRead) ? Colors.green : Colors.grey,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
    return SizedBox();
  }*/
}
