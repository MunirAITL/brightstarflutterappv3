import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class SliderNav extends StatelessWidget {
  final String title;
  final int len;
  final int index;
  final Function(bool) callback;

  const SliderNav({
    Key key,
    @required this.title,
    @required this.len,
    @required this.index,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Txt(
                txt: title,
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          IconButton(
              iconSize: 20,
              icon: Icon(
                Icons.arrow_back_ios,
                color: (index == 0) ? Colors.grey : Colors.black,
              ),
              onPressed: () {
                //
                callback(true);
              }),
          IconButton(
              iconSize: 20,
              icon: Icon(
                Icons.arrow_forward_ios,
                color: (index == len - 1 ? Colors.grey : Colors.black),
              ),
              onPressed: () {
                //
                callback(false);
              })
        ],
      ),
    );
  }
}
