import 'package:aitl/mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/switchview/SwitchView.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/gplaces/GPlacesView.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

import '../txt/IcoTxtIco.dart';

class SPVSwitchView extends StatefulWidget {
  bool isSwitch;
  final Function(String) callback;
  final Function(String) callbackAddress;
  final Function(bool) callbackSwitch;
  final TextEditingController compName;
  final TextEditingController regNo;

  final FocusNode focusCompName;
  final FocusNode focusRegNo;

  final String regAddr;
  String regDate;
  SPVSwitchView({
    Key key,
    @required this.compName,
    @required this.regAddr,
    @required this.regNo,
    @required this.regDate,
    @required this.callback,
    @required this.callbackAddress,
    @required this.callbackSwitch,
    @required this.focusCompName,
    @required this.focusRegNo,
    this.isSwitch = false,
  }) : super(key: key);

  @override
  State createState() => _SPVSwitchViewState();
}

class _SPVSwitchViewState extends State<SPVSwitchView> with Mixin {
  String dd = "";
  String mm = "";
  String yy = "";

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(
                  child: Txt(
                    txt:
                        "Are you buying the property in the name of a SPV (Limited Company)?",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: true,
                    fontWeight: FontWeight.w500,
                    //txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 5),
                SwitchView(
                  isSelected: widget.isSwitch,
                  list: ["Yes", "No"],
                  onChanged: (value) {
                    widget.isSwitch = value;
                    widget.callbackSwitch(value);
                    if (mounted) {
                      setState(() {});
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (widget.isSwitch) ? drawSPVInputFields() : SizedBox(),
        ],
      ),
    );
  }

  drawSPVInputFields() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Column(
          children: [
            drawInputBox(
              context: context,
              title: "Name of Company",
              input: widget.compName,
              kbType: TextInputType.name,
              inputAction: TextInputAction.next,
              focusNode: widget.focusCompName,
              focusNodeNext: widget.focusRegNo,
              len: 100,
            ),
            /*drawInputBox(
              title: "Registered Address",
              input: widget.regAddr,
              kbType: TextInputType.streetAddress,
              len: 255,
            ),*/
            SizedBox(height: 10),
            GPlacesView(
                title: "Registered Address",
                address: widget.regAddr,
                callback: (String address, Location loc) {
                  widget.callbackAddress(address);
                }),
            SizedBox(height: 10),
            drawInputBox(
              context: context,
              title: "Company Registeration Number",
              input: widget.regNo,
              kbType: TextInputType.name,
              inputAction: TextInputAction.next,
              focusNode: widget.focusRegNo,
              len: 50,
            ),
            drawRegDate(),
          ],
        ),
      ),
    );
  }

  drawRegDate() {
    DateTime date = DateTime.now();
    var newDate = new DateTime(date.year, date.month, date.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Txt(
            txt: "Date registered",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 10),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: newDate,
                firstDate: DateTime(1950),
                lastDate: newDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme:
                          ColorScheme.light(primary: MyTheme.brandColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  if (mounted) {
                    setState(() {
                      try {
                        widget.regDate =
                            DateFormat('dd-MMM-yyyy').format(value).toString();
                        final dobArr = date.toString().split('-');
                        this.dd = dobArr[0];
                        this.mm = dobArr[1];
                        this.yy = dobArr[2];
                        widget.callback(widget.regDate);
                      } catch (e) {
                        myLog(e.toString());
                      }
                    });
                  }
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: (widget.regDate == "") ? "Select a date" : widget.regDate,
              rightIcon: Icons.arrow_drop_down,
              txtAlign: TextAlign.left,
              rightIconSize: 50,
            ),
          ),
        ],
      ),
    );
  }
}
