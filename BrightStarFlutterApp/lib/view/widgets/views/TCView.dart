import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;
  Color txt1Color;
  Color txt2Color;

  TCView({
    @required this.screenName,
    this.txt1Color = Colors.black,
    this.txt2Color = Colors.blue,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: "By clicking on '" +
                      screenName +
                      "' you confirm that you accept the ",
                  style: TextStyle(
                    height: MyTheme.txtLineSpace,
                    color: txt1Color,
                    fontSize: getTxtSize(
                        context: context, txtSize: MyTheme.txtSize - .4),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: AppDefine.APP_NAME + ' Terms and Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            decoration: TextDecoration.underline,
                            decorationThickness: 2,
                            color: txt2Color,
                            fontSize: getTxtSize(
                                context: context,
                                txtSize: MyTheme.txtSize - .4),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Terms & Conditions",
                                url: Server.TC_URL,
                              ),
                              fullscreenDialog: true,
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
