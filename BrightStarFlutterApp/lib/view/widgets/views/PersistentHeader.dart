import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';

class PersistentHeader extends SliverPersistentHeaderDelegate with Mixin {
  final Widget widget;
  final h;
  PersistentHeader({this.h, this.widget});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      width: double.infinity,
      height: h,
      child: Card(
        margin: EdgeInsets.all(0),
        color: MyTheme.appbarColor,
        child: Center(child: widget),
      ),
    );
  }

  @override
  double get maxExtent => h;

  @override
  double get minExtent => h;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
