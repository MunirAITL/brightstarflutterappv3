import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PersistentHeader extends SliverPersistentHeaderDelegate {
  final Widget widget;
  final double h;

  PersistentHeader({this.widget, this.h});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      width: double.infinity,
      height: h,
      child: Card(
        margin: EdgeInsets.all(0),
        color: MyTheme.cyanColor
            .withAlpha(255)
            .withOpacity(0.5), //Colors.transparent.withOpacity(0),
        //elevation: 5.0,
        child: Center(child: widget),
      ),
    );
  }

  @override
  double get maxExtent => h;

  @override
  double get minExtent => h;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
