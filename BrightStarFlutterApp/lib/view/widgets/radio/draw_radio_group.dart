import 'package:flutter/material.dart';
import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

enum eRadioType {
  VERTICAL,
  HORIZONTAL,
  GRID2,
}

drawRadioGroup(
    {eRadioType type = eRadioType.VERTICAL,
    double txtSize = 15,
    bool isMainBG = false,
    //
    @required Map<int, String> map,
    @required int selected,
    @required Function(int) callback}) {
  //final width = MediaQuery.of(context).size.width;

  return type == eRadioType.VERTICAL
      ? Container(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          for (var entry in map.entries)
            GestureDetector(
              onTap: () {
                callback(entry.key);
              },
              child: _radioitem(
                  isMainBG: isMainBG,
                  type: type,
                  text: entry.value,
                  txtSize: txtSize,
                  isSelected: entry.key == selected,
                  bgColor: entry.key == selected
                      ? MyTheme.purpleColor
                      : Color(0xFF000),
                  textColor: Colors.black),
            )
        ]))
      : type == eRadioType.HORIZONTAL
          ? SingleChildScrollView(
              //shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    for (var entry in map.entries)
                      GestureDetector(
                          onTap: () {
                            callback(entry.key);
                          },
                          child: Container(
                            //color: Colors.black,
                            width: _textSize(
                                    entry.value,
                                    TextStyle(
                                      fontSize: txtSize,
                                    )).width +
                                40,
                            child: _radioitem(
                                isMainBG: isMainBG,
                                type: type,
                                text: entry.value,
                                txtSize: txtSize,
                                isSelected: entry.key == selected,
                                bgColor: entry.key == selected
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black),
                          ))
                  ]))
          : GridView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: map.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisExtent: 50,
              ),
              itemBuilder: (_, index) {
                final value = map[index];
                return GestureDetector(
                    onTap: () {
                      callback(index);
                    },
                    child: Container(
                      //color: Colors.black,
                      width: _textSize(
                              value,
                              TextStyle(
                                fontSize: txtSize,
                              )).width +
                          40,
                      child: _radioitem(
                          isMainBG: isMainBG,
                          type: type,
                          text: value,
                          txtSize: txtSize,
                          isSelected: index == selected,
                          bgColor: index == selected
                              ? MyTheme.purpleColor
                              : Color(0xFF000),
                          textColor: Colors.black),
                    ));
              });
}

_radioitem({
  eRadioType type,
  bool isMainBG,
  bool isSelected,
  String text,
  double txtSize = 1.5,
  Color bgColor,
  Color textColor,
}) {
  return Padding(
    padding: EdgeInsets.all(type == eRadioType.VERTICAL && !isMainBG ? 0 : 2),
    child: Container(
      decoration: isMainBG
          ? BoxDecoration(
              color: isSelected ? MyTheme.radioBGColor : Colors.grey,
              borderRadius: BorderRadius.circular(5),
            )
          : null,
      child: Padding(
        padding: EdgeInsets.only(
            left: isMainBG ? 5 : 0,
            top: type == eRadioType.VERTICAL ? 5 : 0,
            bottom: type == eRadioType.VERTICAL ? 5 : 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              width: 15,
              height: 15,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(3),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      border: Border(
                          left:
                              BorderSide(color: MyTheme.purpleColor, width: 1),
                          right:
                              BorderSide(color: MyTheme.purpleColor, width: 1),
                          top: BorderSide(color: MyTheme.purpleColor, width: 1),
                          bottom:
                              BorderSide(color: MyTheme.purpleColor, width: 1)),
                      color: bgColor),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Text(
                  text,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      color: isMainBG ? Colors.white : textColor,
                      fontSize: txtSize,
                      fontWeight: isMainBG
                          ? isSelected
                              ? FontWeight.w500
                              : FontWeight.normal
                          : FontWeight.normal),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

//  get intrinsic text width as per font size
Size _textSize(String text, TextStyle style) {
  final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: style),
      maxLines: 1,
      textDirection: TextDirection.ltr)
    ..layout(minWidth: 0, maxWidth: double.infinity);
  return textPainter.size;
}
