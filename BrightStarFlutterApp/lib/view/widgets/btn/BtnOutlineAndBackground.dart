import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class BtnOutlineAndBackground extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Color backgroundColor;
  final Function callback;

  const BtnOutlineAndBackground({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.backgroundColor,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        height: getHP(context, 5),
        elevation: 0.0,
        child: AutoSizeText(
          txt,
          maxLines: 1,
          textAlign: TextAlign.center,
          style: TextStyle(color: txtColor),
        ),
        onPressed: () {
          callback();
        },
        color: backgroundColor,
        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(10.0),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: 1.0, color: borderColor),
        ),

        //color: Colors.black,
      ),
    );
  }
}
