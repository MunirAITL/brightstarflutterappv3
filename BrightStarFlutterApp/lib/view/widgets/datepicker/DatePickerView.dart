import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/IcoTxtIco.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class DatePickerView extends StatelessWidget {
  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final String cap;
  final String dt;
  Color txtColor;
  final Function callback;

  DatePickerView({
    Key key,
    @required this.cap,
    @required this.dt,
    @required this.callback,
    @required this.initialDate,
    @required this.firstDate,
    @required this.lastDate,
    this.txtColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          (this.cap != null)
              ? Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Txt(
                    txt: this.cap,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    fontWeight: FontWeight.w500,
                    isBold: false,
                  ),
                )
              : SizedBox(),
          GestureDetector(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: firstDate,
                lastDate: lastDate,
                fieldHintText: "dd/mm/yyyy",
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme:
                          ColorScheme.light(primary: MyTheme.brandColor),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: IcoTxtIco(
              leftIcon: Icons.calendar_today,
              txt: this.dt,
              txtColor: txtColor,
              rightIcon: Icons.keyboard_arrow_down,
              txtAlign: TextAlign.left,
              rightIconSize: 30,
              leftIconSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
