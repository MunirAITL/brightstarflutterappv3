import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';

SwitchView({bool isSelected, Function(bool) onChanged, List<String> list}) {
// Here, default theme colors are used for activeBgColor, activeFgColor, inactiveBgColor and inactiveFgColor
  return ToggleSwitch(
    initialLabelIndex: 1,
    //borderColor: [MyTheme.brandColor],
    //borderWidth: 1,
    totalSwitches: list.length,
    labels: list,
    fontSize: 14,
    //activeBgColor: [Colors.green],
    activeFgColor: (isSelected) ? Colors.black : Colors.white,
    //inactiveBgColor: Colors.grey,
    inactiveFgColor: (!isSelected) ? Colors.black : Colors.white,
    //radiusStyle: true,
    cornerRadius: 20,
    inactiveBgColor: (isSelected) ? MyTheme.brandColor : Colors.grey,
    activeBgColor: (!isSelected) ? [MyTheme.brandColor] : [Colors.grey],
    onToggle: (index) {
      //print('switched to: $index');
      switch (index) {
        case 0:
          onChanged(true);
          break;
        case 1:
          onChanged(false);
          break;
        default:
      }
    },
  );
}
/*
class SwitchView extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  const SwitchView({
    Key key,
    @required this.value,
    @required this.onChanged,
  }) : super(key: key);

  @override
  State createState() => _SwitchViewState();
}

class _SwitchViewState extends State<SwitchView> with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      //width: getWP(context, 50),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: new BoxDecoration(
                  color: (widget.value) ? MyTheme.brandColor : Colors.grey,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: "Yes",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize - 0.6,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(true);
                },
              ),
            ),
          ),
          Expanded(
            child: Container(
              decoration: new BoxDecoration(
                  color: (!widget.value) ? MyTheme.brandColor : Colors.grey,
                  borderRadius: new BorderRadius.only(
                    topRight: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: "No",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize - 0.6,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(false);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}*/
