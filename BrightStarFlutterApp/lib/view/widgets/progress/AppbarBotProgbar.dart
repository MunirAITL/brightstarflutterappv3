import 'package:flutter/material.dart';

class AppbarBotProgBar extends LinearProgressIndicator
    implements PreferredSizeWidget {
  AppbarBotProgBar({
    Key key,
    double value,
    Color backgroundColor,
    Animation<Color> valueColor,
  }) : super(
          key: key,
          value: value,
          backgroundColor: backgroundColor,
          valueColor: valueColor,
          minHeight: 1,
        ) {
    preferredSize = Size(double.infinity, .5);
  }

  @override
  Size preferredSize;
}
