import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScanHelper {
  drawLine({Color colr = Colors.grey, double h = .5}) {
    return Container(color: colr, height: h);
  }

  scrollUp(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  scrollDown(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        scrollController.position.maxScrollExtent + h + 50,
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  drawTopbar(bool isShowArrow, String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 40, top: 20),
      child: Container(
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
            Container(
              child: (isShowArrow)
                  ? IconButton(
                      icon: Image.asset(
                        "assets/images/icons/ico_arrow_back.png",
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    )
                  : SizedBox(),
            ),
            Expanded(
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
          ])),
    );
  }

  drawCircle({BuildContext context, Color color, double size = 3}) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;
    //var padding = MediaQuery.of(context).padding;
    //double newheight = height - padding.top - padding.bottom;
    return Container(
      width: width * size / 100,
      height: width * size / 100,
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
    );
  }

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            (i < rate) ? Icons.star : Icons.star_outline,
            color: colr,
            size: 20,
          ),
      ],
    );
  }

  expandableTxt(String txt, Color txtColor, Color arrowColor) {
    final txt1 = txt.substring(0, txt.indexOf(".")).trim();
    final txt2 = txt.substring(txt.indexOf(".") + 1).trim();

    return Container(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        iconColor: arrowColor,
        child: Theme(
          data: ThemeData.light()
              .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
          child: ExpansionTile(
            title: Txt(
                txt: txt1 + '...',
                txtColor: txtColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                maxLines: 3,
                isBold: false),
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Txt(
                      txt: txt2,
                      txtColor: txtColor,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
