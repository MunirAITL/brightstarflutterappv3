import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/auth/LoginScreen.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/db_cus/more/settings/CaseAlertScreen.dart';
import 'package:aitl/view/widgets/images/ImgFade.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

import '../../config/db_intro/intro_cfg.dart';
import '../db_intr/MainIntroducerScreen.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  State createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  // static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool isDoneCookieCheck = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      setStatusBarTheme();

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();

          if (!Server.isOtp) {
            if (userData.userModel.communityID ==
                IntroCfg.INTRO_COMMUNITYID.toString()) {
              appData.isIntroducer = true;
              Get.off(() => MainIntroducerScreen()).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });
            } else {
              appData.isIntroducer = false;
              Get.off(
                () => MainCustomerScreen(),
              ).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });
            }
          } else if (userData.userModel.isMobileNumberVerified) {
            if (userData.userModel.communityID ==
                IntroCfg.INTRO_COMMUNITYID.toString()) {
              appData.isIntroducer = true;
              Get.off(() => MainIntroducerScreen()).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });
            } else {
              appData.isIntroducer = false;
              Get.off(
                () => MainCustomerScreen(),
              ).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });
            }
          } else {
            setState(() {
              isDoneCookieCheck = true;
            });
          }
        } else {
          setState(() {
            isDoneCookieCheck = true;
          });
        }
      } catch (e) {}
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        Get.off(
          () => CaseAlertScreen(
            message: message,
          ),
        ).then((value) async {
          await userData.setUserModel();
          if (userData.userModel.communityID ==
              IntroCfg.INTRO_COMMUNITYID.toString()) {
            appData.isIntroducer = true;
            Get.to(() => MainIntroducerScreen()).then((value) {
              setState(() {
                isDoneCookieCheck = true;
              });
            });
          } else {
            appData.isIntroducer = false;
            Get.off(
              () => MainCustomerScreen(),
            ).then((value) {
              setState(() {
                isDoneCookieCheck = true;
              });
            });
          }
        });
      }
    } catch (e) {
      //log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    final x = getW(context);
    myLog(x.toString());
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: MyTheme.themeData.accentColor,
          resizeToAvoidBottomInset: true,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? Center(
                    child: Container(
                        width: getWP(context, 50),
                        child: Image.asset(
                          'assets/images/logo/logo.png',
                          fit: BoxFit.fitWidth,
                        )),
                  )
                : Container(
                    height: getH(context),
                    child: Stack(children: <Widget>[
                      new Container(
                        height: getHP(context, 70),
                        /*decoration: new BoxDecoration(
                          image: new DecorationImage(
                            image: new AssetImage(
                                "assets/images/screens/splash/splash_bs.png"),
                            fit: BoxFit.cover,
                          ),
                        ),*/
                        child: ImgFade(
                            url: "assets/images/screens/splash/splash_bs.png"),
                      ),
                      Positioned(
                        top: 0,
                        child: Stack(children: <Widget>[
                          Container(
                            color: Colors.white,
                            width: getW(context),
                            height: getHP(context, 10),
                          ),
                          Positioned(
                            left: getWP(context, 5),
                            top: getHP(context, 1),
                            child: Image.asset(
                              "assets/images/logo/logo.png",
                              width: getWP(context, MyTheme.logoWidth),
                            ),
                          ),
                        ]),
                      ),
                      Positioned(
                        top: getHP(context, 80),
                        child: Container(
                          width: getW(context),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 30, right: 30),
                            child: MaterialButton(
                              color: MyTheme.cyanColor,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 15, bottom: 15),
                                child: new Txt(
                                  txt: "Get Started",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: true,
                                ),
                              ),
                              onPressed: () async {
                                Get.to(
                                  () => LoginScreen(),
                                ).then((value) {
                                  //callback(route);
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    ]),
                  ),
          ),
        ),
      ),
    );
  }
}
