import 'dart:io';

import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/my_cases/MyCasesAPIMgr.dart';
import 'package:aitl/controller/api/misc/DeviceInfoAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/view/common/ActionReqPage.dart';
import 'package:aitl/view/common/RefClientPage.dart';
import 'package:aitl/view/db_cus/my_cases/MyCaseTab.dart';
import 'package:aitl/view/db_cus/new_case/PostNewCaseScreen.dart';
import 'package:aitl/view/db_intr/createlead/leaddetails/create_lead.dart';
import 'package:aitl/view/db_intr/mycases/mycases_tab.dart';
import 'package:aitl/view/welcome/WelcomeScreen.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/drawer/AppDrawer.dart';
import 'package:aitl/view/widgets/dropdown/DropDownPicker.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:new_version/new_version.dart';
import 'package:url_launcher/url_launcher.dart';

class MainCustomerScreen extends StatefulWidget {
  @override
  State createState() => MainCustomerScreenState();
}

class MainCustomerScreenState extends State<MainCustomerScreen>
    with Mixin, StateListener, TickerProviderStateMixin {
  var _androidAppRetain = MethodChannel("android_app_retain");
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  final GlobalKey _ddKey = GlobalKey();
  final _controller = ScrollController();

  static int currentTab = 0;

  StateProvider _stateProvider;

  //  page stuff start here
  int caseStatus = NewCaseCfg.ALL;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = 4;
  List<LocationsModel> listTaskInfoSearchModel = [];

  //  dropdown title
  DropListModel caseDD = DropListModel([]);
  OptionItem caseOpt = OptionItem(id: null, title: "I am looking for...");
  Size caseDDSize;

  final mapAdCTA = [
    {
      'btnTitle': 'Ad CTA1',
      'headline': 'Ad headline',
      'content': 'Ad content',
    },
    {
      'btnTitle': 'Ad CTA2',
      'headline': 'Ad headline',
      'content': 'Ad content',
    },
  ];

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_usercase_reload_case_api) {
      await refreshData();
      checkNewerVersion();
    } else if (state == ObserverState.STATE_CHANGED_logout) {
      await CookieMgr().delCookiee();
      await DBMgr.shared.delTable("User");
      Get.offAll(() => WelcomeScreen());
    }
  }

  checkNewerVersion() async {
    // Instantiate NewVersion manager object (Using GCP Console app as example)
    if (mounted) {
      final newVersion = NewVersion(
        iOSId: 'uk.co.brightstarhub.clients',
        androidId: 'uk.co.brightstarhub.clients',
      );
      newVersion.showAlertIfNecessary(context: context);
    }
  }

  Future<void> refreshData() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        await MyCasesAPIMgr().wsOnPageLoad(
          context: context,
          pageStart: 0,
          pageCount: pageCount,
          caseStatus: NewCaseCfg.ALL,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    listTaskInfoSearchModel.clear();
                    final List<dynamic> locations =
                        model.responseData.locations;
                    for (LocationsModel location in locations) {
                      listTaskInfoSearchModel.add(location);
                    }
                  } catch (e) {}
                }
              } catch (e) {}
            }
          },
        );
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    listTaskInfoSearchModel = null;
    _controller.dispose();
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  getSizeAndPosition() {
    if (mounted) {
      RenderBox _cardBox = _ddKey.currentContext.findRenderObject();
      caseDDSize = _cardBox.size;
      myLog(caseDDSize.height.toString());
      setState(() {});
    }
  }

  appInit() async {
    try {
      int i = 0;
      List<OptionItem> list = [];
      for (var map in NewCaseCfg.listCreateNewCase) {
        if (map['title'] != 'Others')
          list.add(OptionItem(id: (i++).toString(), title: map['title']));
      }
      caseDD = DropListModel(list);
    } catch (e) {}

    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      getSizeAndPosition();
      await refreshData();
      checkNewerVersion();
    });

    try {
      DeviceInfoAPIMgr().wsFcmDeviceInfo(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {}
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog("not in");
          }
        },
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          if (Platform.isAndroid) {
            if (Navigator.of(context).canPop()) {
              return Future.value(true);
            } else {
              _androidAppRetain.invokeMethod("sendToBackground");
              return Future.value(false);
            }
          } else {
            return Future.value(true);
          }
        },
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            key: _drawerKey,
            //drawerScrimColor: Colors.grey.withOpacity(0),
            endDrawer: AppDrawer(
              isRightClose: true,
              isNewCaseTab: true,
              isMyCasesTab: false,
              isTimelineTab: false,
              isNotiTab: false,
            ),
            backgroundColor: MyTheme.themeData.accentColor,
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (detail) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      //expandedHeight: 160,
                      elevation: 0,
                      toolbarHeight: getHP(context, 10),
                      backgroundColor: MyTheme.appbarColor,
                      iconTheme: IconThemeData(
                          color: MyTheme.brandColor //change your color here
                          ),
                      pinned: true,
                      floating: false,
                      snap: false,
                      forceElevated: false,
                      centerTitle: false,
                      title: Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Container(
                          width: getWP(context, MyTheme.logoWidth),
                          child: Image.asset(
                            "assets/images/logo/logo.png",
                            //fit: BoxFit.fitWidth,
                            //width: getWP(context, 80),
                          ),
                        ),
                      ),
                      actions: [
                        Builder(
                          builder: (context) => Padding(
                            padding: const EdgeInsets.only(right: 30),
                            child: GestureDetector(
                              onTap: () {
                                Scaffold.of(context).openEndDrawer();
                              },
                              child: Image.asset(
                                "assets/images/icons/menu_icon.png",
                                width: 25,
                              ),
                            ),
                          ),
                        ),
                      ],

                      bottom: PreferredSize(
                          preferredSize: new Size(getW(context),
                              getHP(context, (isLoading) ? .5 : 0)),
                          child: (isLoading)
                              ? AppbarBotProgBar(
                                  backgroundColor: MyTheme.appbarProgColor,
                                )
                              : SizedBox()),
                    ),
                  ];
                },
                body: drawLayout(),
              ),
            ),
          ),
        ));
  }

  drawLayout() {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: ListView(
            // shrinkWrap: true,
            // primary: true,
            controller: _controller,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  color: Colors.white,
                  child: Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //SizedBox(height: 10),
                          GestureDetector(
                            onTap: () {
                              // Get.to(MyCaseTab());
                            },
                            child: Txt(
                              txt: "Hello " + userData.userModel.firstName ??
                                  '' + ",",
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize + 1.3,
                              txtAlign: TextAlign.start,
                              isBold: false,
                            ),
                          ),
                          SizedBox(height: 10),
                          Txt(
                            txt: "welcome back",
                            txtColor: MyTheme.brandColor,
                            txtSize: MyTheme.txtSize + 1,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                          //SizedBox(height: 10),
                        ],
                      )),
                ),
              ),
              //(isShowSlider) ? drawGridImages() : SizedBox(),
              SizedBox(height: 20),
              drawButtons(),
              drawAdCTA(),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  decoration: BoxDecoration(
                      color: MyTheme.cyanColor,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Column(
                    //crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(top: 20, left: 20, right: 20),
                        child: Txt(
                          txt: "Want to submit a new case?",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10, top: 20, bottom: 40),
                        child: Container(
                          width: getW(context),
                          child: Stack(
                            //alignment: Alignment.center,
                            //mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                key: _ddKey,
                                width: getWP(context, 45),
                                //height: getHP(context, 10),
                                //color: Colors.black,
                                child: DropDownPicker(
                                  cap: null,
                                  bgColor: Colors.white,
                                  txtColor: (caseOpt.id == null)
                                      ? HexColor.fromHex("#85868C")
                                      : Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  ddTitleSize: .4,
                                  ddRadius: 5,
                                  itemSelected: caseOpt,
                                  dropListModel: caseDD,
                                  onOptionSelected: (optionItem) {
                                    caseOpt = optionItem;
                                    _controller.animateTo(
                                      0.0,
                                      curve: Curves.easeOut,
                                      duration:
                                          const Duration(milliseconds: 300),
                                    );
                                    setState(() {});
                                  },
                                ),
                              ),
                              SizedBox(height: 15),
                              (caseDDSize == null)
                                  ? SizedBox()
                                  : Positioned(
                                      right: getWP(context, 2.5),
                                      top: 5,
                                      child: Container(
                                        width: getWP(context, 30),
                                        //alignment: Alignment.center,
                                        //color: Colors.black,
                                        child: BSBtn(
                                            txt: "Go",
                                            radius: 5,
                                            leadingSpace: 20,
                                            height: caseDDSize.height - 10,
                                            callback: () {
                                              if (caseOpt.id != null) {
                                                Get.to(
                                                  () => PostNewCaseScreen(
                                                    indexCase:
                                                        int.parse(caseOpt.id),
                                                  ),
                                                ).then((value) {
                                                  //callback(route);
                                                });
                                              } else {
                                                showToast(
                                                    context: context,
                                                    msg:
                                                        "Please choose case from the list");
                                              }
                                            }),
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              /* Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Container(
                  color: MyTheme.cyanColor,
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                        child: Txt(
                          txt: "Request a call back",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                        child: Container(
                          width: getWP(context, 25),
                          child: BSBtn(
                              txt: "Go",
                              radius: 5,
                              //leadingSpace: 20,
                              height: getHP(context, 5),
                              callback: () {
                                //
                              }),
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),*/

              (listTaskInfoSearchModel.length > 0)
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Txt(
                            txt: "Recent Cases",
                            txtColor: MyTheme.brandColor,
                            txtSize: MyTheme.txtSize + .2,
                            txtAlign: TextAlign.start,
                            isBold: true,
                          ),
                        ),
                        drawRecentCases(),
                      ],
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ],
    );
  }

  drawRecentCases() {
    try {
      return Container(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
              child: Container(
                color: MyTheme.brandColor,
                height: getHP(context, 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 18.0),
                          child: Txt(
                              txt: "TYPE",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - 1,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )),
                    Expanded(
                      flex: 3,
                      child: Txt(
                          txt: "CASE No",
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - 1,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                    Expanded(
                      flex: 3,
                      child: Txt(
                          txt: "APPLICATION STARTED",
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - 1,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  color: Colors.white,
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: listTaskInfoSearchModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawRecentCaseItem(index);
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } catch (e) {}
  }

  drawRecentCaseItem(index) {
    try {
      LocationsModel locationsModel = listTaskInfoSearchModel[index];
      if (locationsModel == null) return SizedBox();

      var now = Jiffy(locationsModel.creationDate).format("dd/MM/yy");

      return index < pageCount - 1
          ? GestureDetector(
              onTap: () async {
                try {
                  Get.to(
                    () => WebScreen(
                      title: locationsModel.title,
                      caseID: locationsModel.id.toString(),
                      url: CaseDetailsWebHelper().getLink(
                          title: locationsModel.title,
                          taskId: locationsModel.id),
                    ),
                    //fullscreenDialog: true,
                  );
                } catch (e) {}
              },
              child: Container(
                //color: Colors.black,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Row(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 3,
                              child: Container(
                                  //width: getWP(context, 25),
                                  //color: Colors.yellow,
                                  child: Txt(
                                txt: locationsModel.title,
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - 0.4,
                                txtAlign: TextAlign.start,
                                isBold: false,
                                isOverflow: true,
                              )),
                            ),
                            Expanded(
                              flex: 3,
                              child: Txt(
                                  txt: locationsModel.id.toString(),
                                  txtColor: MyTheme.brandColor,
                                  txtSize: MyTheme.txtSize - 0.4,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Expanded(
                              flex: 3,
                              child: Txt(
                                  txt: now,
                                  txtColor: MyTheme.brandColor,
                                  txtSize: MyTheme.txtSize - 0.4,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Icon(
                                Icons.arrow_forward,
                                color: Colors.black54,
                              ),
                            ),
                            //SizedBox(width: 5),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            bottom: 10, top: 10, right: 10),
                        child: Container(color: Colors.grey, height: .5),
                      )
                    ],
                  ),
                ),
              ),
            )
          : GestureDetector(
              onTap: () {
                Get.to(MyCaseTab());
              },
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  color: Colors.transparent,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Txt(
                          txt: "More...",
                          txtColor: MyTheme.brandColor,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                ),
              ),
            );
    } catch (e) {
      myLog(e.toString());
    }
  }

  /*
  drawRecentCases() {
    try {
      return Padding(
        padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
        child: Container(
          color: Colors.white,
          child: Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Container(
                  color: MyTheme.brandColor,
                  height: getHP(context, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Txt(txt: "TYPE", txtColor: Colors.white, txtSize: MyTheme.txtSize - 1, txtAlign: TextAlign.center, isBold: false),
                        ),
                      ),
                      Flexible(
                        child: Txt(txt: "CASE No", txtColor: Colors.white, txtSize: MyTheme.txtSize - 1, txtAlign: TextAlign.center, isBold: false),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Txt(txt: "APPLICATION STARTED", txtColor: Colors.white, txtSize: MyTheme.txtSize - 1, txtAlign: TextAlign.center, isBold: false),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: listTaskInfoSearchModel.length,
                    itemBuilder: (context, index) {
                      final LocationsModel userNotesModel = listTaskInfoSearchModel[index] ;

                      var now = Jiffy(userNotesModel.creationDate).format("dd/MM/yy");

                      return GestureDetector(
                        onTap: () async {
                      */ /*    Get.to(() => CaseNoteDoneScreen(userNotesModel: userNotesModel)).then((isOk) {
                            if (isOk) {
                              //  api call
                              UserCaseAPIMgr().wsCaseNoteDone(
                                  context: context,
                                  userNoteModel: userNotesModel,
                                  callback: (model) {
                                    if (model != null && mounted) {
                                      try {
                                        if (model.success) {
                                          try {
                                            for (UserNotesModel userNoteModel2 in listUserNotesModel) {
                                              if (userNoteModel2.id == model.responseData.userNote.id) {
                                                listUserNotesModel.remove(userNoteModel2);
                                                break;
                                              }
                                            }
                                            setState(() {});
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        } else {
                                          try {
                                            if (mounted) {
                                              final err = model.messages.post_user[0].toString();
                                              showToast(txtColor: Colors.white, bgColor: MyTheme.brandColor, msg: err);
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        }
                                      } catch (e) {
                                        myLog(e.toString());
                                      }
                                    }
                                  });
                            }
                          });
                    */ /*
                        },
                        child: Container(
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Column(
                              children: [
                                Row(
                                  //crossAxisAlignment: CrossAxisAlignment.start,
                                  //mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                          //width: getWP(context, 25),
                                          //color: Colors.yellow,
                                          child: Txt(
                                        txt: userNotesModel.title,
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                        isOverflow: true,
                                      )),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Txt(
                                          txt: userNotesModel.id.toString(),
                                          txtColor: MyTheme.brandColor,
                                          txtSize: MyTheme.txtSize - 0.4,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                    //SizedBox(width: 10),
                                    Expanded(
                                      flex: 3,
                                      child: Txt(txt: now, txtColor: MyTheme.brandColor, txtSize: MyTheme.txtSize - 0.4, txtAlign: TextAlign.center, isBold: false),
                                    ),
                                    Flexible(
                                      child: Icon(
                                        Icons.arrow_forward,
                                        color: Colors.black54,
                                      ),
                                      */ /*onPressed: () async {
                                        Get.to(
                                          () => TaskBiddingScreen(
                                                                  title: caseModel.title,
                                                                  taskId: caseModel.id),
                                        ).then((value) {
                                          //callback(route);
                                        });

                                        },*/ /*
                                      //),
                                    ),
                                    //SizedBox(width: 5),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 10, top: 20),
                                  child: Container(color: Colors.grey, height: .5),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
              //SizedBox(height: 20),
            ],
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
      return Container();
    }
  }*/

  drawButtons() {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          appData.isIntroducer
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BSBtn(
                        txt: "Refer a client",
                        radius: 5,
                        leadingSpace: 20,
                        height: getHP(context, 7),
                        callback: () {
                          Get.to(() => RefClientPage());
                        }),
                    SizedBox(height: 5),
                    BSBtn(
                        txt: "Submit an enquiry",
                        radius: 5,
                        leadingSpace: 20,
                        height: getHP(context, 7),
                        callback: () {
                          Get.to(() => CreateLeadPage());
                        }),
                    SizedBox(height: 5),
                    BSBtn(
                        txt: "Manage an existing case",
                        radius: 5,
                        leadingSpace: 20,
                        height: getHP(context, 7),
                        callback: () {
                          Get.to(() => MyCasesTab());
                        }),
                    SizedBox(height: 5),
                    BSBtn(
                        txt: "Request a call back",
                        radius: 5,
                        leadingSpace: 20,
                        height: getHP(context, 7),
                        callback: () {
                          final Uri emailLaunchUri = Uri(
                            scheme: 'mailto',
                            path: AppDefine.SUPPORT_EMAIL,
                            query: encodeQueryParameters(<String, String>{
                              'subject': 'Request a call back'
                            }),
                          );
                          launchUrl(emailLaunchUri);
                        }),
                    SizedBox(height: 5),
                  ],
                )
              : SizedBox(),
          BSBtn(
              txt: "Action Required",
              radius: 5,
              leadingSpace: 20,
              height: getHP(context, 7),
              callback: () {
                Get.to(() => ActionReqPage());
              }),
        ],
      ),
    );
  }

  drawAdCTA() {
    return Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: SingleChildScrollView(
            //shrinkWrap: true,
            primary: false,
            scrollDirection: Axis.horizontal,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                  txt: "Sponsored",
                  isOverflow: true,
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize + .2,
                  txtAlign: TextAlign.center,
                  isBold: true,
                ),
                SizedBox(height: 10),
                IntrinsicHeight(
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        for (var map in mapAdCTA)
                          GestureDetector(
                              onTap: () {
                                //callback(entry.key);
                              },
                              child: Container(
                                  //color: Colors.black,
                                  width: getWP(context, 46),
                                  //height: getHP(context, 15),
                                  child: Card(
                                      elevation: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.all(10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Txt(
                                              txt: map['headline'],
                                              isOverflow: true,
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .2,
                                              txtAlign: TextAlign.center,
                                              isBold: true,
                                            ),
                                            SizedBox(height: 10),
                                            Txt(
                                              txt: map['content'],
                                              isOverflow: true,
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .4,
                                              txtAlign: TextAlign.center,
                                              isBold: false,
                                            ),
                                            SizedBox(height: 10),
                                            BSBtn(
                                                txt: map['btnTitle'],
                                                radius: 5,
                                                leadingSpace: 20,
                                                height: getHP(context, 4),
                                                txtSize: MyTheme.txtSize - .4,
                                                callback: () {
                                                  //Get.to(() => ActionReqPage());
                                                  showAlert(
                                                      context: context,
                                                      msg: 'Not Authorized');
                                                }),
                                          ],
                                        ),
                                      ))))
                      ]),
                )
              ],
            )));
  }
}
