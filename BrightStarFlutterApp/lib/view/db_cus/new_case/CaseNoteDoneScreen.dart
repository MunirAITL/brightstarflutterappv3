import 'package:aitl/mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotesModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CaseNoteDoneScreen extends StatefulWidget {
  final UserNotesModel userNotesModel;
  const CaseNoteDoneScreen({
    Key key,
    @required this.userNotesModel,
  }) : super(key: key);
  @override
  State createState() => _CaseNoteDoneScreenState();
}

class _CaseNoteDoneScreenState extends State<CaseNoteDoneScreen> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back(result: false);
                      }),
                  title: UIHelper().drawAppbarTitle(title: "Task Details"),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getWP(context, 80),
      //height: getHP(context, 55),
      child: ListView(
        shrinkWrap: true,
        children: [
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 40, right: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                drawInfoBox("Case No:", widget.userNotesModel.id.toString()),
                SizedBox(height: 20),
                drawInfoBox("Title:", widget.userNotesModel.title),
                SizedBox(height: 20),
                drawInfoBox("Description:", widget.userNotesModel.comments),
                SizedBox(height: 20),
                drawInfoBox(
                    "Assigned by:", widget.userNotesModel.initiatorName),
                /*SizedBox(height: 20),
                Txt(
                    txt:
                        "NB: After completing the task, please click the 'YES' Button Otherwise click the 'No' Button.",
                    txtColor: Colors.black,
                    txtSize: 1.8,
                    txtAlign: TextAlign.center,
                    isBold: false),*/

                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Btn(
                        txt: "No",
                        txtColor: Colors.black,
                        bgColor: Colors.grey,
                        width: getWP(context, 25),
                        height: getHP(context, 5),
                        callback: () {
                          Get.back(result: false);
                        }),
                    Btn(
                        txt: "Yes",
                        txtColor: Colors.white,
                        bgColor: MyTheme.brandColor,
                        width: getWP(context, 25),
                        height: getHP(context, 5),
                        callback: () {
                          Get.back(result: true);
                        }),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawInfoBox(String title, String val) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Txt(
            txt: title,
            txtColor: Colors.black,
            txtSize: 1.8,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(width: 10),
        Expanded(
          child: Txt(
              txt: val,
              txtColor: Colors.black,
              txtSize: 1.8,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
      ],
    );
  }
}
