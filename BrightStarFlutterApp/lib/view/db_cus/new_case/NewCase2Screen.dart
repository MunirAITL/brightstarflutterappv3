import 'package:aitl/mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/new_case/PostNewCaseScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewCase2Screen extends StatefulWidget {
  @override
  State createState() => _NewCase2ScreenState();
}

class _NewCase2ScreenState extends State<NewCase2Screen> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: UIHelper().drawAppbarTitle(title: "Create New"),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                ),
              ];
            },
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (detail) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: drawLayout(),
            ),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        //width: double.infinity,
        //height: getH(context),
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: NewCaseCfg.listCreateNewCase.length,
            itemBuilder: (context, index) {
              final Map<String, dynamic> map =
                  NewCaseCfg.listCreateNewCase[index];
              final icon = map["url"];
              final title = map["title"];
              if (map['title'] == 'Others') return SizedBox();
              return GestureDetector(
                onTap: () {
                  Get.off(
                    () => PostNewCaseScreen(
                      indexCase: index,
                    ),
                  ).then((value) {
                    //callback(route);
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 5),
                  child: Card(
                    elevation: 2,
                    //margin: EdgeInsets.symmetric(vertical: 10),
                    //color: Colors.black,
                    //height: getHP(context, 12),
                    /*decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          //spreadRadius: 0,
                          //blurRadius: 0,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),*/
                    //color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListTile(
                            //crossAxisAlignment: CrossAxisAlignment.center,
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //children: [
                            title: Txt(
                                txt: title,
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: true),

                            trailing: Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Container(
                                //width: boxW,
                                //height: boxW,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage(icon),
                                  radius: 30,
                                  backgroundColor: Colors.transparent,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
