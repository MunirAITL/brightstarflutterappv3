import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/new_case/PostCaseAPIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/PostNewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotesModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:aitl/view/widgets/views/OtherApplicantSwitchView.dart';
import 'package:aitl/view/widgets/views/SPVSwitchView.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostNewCaseScreen extends StatefulWidget {
  final int indexCase;

  const PostNewCaseScreen({
    Key key,
    @required this.indexCase,
  }) : super(key: key);

  @override
  State createState() => _PostNewCaseScreenState();
}

class _PostNewCaseScreenState extends State<PostNewCaseScreen> with Mixin {
  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];
  List<FocusNode> listFocusNode = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  final _regNo = TextEditingController();
  final focusCompName = FocusNode();
  final focusRegNo = FocusNode();

  String _regAddr = "";
  String regDate = "";

  var title = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  int otherApplicantRadioIndex = 1;

  validate() {
    if (isOtherApplicantSwitch) {
      int i = 0;
      while (i < otherApplicantRadioIndex) {
        if (!UserProfileVal().isEmailOK(
            context,
            listOApplicantInputFieldsCtr[i],
            "Invalid " +
                MyDefine.ORDINAL_NOS[i + 1] +
                " applicant email address")) {
          return false;
        }
        i++;
      }
    }
    if (isSPVSwitch) {
      if (UserProfileVal()
          .isEmpty(context, _compName, "Missing company name")) {
        return false;
      } else if (_regAddr == "") {
        showToast(context: context, msg: "Missing company registered address");
        return false;
      } else if (_regNo.text.isEmpty) {
        showToast(
            context: context, msg: "Missing company registeration number");
        return false;
      } else if (regDate == "") {
        showToast(context: context, msg: "Please select registered date");
        return false;
      }
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _compName.dispose();
    _regNo.dispose();
    listOApplicantInputFieldsCtr = null;
    listFocusNode = null;
    _regAddr = null;
    regDate = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
    } catch (e) {}
    try {
      final map = NewCaseCfg.listCreateNewCase[widget.indexCase];
      //final icon = map["url"];
      title = map["title"];
      if (map["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (map["isSPV"]) {
        isSPVSwitchShow = true;
      }
    } catch (e) {}
    try {
      _compName.addListener(() {
        myLog(_compName.text);
      });
      _regNo.addListener(() {
        myLog(_regNo.text);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: UIHelper().drawAppbarTitle(title: "Create Case"),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                ),
              ];
            },
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (detail) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: drawLayout(),
            ),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //height: getH(context),
      child: SingleChildScrollView(
        primary: true,
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
            children: [
              SizedBox(height: getHP(context, 5)),
              drawCaseType(),
              SizedBox(height: 30),
              (isOtherApplicantSwitchShow)
                  ? OtherApplicantSwitchView(
                      listOApplicantInputFieldsCtr:
                          listOApplicantInputFieldsCtr,
                      listFocusNode: listFocusNode,
                      isSwitch: isOtherApplicantSwitch,
                      callback: (value) {
                        otherApplicantRadioIndex = value;
                        myLog(value);
                      },
                      callbackSwitch: (isSwitch_1) {
                        isOtherApplicantSwitch = isSwitch_1;
                      },
                    )
                  : SizedBox(),
              (isSPVSwitchShow)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: SPVSwitchView(
                        focusCompName: focusCompName,
                        focusRegNo: focusRegNo,
                        compName: _compName,
                        regAddr: _regAddr,
                        regNo: _regNo,
                        regDate: regDate,
                        isSwitch: isSPVSwitch,
                        callback: (value) {
                          regDate = value;
                        },
                        callbackSwitch: (isSwitch_2) {
                          isSPVSwitch = isSwitch_2;
                        },
                        callbackAddress: (address) {
                          _regAddr = address;
                          setState(() {});
                        },
                      ),
                    )
                  : SizedBox(),
              Padding(
                padding: const EdgeInsets.only(
                    top: 40, left: 20, right: 20, bottom: 20),
                child: BSBtn(
                  txt: "Create Case",
                  height: getHP(context, 6),
                  //width: getW(context),
                  callback: () {
                    if (validate()) {
                      final param = PostNewCaseHelper().getParam(
                        isOtherApplicantSwitch: isOtherApplicantSwitch,
                        isSPVSwitch: isSPVSwitch,
                        listOApplicantInputFieldsCtr:
                            listOApplicantInputFieldsCtr,
                        compName: _compName.text.trim(),
                        regAddr: _regAddr,
                        regDate: regDate.trim(),
                        regNo: _regNo.text.trim(),
                        title: title.trim(),
                        note: "",
                      );
                      PostCaseAPIMgr().wsOnPostCase(
                        context: context,
                        param: param,
                        callback: (model) async {
                          if (model != null && mounted) {
                            try {
                              final UserNotesModel caseModel =
                                  model.responseData.task;
                              //  email notification api call
                              await APIViewModel().req<CommonAPIModel>(
                                context: context,
                                url: Server.CASE_EMAI_NOTI_GET_URL.replaceAll(
                                    "#caseId#",
                                    model.responseData.task.id.toString()),
                                isLoading: true,
                                reqType: ReqType.Get,
                              );

                              Get.to(
                                () => WebScreen(
                                  caseID: caseModel.id.toString(),
                                  title: title,
                                  url: CaseDetailsWebHelper().getLink(
                                    title: title,
                                    taskId: caseModel.id,
                                  ),
                                ),
                              ).then((value) {
                                try {
                                  StateProvider().notify(ObserverState
                                      .STATE_CHANGED_usercase_reload_case_api);
                                  StateProvider().notify(ObserverState
                                      .STATE_CHANGED_mycases_reload_case_api);
                                  Get.back();
                                  /*
                                  Get.off(
                                    () => MyCaseTab(),
                                  ).then((value) {
                                    //callback(route);
                                  });*/
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              });
                            } catch (e) {
                              myLog(e.toString());
                            }
                          }
                        },
                      );
                    }
                  },
                ),
              ),
              SizedBox(height: getHP(context, 10)),
            ],
          ),
        ),
      ),
    );
  }

  drawCaseType() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Type",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            TxtBox(txt: title, height: 5),
          ],
        ),
      ),
    );
  }
}
