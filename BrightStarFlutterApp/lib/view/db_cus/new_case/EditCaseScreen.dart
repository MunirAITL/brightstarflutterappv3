import 'dart:convert';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/new_case/EditCaseAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/EditCaseHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfoEntityModelListModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/my_cases/MyCaseTab.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/OtherApplicantSwitchView.dart';
import 'package:aitl/view/widgets/views/SPVSwitchView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownPicker.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class EditCaseScreen extends StatefulWidget {
  final LocationsModel caseModel;
  const EditCaseScreen({
    Key key,
    @required this.caseModel,
  }) : super(key: key);
  @override
  State createState() => _EditCaseScreenState();
}

class _EditCaseScreenState extends State<EditCaseScreen> with Mixin {
  //final TextEditingController _note = TextEditingController();

  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];
  List<FocusNode> listFocusNode = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  final _regNo = TextEditingController();
  final focusCompName = FocusNode();
  final focusRegNo = FocusNode();

  String _regAddr = "";
  String regDate = "";

  var title = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  int otherApplicantRadioIndex = 1;
  int totalOtherApplicantRadio = 0;

  StateProvider _stateProvider;

  Map mapCaseCfg;
  //  dropdown title
  DropListModel caseDD = DropListModel([]);
  OptionItem caseOpt;

  MortgageCaseInfoEntityModelListModel mortgageCaseInfoEntityModelListModel;

  validate() {
    if (isOtherApplicantSwitch) {
      int i = 0;
      while (i < otherApplicantRadioIndex) {
        if (!UserProfileVal().isEmailOK(
            context,
            listOApplicantInputFieldsCtr[i],
            "Invalid " +
                MyDefine.ORDINAL_NOS[i + 1] +
                " applicant email address")) {
          return false;
        }
        i++;
      }
    }
    if (isSPVSwitch) {
      if (UserProfileVal()
          .isEmpty(context, _compName, "Missing company name")) {
        return false;
      } else if (_regAddr == "") {
        showToast(context: context, msg: "Missing company registered address");
        return false;
      } else if (!UserProfileVal()
          .isEmpty(context, _regNo, "Missing company registeration number")) {
        return false;
      } else if (regDate == "") {
        showToast(context: context, msg: "Please select registered date");
        return false;
      }
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    _compName.dispose();
    _regNo.dispose();
    listOApplicantInputFieldsCtr = null;
    mortgageCaseInfoEntityModelListModel = null;
    listFocusNode = null;
    mapCaseCfg = null;
    caseDD = null;
    caseOpt = null;
    title = null;
    _regAddr = null;
    regDate = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    _stateProvider = new StateProvider();
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
    } catch (e) {}

    try {
      title = widget.caseModel.title;
    } catch (e) {}

    try {
      mortgageCaseInfoEntityModelListModel =
          widget.caseModel.mortgageCaseInfoEntityModelListModel[0];
      if (mortgageCaseInfoEntityModelListModel.isAnyOthers == "Yes") {
        isOtherApplicantSwitch = true;
        try {
          listOApplicantInputFieldsCtr[0].text =
              mortgageCaseInfoEntityModelListModel.customerEmail1;
          if (mortgageCaseInfoEntityModelListModel.customerEmail1
                  .toString()
                  .length >
              0) totalOtherApplicantRadio++;
        } catch (e) {}
        try {
          //mortgageCaseInfoEntityModelListModel.customerEmail2 = "ac@aa.com";
          listOApplicantInputFieldsCtr[1].text =
              mortgageCaseInfoEntityModelListModel.customerEmail2;
          if (mortgageCaseInfoEntityModelListModel.customerEmail2
                  .toString()
                  .length >
              0) totalOtherApplicantRadio++;
        } catch (e) {}
        try {
          //mortgageCaseInfoEntityModelListModel.customerEmail3 = "a2222c@aa.com";
          listOApplicantInputFieldsCtr[2].text =
              mortgageCaseInfoEntityModelListModel.customerEmail3;
          if (mortgageCaseInfoEntityModelListModel.customerEmail3
                  .toString()
                  .length >
              0) totalOtherApplicantRadio++;
        } catch (e) {}
      }
      if (mortgageCaseInfoEntityModelListModel
              .areYouBuyingThePropertyInNameOfASPV ==
          "Yes") {
        isSPVSwitch = true;
        try {
          _compName.text = mortgageCaseInfoEntityModelListModel.companyName;
        } catch (e) {}
        try {
          _regAddr = mortgageCaseInfoEntityModelListModel.registeredAddress;
        } catch (e) {}
        try {
          final f1 = new DateFormat('yyyy-MM-dd');
          final f2 = new DateFormat('dd-MMMM-yyyy');
          var inputDate =
              f1.parse(mortgageCaseInfoEntityModelListModel.dateRegistered);
          var outputDate = f2.format(inputDate);
          regDate = outputDate;
        } catch (e) {}
        try {
          _regNo.text =
              mortgageCaseInfoEntityModelListModel.companyRegistrationNumber;
        } catch (e) {}
      }
    } catch (e) {}
    try {
      mapCaseCfg = NewCaseHelper().getCaseByTitle(title);
      if (mapCaseCfg["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (mapCaseCfg["isSPV"]) {
        isSPVSwitchShow = true;
      }
      caseOpt = OptionItem(id: null, title: title);
    } catch (e) {}
    try {
      int i = 0;
      List<OptionItem> list = [];
      for (var map in NewCaseCfg.listCreateNewCase) {
        if (map['title'] != 'Others')
          list.add(OptionItem(id: (i++).toString(), title: map['title']));
      }
      caseDD = DropListModel(list);
    } catch (e) {}
    try {
      _compName.addListener(() {
        myLog(_compName.text);
      });
      _regNo.addListener(() {
        myLog(_regNo.text);
      });
    } catch (e) {}

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: UIHelper().drawAppbarTitle(title: "Edit Case"),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                ),
              ];
            },
            body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (detail) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: drawLayout(),
            ),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //height: getH(context),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
            children: [
              SizedBox(height: getHP(context, 5)),
              drawCaseType(),
              SizedBox(height: 30),
              SizedBox(height: 30),
              (isOtherApplicantSwitchShow)
                  ? OtherApplicantSwitchView(
                      listOApplicantInputFieldsCtr:
                          listOApplicantInputFieldsCtr,
                      listFocusNode: listFocusNode,
                      isSwitch: isOtherApplicantSwitch,
                      totalOtherApplicantRadio: totalOtherApplicantRadio,
                      callback: (value) {
                        otherApplicantRadioIndex = value;
                        myLog(value);
                      },
                      callbackSwitch: (isSwitch_1) {
                        isOtherApplicantSwitch = isSwitch_1;
                      },
                    )
                  : SizedBox(),
              (isSPVSwitchShow)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: SPVSwitchView(
                        focusCompName: focusCompName,
                        focusRegNo: focusRegNo,
                        compName: _compName,
                        regAddr: _regAddr,
                        regNo: _regNo,
                        regDate: regDate,
                        isSwitch: isSPVSwitch,
                        callback: (value) {
                          regDate = value;
                        },
                        callbackSwitch: (isSwitch_2) {
                          isSPVSwitch = isSwitch_2;
                        },
                        callbackAddress: (address) {
                          _regAddr = address;
                          setState(() {});
                        },
                      ),
                    )
                  : SizedBox(),
              Padding(
                padding: const EdgeInsets.only(
                    top: 40, left: 20, right: 20, bottom: 20),
                child: BSBtn(
                  txt: "Update Case",
                  height: getHP(context, 6),
                  //width: getW(context),
                  callback: () {
                    if (validate()) {
                      final param = EditCaseHelper().getParam(
                        caseModel: widget.caseModel,
                        isOtherApplicantSwitch: isOtherApplicantSwitch,
                        isSPVSwitch: isSPVSwitch,
                        listOApplicantInputFieldsCtr:
                            listOApplicantInputFieldsCtr,
                        compName: _compName.text.trim(),
                        regAddr: _regAddr,
                        regDate: regDate.trim(),
                        regNo: _regNo.text.trim(),
                        title: title.trim(),
                      );
                      myLog(json.encode(param));
                      EditCaseAPIMgr().wsOnPutCase(
                        context: context,
                        param: param,
                        callback: (model) {
                          if (model != null && mounted) {
                            try {
                              final LocationsModel caseModel =
                                  model.responseData.task;

                              Get.to(
                                () => WebScreen(
                                  title: title,
                                  caseID: caseModel.id.toString(),
                                  url: CaseDetailsWebHelper().getLink(
                                    title: title,
                                    taskId: caseModel.id,
                                  ),
                                ),
                              ).then((value) {
                                try {
                                  _stateProvider.notify(ObserverState
                                      .STATE_CHANGED_usercase_reload_case_api);
                                  _stateProvider.notify(ObserverState
                                      .STATE_CHANGED_mycases_reload_case_api);

                                  Get.off(
                                    () => MyCaseTab(),
                                  ).then((value) {
                                    //callback(route);
                                  });
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              });
                            } catch (e) {
                              myLog(e.toString());
                            }
                          }
                        },
                      );
                    }
                  },
                ),
              ),
              SizedBox(height: getHP(context, 10)),
            ],
          ),
        ),
      ),
    );
  }

  drawCaseType() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Type",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            //TxtBox(txt: title, height: 5),
            (caseOpt != null)
                ? DropDownPicker(
                    cap: null,
                    bgColor: Colors.white,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    ddTitleSize: 0,
                    ddRadius: 5,
                    itemSelected: caseOpt,
                    dropListModel: caseDD,
                    onOptionSelected: (optionItem) {
                      caseOpt = optionItem;
                      setState(() {});
                    },
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
