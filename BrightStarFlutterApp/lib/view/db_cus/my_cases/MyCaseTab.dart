import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/my_cases/MyCasesAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/new_case/NewCase2Screen.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';

class MyCaseTab extends StatefulWidget {
  MyCaseTab({
    Key key,
  }) : super(key: key);

  @override
  State createState() => MyCaseTabState();
}

class MyCaseTabState extends State<MyCaseTab>
    with Mixin, SingleTickerProviderStateMixin, StateListener {
  TabController _tabController;
  List<LocationsModel> listTaskInfoSearchModel = [];

  StateProvider _stateProvider;

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  //  tab stuff start here
  int caseStatus = NewCaseCfg.ALL;
  int totalTabs = 5;
  int tabIndex = 0;

  int totalMsg = 4;
  int totalNoti = 2;

  @override
  onStateChanged(ObserverState state) async {
    isLoading = false;
    if (state == ObserverState.STATE_CHANGED_dashboard_reload) {
      Get.back();
    } else if (state == ObserverState.STATE_CHANGED_mycases_reload_case_api) {
      _getRefreshData();
    }
  }

  onPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });
      MyCasesAPIMgr().wsOnPageLoad(
          context: context,
          pageStart: pageStart,
          pageCount: pageCount,
          caseStatus: caseStatus,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    final List<dynamic> locations =
                        model.responseData.locations;
                    if (locations != null && mounted) {
                      //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                      if (locations.length != pageCount) {
                        isPageDone = true;
                      }
                      try {
                        for (LocationsModel location in locations) {
                          listTaskInfoSearchModel.add(location);
                        }
                      } catch (e) {
                        myLog(e.toString());
                      }
                      myLog(listTaskInfoSearchModel.toString());
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    } else {
                      if (mounted) {
                        if (mounted) {
                          setState(() {
                            isLoading = false;
                          });
                        }
                      }
                    }
                  } catch (e) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                    myLog(e.toString());
                  }
                } else {
                  try {
                    //final err = model.errorMessages.login[0].toString();
                    if (mounted) {
                      isLoading = false;
                      showToast(context: context, msg: "Cases not found");
                    }
                  } catch (e) {
                    myLog(e.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                myLog(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            } else {
              myLog("not in");
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          });
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTaskInfoSearchModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _tabController.dispose();
    _tabController = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    listTaskInfoSearchModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    isLoading = false;
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _tabController = new TabController(vsync: this, length: totalTabs);
      _tabController.addListener(() {
        if (!isLoading) {
          controlTabbarIndex(_tabController.index);
          myLog('my tab index is: ' + _tabController.index.toString());
        }
      });
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  controlTabbarIndex(int index) {
    try {
      switch (index) {
        case 0:
          caseStatus = NewCaseCfg.ALL;
          break;
        case 1:
          caseStatus = NewCaseCfg.IN_PROGRESS;
          break;
        case 2:
          caseStatus = NewCaseCfg.SUBMITTED;
          break;
        case 3:
          caseStatus = NewCaseCfg.FMA_SUBMITTED;
          break;
        case 4:
          caseStatus = NewCaseCfg.COMPLETED;
          break;
        default:
      }
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: totalTabs,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: MyTheme.themeData.accentColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    //expandedHeight: 60,
                    elevation: 0,
                    //toolbarHeight: getHP(context, 10),
                    backgroundColor: MyTheme.appbarColor,
                    iconTheme: IconThemeData(
                        color: MyTheme.brandColor //change your color here
                        ),
                    pinned: true,
                    floating: false,
                    snap: false,
                    forceElevated: false,
                    centerTitle: false,
                    leading: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                        ),
                        onPressed: () {
                          Get.back();
                        }),
                    title: UIHelper().drawAppbarTitle(title: "My Cases"),
                    /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                    bottom: PreferredSize(
                      preferredSize: Size.fromHeight(getHP(context, 10)),
                      child: Column(
                        children: [
                          Container(
                            width: getW(context),
                            height: getHP(context, 4),
                            color: Colors.grey,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: Center(
                                child: Txt(
                                    txt: "View the ongoing cases",
                                    txtColor: Colors.white,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.center,
                                    isBold: false),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: TabBar(
                              onTap: (index2) {
                                /*if (isLoading) {
                                  setState(() {
                                    _tabController.index =
                                        _tabController.previousIndex;
                                  });
                                }*/
                              },
                              controller: _tabController,
                              isScrollable: true,
                              indicatorColor: MyTheme.brandColor,
                              indicatorWeight: 1,
                              unselectedLabelColor: Colors.grey.shade500,
                              labelColor: MyTheme.brandColor,
                              /*indicator: UnderlineTabIndicator(
                                borderSide:
                                    BorderSide(width: 5.0, color: Colors.white),
                                insets: EdgeInsets.symmetric(horizontal: 16.0)),*/
                              tabs: [
                                Tab(
                                    child: Txt(
                                        txt: "All",
                                        txtColor: null,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.center,
                                        isBold: true)),
                                Tab(
                                    child: Txt(
                                        txt: "In-Progress",
                                        txtColor: null,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.center,
                                        isBold: true)),
                                Tab(
                                    child: Txt(
                                        txt: "Submitted",
                                        txtColor: null,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.center,
                                        isBold: true)),
                                Tab(
                                    child: Txt(
                                        txt: "FMA Submitted",
                                        txtColor: null,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.center,
                                        isBold: true)),
                                Tab(
                                    child: Txt(
                                        txt: "Completed",
                                        txtColor: null,
                                        txtSize: MyTheme.txtSize - 0.4,
                                        txtAlign: TextAlign.center,
                                        isBold: true)),
                              ],
                            ),
                          ),
                          (isLoading)
                              ? AppbarBotProgBar(
                                  backgroundColor: MyTheme.appbarProgColor,
                                )
                              : Container()
                        ],
                      ),
                    ),
                  ),
                ];
              },
              body: TabBarView(
                physics: (isLoading)
                    ? NeverScrollableScrollPhysics()
                    : AlwaysScrollableScrollPhysics(),
                controller: _tabController,
                children: <Widget>[
                  drawRecentCases(),
                  drawRecentCases(),
                  drawRecentCases(),
                  drawRecentCases(),
                  drawRecentCases(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  drawRecentCases() {
    try {
      return Container(
        child: (listTaskInfoSearchModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      onPageLoad();
                    }
                  }
                  return true;
                },
                child: RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.brandColor,
                  onRefresh: _getRefreshData,
                  child: Container(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20),
                          child: Container(
                            color: MyTheme.brandColor,
                            height: getHP(context, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20),
                                    child: Txt(
                                        txt: "TYPE",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - 1,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 40),
                                  child: Flexible(
                                    child: Txt(
                                        txt: "CASE No",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - 1,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                  ),
                                ),
                                Flexible(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 20),
                                    child: Txt(
                                        txt: "APPLICATION STARTED",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - 1,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Expanded(
                          flex: 5,
                          child: Container(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Container(
                                color: Colors.white,
                                child: ListView.builder(
                                  addAutomaticKeepAlives: true,
                                  cacheExtent: AppConfig.page_limit.toDouble(),
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  //primary: false,
                                  itemCount: listTaskInfoSearchModel.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return drawRecentCaseItem(index);
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: getWP(context, 40),
                            height: getHP(context, 20),
                            child: Image.asset(
                              'assets/images/screens/home/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(height: 20),
                          Txt(
                            txt:
                                "Looks like you haven't created any cases yet?",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.center,
                            isBold: false,
                            txtLineSpace: 1.2,
                          ),
                          SizedBox(height: 20),
                          Btn(
                            txt: "Create Now!",
                            bgColor: MyTheme.brandColor,
                            txtColor: Colors.white,
                            width: null,
                            height: getHP(context, 5),
                            callback: () {
                              Get.to(() => NewCase2Screen());
                            },
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawRecentCaseItem(index) {
    try {
      LocationsModel locationsModel = listTaskInfoSearchModel[index];
      if (locationsModel == null) return SizedBox();
      //final icon = NewCaseHelper().getCreateCaseIconByTitle(caseModel.title);
      //if (icon == null) return SizedBox();
      //final topBadgePos = getHP(context, 2);
      //final endBadgePos = getHP(context, .05);

      var now = Jiffy(locationsModel.creationDate).format("dd/MM/yy");

      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
              () => WebScreen(
                title: locationsModel.title,
                caseID: locationsModel.id.toString(),
                url: CaseDetailsWebHelper().getLink(
                    title: locationsModel.title, taskId: locationsModel.id),
              ),
              fullscreenDialog: true,
            ).then((value) {
              //callback(route);
            });
          } catch (e) {}
        },
        child: Container(
          //color: Colors.black,
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Container(
                            //width: getWP(context, 25),
                            //color: Colors.yellow,
                            child: Txt(
                          txt: locationsModel.title,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - 0.4,
                          txtAlign: TextAlign.start,
                          isBold: false,
                          isOverflow: true,
                        )),
                      ),
                      Expanded(
                        flex: 3,
                        child: Txt(
                            txt: locationsModel.id.toString(),
                            txtColor: MyTheme.brandColor,
                            txtSize: MyTheme.txtSize - 0.4,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                      //SizedBox(width: 10),
                      Expanded(
                        flex: 3,
                        child: Txt(
                            txt: now,
                            txtColor: MyTheme.brandColor,
                            txtSize: MyTheme.txtSize - 0.4,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                      Flexible(
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.black54,
                        ),
                        /*onPressed: () async {
                            Get.to(
                              () => TaskBiddingScreen(
                                                              title: caseModel.title,
                                                              taskId: caseModel.id),
                              
                            ).then((value) {
                              //callback(route);
                            });                                      
                          },*/
                        //),
                      ),
                      //SizedBox(width: 5),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(bottom: 10, top: 10, right: 10),
                  child: Container(color: Colors.grey, height: .5),
                )
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
  }
}
