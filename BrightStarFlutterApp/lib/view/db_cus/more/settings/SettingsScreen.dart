import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/MoreHelper.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/more/settings/TestNotiScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../profile/EditProfileScreen.dart';
import 'NotiSettingsScreen.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  List<String> listItems = [
    "Edit account",
    "Notification settings",
    "Test Notification"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          elevation: 0,
          backgroundColor: MyTheme.appbarColor,
          title: UIHelper().drawAppbarTitle(title: "Settings"),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: ListView.builder(
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                return GestureDetector(
                  onTap: () async {
                    switch (index) {
                      case 0:
                        Get.to(
                          () => EditProfileScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 1:
                        Get.to(
                          () => NotiSettingsScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 2:
                        Get.to(
                          () => TestNotiScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      default:
                    }
                  },
                  child: Container(
                    color: Colors.transparent,
                    child: ListTile(
                      title: Txt(
                          txt: item,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      trailing: (index != MoreHelper.listMore.length - 1)
                          ? Icon(
                              Icons.arrow_right,
                              color: Colors.black,
                              size: 30,
                            )
                          : SizedBox(),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
