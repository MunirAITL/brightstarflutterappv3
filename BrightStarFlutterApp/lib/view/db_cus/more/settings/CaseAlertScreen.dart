import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';

class CaseAlertScreen extends StatelessWidget with Mixin {
  final Map<String, dynamic> message;

  const CaseAlertScreen({Key key, @required this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          backgroundColor: MyTheme.appbarColor,
          title: UIHelper().drawAppbarTitle(title: "Case alerts"),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: drawCaseUI(context),
      ),
    );
  }

  drawCaseUI(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
              child: Txt(
                  txt: message['data']['Message'].toString() ?? '',
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
              child: Txt(
                  txt: message['data']['Description'].toString() ?? '',
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
              child: BSBtn(
                  txt: "Close",
                  height: getHP(context, 6),
                  callback: () {
                    Get.back();
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
