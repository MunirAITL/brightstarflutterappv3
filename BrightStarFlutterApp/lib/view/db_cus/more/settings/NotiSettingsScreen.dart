import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/more/settings/NotiSettingsCfg.dart';
import 'package:aitl/controller/api/db_cus/tab_more/settings/NotiSettingsAPIMgr.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';

class NotiSettingsScreen extends StatefulWidget {
  @override
  State createState() => _NotiSettingsScreenState();
}

class _NotiSettingsScreenState extends State<NotiSettingsScreen> with Mixin {
  NotiSettingsCfg notiSettingsCfg;

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    notiSettingsCfg = null;
    super.dispose();
  }

  appInit() async {
    try {
      notiSettingsCfg = NotiSettingsCfg();
      NotiSettingsAPIMgr().wsGetNotificationSettingsAPI(
          context: context,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    final caseUpdateMap = notiSettingsCfg.listOpt[0];
                    final caseRecomendationMap = notiSettingsCfg.listOpt[1];
                    final caseCompletedMap = notiSettingsCfg.listOpt[2];
                    final caseReminderMap = notiSettingsCfg.listOpt[3];
                    final userUpdateMap = notiSettingsCfg.listOpt[4];
                    final helpfulInfoMap = notiSettingsCfg.listOpt[5];
                    final updateNewsLetterMap = notiSettingsCfg.listOpt[6];

                    //  caseUpdate
                    caseUpdateMap['isEmail'] = model
                        .responseData.userNotificationSetting.isCaseUpdateEmail;
                    caseUpdateMap['isSms'] = model
                        .responseData.userNotificationSetting.isCaseUpdateSMS;
                    caseUpdateMap['isPush'] = model.responseData
                        .userNotificationSetting.isCaseUpdateNotification;
                    //  caseRecomendation
                    caseRecomendationMap['isEmail'] = model.responseData
                        .userNotificationSetting.isCaseRecomendationEmail;
                    caseRecomendationMap['isPush'] = model
                        .responseData
                        .userNotificationSetting
                        .isCaseRecomendationNotification;
                    //  caseCompleted
                    caseCompletedMap['isEmail'] = model.responseData
                        .userNotificationSetting.isCaseCompletedEmail;
                    caseCompletedMap['isSms'] = model.responseData
                        .userNotificationSetting.isCaseCompletedSMS;
                    caseCompletedMap['isPush'] = model.responseData
                        .userNotificationSetting.isCaseCompletedNotification;
                    //  caseReminder
                    caseReminderMap['isEmail'] = model.responseData
                        .userNotificationSetting.isCaseReminderEmail;
                    caseReminderMap['isSms'] = model
                        .responseData.userNotificationSetting.isCaseReminderSMS;
                    caseReminderMap['isPush'] = model.responseData
                        .userNotificationSetting.isCaseReminderNotification;
                    //  userUpdateMap
                    userUpdateMap['isEmail'] = model
                        .responseData.userNotificationSetting.isUserUpdateEmail;
                    userUpdateMap['isPush'] = model.responseData
                        .userNotificationSetting.isUserUpdateNotification;
                    //  helpfulInfoMap
                    helpfulInfoMap['isEmail'] = model.responseData
                        .userNotificationSetting.isHelpfulInformationEmail;
                    helpfulInfoMap['isPush'] = model
                        .responseData
                        .userNotificationSetting
                        .isHelpfulInformationNotification;
                    //  updateNewsLetterMap
                    updateNewsLetterMap['isEmail'] = model.responseData
                        .userNotificationSetting.isUpdateAndNewsLetterEmail;
                    updateNewsLetterMap['isPush'] = model
                        .responseData
                        .userNotificationSetting
                        .isUpdateAndNewsLetterNotification;

                    //notiSettingsCfg.listOpt[6] = updateNewsLetterMap;
                    //log(notiSettingsCfg.listOpt[6].toString());

                    setState(() {});
                  } catch (e) {
                    myLog(e.toString());
                  }
                } else {
                  try {
                    //final err = model.errorMessages.login[0].toString();
                    if (mounted) {
                      showToast(
                          context: context,
                          msg: "Notifications Settings not found");
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          });
    } catch (e) {}
    try {
      /*await NotiSettingsHelper().getSettings().then((listOpt) {
        if (listOpt != null) {
          notiSettingsCfg.listOpt = listOpt;
          setState(() {});
        } else {}
      });*/
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: UIHelper()
                      .drawAppbarTitle(title: "Notification settings"),
                  bottom: PreferredSize(
                      preferredSize: new Size(
                          getW(context), getHP(context, (isLoading) ? .5 : 0)),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Container(
        child: (notiSettingsCfg == null)
            ? SizedBox()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Txt(
                          txt:
                              "Your notifications can be updated at any time via the options below or the " +
                                  AppDefine.APP_NAME +
                                  " App.",
                          txtColor: MyTheme.timelinePostCallerNameColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          txtLineSpace: 1.2,
                          isBold: false),
                    ),
                    for (var listOpt in notiSettingsCfg.listOpt)
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Container(
                          width: double.infinity,
                          //color: Colors.black,
                          child: Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 10, left: 10),
                                  child: Txt(
                                      txt: listOpt['title'],
                                      txtColor: Colors.black,
                                      txtSize: 2,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                ),
                                SizedBox(height: 10),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 10, left: 10),
                                  child: Txt(
                                      txt: listOpt['subTitle'],
                                      txtColor: Colors.black,
                                      txtSize: 2,
                                      txtLineSpace: 1.2,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                                (listOpt['email'] as bool)
                                    ? Theme(
                                        data: MyTheme.radioThemeData,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isEmail'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isEmail'] = newValue;
                                                });
                                              },
                                            ),
                                            Txt(
                                                txt: 'Email',
                                                txtColor: Colors.black,
                                                txtSize: 2,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                (listOpt['sms'] as bool)
                                    ? Theme(
                                        data: MyTheme.radioThemeData,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isSms'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isSms'] = newValue;
                                                });
                                              },
                                            ),
                                            Txt(
                                                txt: 'Sms',
                                                txtColor: Colors.black,
                                                txtSize: 2,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                (listOpt['push'] as bool)
                                    ? Theme(
                                        data: MyTheme.radioThemeData,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Checkbox(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              value: listOpt['isPush'],
                                              onChanged: (newValue) {
                                                setState(() {
                                                  listOpt['isPush'] = newValue;
                                                });
                                              },
                                            ),
                                            Txt(
                                                txt: 'Push',
                                                txtColor: Colors.black,
                                                txtSize: 2,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15, right: 15, top: 10, bottom: 20),
                      child: BSBtn(
                          txt: "Save",
                          height: getHP(context, 6),
                          callback: () {
                            NotiSettingsAPIMgr().wsPostNotificationSettingsAPI(
                              context: context,
                              notiSettingsCfg: notiSettingsCfg,
                              callback: (model) {
                                if (model != null && mounted) {
                                  try {
                                    if (model.success) {
                                      try {
                                        // final msg = model
                                        // .messages.postNotificationSetting[0]
                                        //.toString();
                                        showToast(
                                            context: context,
                                            msg:
                                                "Notification setting saved successfully.", //msg,
                                            which: 1);
                                        //await NotiSettingsHelper().setSettings(notiSettingsCfg.listOpt);
                                      } catch (e) {
                                        myLog(e.toString());
                                      }
                                    } else {
                                      try {
                                        if (mounted) {
                                          final err = model.messages
                                              .postNotificationSetting[0]
                                              .toString();
                                          showToast(
                                            context: context,
                                            msg: err,
                                          );
                                        }
                                      } catch (e) {
                                        myLog(e.toString());
                                      }
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              },
                            );
                          }),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
