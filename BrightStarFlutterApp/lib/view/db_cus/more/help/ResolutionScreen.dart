import 'dart:developer';
import 'dart:io';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/tab_more/help/ResolutionAPIMgr.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/ResolutionHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class ResolutionScreen extends StatefulWidget {
  @override
  State createState() => _ResolutionScreenState();
}

class _ResolutionScreenState extends State<ResolutionScreen> with Mixin {
  final TextEditingController _desc = TextEditingController();

  static const int MAX_FILE_UPLOAD = 5;

  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];

  ResolutionHelper resolutionHelper;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _desc.dispose();
    resolutionHelper = null;
    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  browseFiles() async {
    try {
      try {
        FilePickerResult result = await FilePicker.platform.pickFiles(
          allowMultiple: false,
          type: FileType.any,
          //type: FileType.custom,
          /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
        );

        if (result != null) {
          if (listMediaUploadFilesModel.length >= MAX_FILE_UPLOAD) {
            showToast(
                context: context,
                msg: "Maximum file upload limit is " +
                    MAX_FILE_UPLOAD.toString() +
                    " files");
            return;
          }
          MediaUploadAPIMgr().wsMediaUploadFileAPI(
              context: context,
              file: File(result.files.single.path),
              callback: (model) {
                if (model != null && mounted) {
                  try {
                    if (model.success) {
                      listMediaUploadFilesModel
                          .add(model.responseData.images[0]);
                      setState(() {});
                    } else {
                      final err =
                          model.errorMessages.upload_pictures[0].toString();
                      showToast(context: context, msg: err);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              });
        }
      } catch (e) {
        myLog(e.toString());
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  appInit() async {
    try {
      resolutionHelper = ResolutionHelper();
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 160,
                  elevation: 0,
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  title: UIHelper().drawAppbarTitle(title: "Contact us"),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return (resolutionHelper == null)
        ? SizedBox()
        : Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 30),
            child: Container(
              //color: Colors.white,
              child: ListView(
                shrinkWrap: true,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: DropDownListDialog(
                      context: context,
                      title: resolutionHelper.opt.title,
                      ddTitleList: resolutionHelper.dd,
                      callback: (optionItem) {
                        resolutionHelper.opt = optionItem;
                        setState(() {});
                      },
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Txt(
                        txt: "How can we help you?",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.only(left: 20.0, right: 20, top: 10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextField(
                      controller: _desc,
                      minLines: 5,
                      maxLines: 10,
                      //expands: true,
                      autocorrect: false,
                      maxLength: 500,
                      keyboardType: TextInputType.multiline,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                      ),
                      decoration: InputDecoration(
                        hintText: 'Description',
                        hintStyle: TextStyle(color: Colors.grey),
                        //labelText: 'Your message',
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 15, top: 15, right: 15),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Txt(
                        txt: "Attachments - " +
                            listMediaUploadFilesModel.length.toString() +
                            ' files added',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  for (MediaUploadFilesModel model in listMediaUploadFilesModel)
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Card(
                        child: ListTile(
                          leading: IconButton(
                              icon: Icon(
                                Icons.remove_circle,
                                color: Colors.red,
                              ),
                              onPressed: () {
                                listMediaUploadFilesModel.remove(model);
                                setState(() {});
                              }),
                          title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Txt(
                                txt: model.name.split('/').last ?? '',
                                txtColor: Colors.black87,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ),
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: DottedBorder(
                      borderType: BorderType.RRect,
                      radius: Radius.circular(10),
                      padding: EdgeInsets.all(6),
                      color: Colors.grey,
                      strokeWidth: 1.5,
                      child: GestureDetector(
                        onTap: () async {
                          await browseFiles();
                        },
                        child: Container(
                          height: getHP(context, 5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.attach_file,
                                color: Colors.black54,
                                size: 30,
                              ),
                              Txt(
                                  txt: "Add up to 5 files",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                    child: BSBtn(
                      txt: "Submit",
                      height: getHP(context, 6),
                      callback: () {
                        if (resolutionHelper.opt.id == null) {
                          showToast(
                              context: context,
                              msg: "Please choose ticket type from the list");
                          return;
                        } else if (_desc.text.trim().length == 0) {
                          showToast(
                              context: context,
                              msg: "Please enter description");
                          return;
                        }
                        List<String> listFileUrl = [];
                        for (MediaUploadFilesModel model
                            in listMediaUploadFilesModel) {
                          listFileUrl.add(model.url);
                        }
                        final desc = _desc.text.trim() +
                            "\n\n" +
                            "From " +
                            ((Platform.isAndroid) ? 'Android' : 'iPhone');
                        ResolutionAPIMgr().wsResolutionAPI(
                          context: context,
                          resolutionHelper: resolutionHelper,
                          desc: desc,
                          listFileUrl: listFileUrl,
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  _desc.text = "";
                                  listMediaUploadFilesModel.clear();
                                  //final msg = model.messages.resolution_post[0]
                                  // .toString();
                                  showToast(
                                      context: context,
                                      msg:
                                          "Thank you for submitting the support ticket.\n\nOur support member will contact you as soon as possible.",
                                      which: 1);
                                  setState(() {});
                                } else {}
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
