import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/more/help/SupportCentreScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';

class HelpScreen extends StatefulWidget {
  @override
  State createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> with Mixin {
  List<String> listItems = [
    "Support centre",
    "FAQ",
    //"Terms and conditions",
    //"Privacy",
    //"Application tutorial"
  ];

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.appbarColor,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          title: UIHelper().drawAppbarTitle(title: "Help"),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: ListView.builder(
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                return GestureDetector(
                  onTap: () async {
                    switch (index) {
                      case 0: //  Support Center
                        Get.to(
                          () => SupportCentreScreen(),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 1: //  FAQ
                        Get.to(
                          () => WebScreen(
                            title: "FAQ",
                            url: Server.FAQ_URL,
                          ),
                        ).then((value) {
                          //callback(route);
                        });
                        break;
                      case 3: //  Application Tuturial
                        Get.back(result: true);
                        break;
                      default:
                    }
                  },
                  child: Container(
                    child: ListTile(
                      title: Txt(
                          txt: item,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      trailing: Icon(
                        Icons.arrow_right,
                        color: Colors.black,
                        size: 30,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
