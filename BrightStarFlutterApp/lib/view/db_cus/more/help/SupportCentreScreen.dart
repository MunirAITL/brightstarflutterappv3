import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/more/help/ResolutionScreen.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportCentreScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.appbarColor,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          title: UIHelper().drawAppbarTitle(title: "Support"),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: MyTheme.brandColor,
          child: Icon(
            Icons.add,
            color: Colors.white, //The color which you want set.
          ),
          onPressed: () => {
            Get.to(
              () => ResolutionScreen(),
            ).then((value) {
              //callback(route);
            })
          },
        ),
        body: drawSupportButtons(context),
      ),
    );
  }

  drawSupportButtons(context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Email: " + AppDefine.SUPPORT_EMAIL,
              txtColor: Colors.white,
              bgColor: MyTheme.brandColor,
              width: getW(context),
              height: getHP(context, 6),
              radius: 10,
              callback: () {
                launch("mailto:" + AppDefine.SUPPORT_EMAIL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Call: " + AppDefine.SUPPORT_CALL,
              txtColor: Colors.white,
              bgColor: MyTheme.brandColor,
              width: getW(context),
              height: getHP(context, 6),
              radius: 10,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Send Message",
              txtColor: Colors.white,
              bgColor: MyTheme.brandColor,
              width: getW(context),
              height: getHP(context, 6),
              radius: 10,
              callback: () async {
                Get.to(
                  () => ResolutionScreen(),
                ).then((value) {
                  //callback(route);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
