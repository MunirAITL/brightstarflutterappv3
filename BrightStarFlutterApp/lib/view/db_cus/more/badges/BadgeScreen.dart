import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/api/db_cus/tab_more/badge/BadgeAPIMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
/*

class BadgeScreen extends StatefulWidget {
  @override
  State createState() => _BadgeScreenState();
}

class _BadgeScreenState extends State<BadgeScreen> with Mixin, BadgeMixin {
  List<dynamic> listItems = [];
  List<UserBadgeModel> listUserBadgeModel = [];
  UserBadgeModel userBadgeModel;

  updateUserBadges() async {
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) async {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;
                await getBadgeList(context, listUserBadgeModel, (e, val) {
                  if (e == badgeEnum.BadgeEmailAPIModel) {
                    showAlert(msg: val.toString());
                  } else if (e == badgeEnum.BadgePhotoIDScreen) {
                    updateUserBadges();
                  }
                }, (list) {
                  listItems = list;
                  setState(() {});
                });
              } else {
                //showToast(txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                showToast(
                    txtColor: Colors.white,
                    bgColor: MyTheme.brandColor,
                    msg: "Sorry, something went wrong");
              }
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog("not in");
          }
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listItems = null;
    listUserBadgeModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      updateUserBadges();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                    //expandedHeight: 160,
                    elevation: 0,
                    //toolbarHeight: getHP(context, 10),
                    backgroundColor: MyTheme.appbarColor,
                    pinned: true,
                    floating: false,
                    snap: false,
                    forceElevated: false,
                    centerTitle: false,
                    title: Txt(
                        txt: "Badges",
                        txtColor: Colors.black,
                        txtSize: MyTheme.appbarTitleFontSize,
                        txtAlign: TextAlign.start,
                        isBold: false)),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          primary: true,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Txt(
                  txt:
                      "Badges are issued when specific requirements are met. A green tick shows that the verification is currently active.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  //txtLineSpace: 1.2,
                  isBold: false),
            ),
            SizedBox(height: 20),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                BtnOutline btn = item['btn'] as BtnOutline;

                return Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                            //color: Colors.black,
                            width: getWP(context, 11),
                            height: getWP(context, 11),
                            child: Image.asset((item['isVerified'])
                                ? item['icon']
                                    .toString()
                                    .replaceAll(".png", "_ok.png")
                                : item['icon'])),
                        SizedBox(width: 20),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: item["title"],
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isOverflow: true,
                                  isBold: false),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 5, bottom: 5),
                                child: Txt(
                                    txt: item["desc"],
                                    txtColor: Colors.black54,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.start,
                                    //txtLineSpace: 1.4,
                                    isBold: false),
                              ),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: btn ?? SizedBox()),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
*/

class BadgeScreen extends StatefulWidget {
  @override
  State createState() => _BadgeScreenState();
}

class _BadgeScreenState extends State<BadgeScreen> with Mixin {
  List<dynamic> listItems = [];
  List<UserBadgeModel> listUserBadgeModel = [];
  UserBadgeModel userBadgeModel;
  bool isMobileVerified = false;
  bool isEmailVerified = false;
  bool isPrivacyAccepted = false;
  String privacyAcceptedDate = DateTime.now().toString();
  bool isEIDVerified = false;

  updateUserBadges() async {
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;

                for (int i = 0; i < listUserBadgeModel.length; i++) {
                  debugPrint(
                      "position $i type = " + listUserBadgeModel[i].type);
                  debugPrint("position $i status = " +
                      listUserBadgeModel[i].isVerified.toString());

                  if (listUserBadgeModel[i].type == "Mobile" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isMobileVerified = true;
                      debugPrint("position $i isMobileVerified = " +
                          isMobileVerified.toString());
                    });
                  }

                  if (listUserBadgeModel[i].type == "Email" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isEmailVerified = true;
                    });
                  }
                  if (listUserBadgeModel[i].type == "CustomerPrivacy" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isPrivacyAccepted = true;
                    });
                    setState(() {
                      privacyAcceptedDate =
                          listUserBadgeModel[i].creationDate.toString();
                    });
                  }

                  if (listUserBadgeModel[i].type == "ElectricId" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isEIDVerified = true;
                    });
                  }
                }

                try {
                  setState(() {
                    listItems = [
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_phone_icon.png",
                        "title": "Mobile",
                        "desc":
                            "You will receive this badge when you mobile number is verified. This will also allow you to receive Case related notifications and make calls* from the portal.",
                        "btn": isMobileVerified
                            ? BtnOutline(
                                txt: "✓ Verified",
                                txtColor: Colors.green,
                                borderColor: Colors.black87,
                                callback: () {})
                            : BtnOutline(
                                txt: "Verify",
                                txtColor: Colors.black87,
                                borderColor: Colors.black87,
                                callback: () async {
                                  String countryCode = "+44";
                                  String countryName = "GB";
                                  var cn = await PrefMgr.shared
                                      .getPrefStr("countryName");
                                  var cd = await PrefMgr.shared
                                      .getPrefStr("countryCode");
                                  if (cn != null &&
                                      cd != null &&
                                      cn.toString().isNotEmpty &&
                                      cd.toString().isNotEmpty) {
                                    setState(() {
                                      countryName = cn;
                                      countryCode = cd;
                                    });
                                  }

                                  Sms2APIMgr().wsLoginMobileOtpPostAPI(
                                    context: context,
                                    countryCode: countryCode,
                                    mobile: userData.userModel.mobileNumber,
                                    // mobile: getFullPhoneNumber(countryCodeTxt: countryCode,number: _phoneController.text.trim()),
                                    callback: (model) {
                                      if (model != null && mounted) {
                                        try {
                                          if (model.success) {
                                            try {
                                              //final msg = model.messages.postUserotp[0].toString();
                                              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.redColor,msg: msg, which: 1);
                                              if (mounted) {
                                                Sms2APIMgr().wsSendOtpNotiAPI(
                                                    context: context,
                                                    otpId: model.responseData
                                                        .userOTP.id,
                                                    callback: (model) {
                                                      if (model != null &&
                                                          mounted) {
                                                        try {
                                                          if (model.success) {
                                                            Get.to(
                                                              () => Sms3Screen(
                                                                isBack: true,
                                                                mobileUserOTPModel:
                                                                    model
                                                                        .responseData
                                                                        .userOTP,
                                                              ),
                                                            );
                                                          } else {
                                                            try {
                                                              if (mounted) {
                                                                final err = model
                                                                    .messages
                                                                    .postUserotp[
                                                                        0]
                                                                    .toString();
                                                                showToast(
                                                                    context:
                                                                        context,
                                                                    msg: err);
                                                              }
                                                            } catch (e) {}
                                                          }
                                                        } catch (e) {}
                                                      }
                                                    });
                                              }
                                            } catch (e) {}
                                          } else {
                                            try {
                                              if (mounted) {
                                                final err = model
                                                    .messages.postUserotp[0]
                                                    .toString();
                                                showToast(
                                                    context: context, msg: err);
                                              }
                                            } catch (e) {}
                                          }
                                        } catch (e) {}
                                      }
                                    },
                                  );
                                }),
                      },
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_email_icon.png",
                        "title": "Email",
                        "desc":
                            "You receive this badge upon verification of your email address. This will also allow you to receive case related notifications and send or receive emails from the portal.",
                        "btn": isEmailVerified
                            ? BtnOutline(
                                txt: "✓ Verified",
                                txtColor: Colors.green,
                                borderColor: Colors.black87,
                                callback: () {})
                            : BtnOutline(
                                txt: "Send Email",
                                txtColor: Colors.black87,
                                borderColor: Colors.black87,
                                callback: () {
                                  //
                                  BadgeAPIMgr().wsPostEmailBadge(
                                      context: context,
                                      callback: (BadgeEmailAPIModel model) {
                                        if (mounted) {
                                          if (model.success) {
                                            showToast(
                                                context: context,
                                                msg:
                                                    "We have sent an email for verification, please check your email",
                                                which: 1);
                                          } else {
                                            showSnake("Alert!",
                                                "Sorry, something went wrong");
                                          }
                                        }
                                      });
                                }),
                      },
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_prv_icon.png",
                        "title": "Customer Privacy",
                        "desc":
                            "You receive this badge when you agree to the Customer Privacy Statement. Customer Privacy accepted at ${DateFormat.yMMMMd().format(DateTime.parse(privacyAcceptedDate))}.",
                        "btn": isPrivacyAccepted
                            ? BtnOutline(
                                txt: "✓ Accepted",
                                txtColor: Colors.green,
                                borderColor: Colors.black87,
                                callback: () {
                                  if (mounted) {
                                    PrivacyPolicyAPIMgr().wsOnLoad(
                                      context: context,
                                      callback: (model) {
                                        if (model != null) {
                                          try {
                                            if (model.success) {
                                              debugPrint(
                                                  "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                                              for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                                  in model.responseData
                                                      .termsPrivacyNoticeSetupsList) {
                                                if (termsPrivacyPoliceSetups
                                                        .type
                                                        .toString() ==
                                                    "Customer Privacy Notice") {
                                                  Get.to(
                                                    () => PDFDocumentPage(
                                                      title: "Privacy",
                                                      url:
                                                          termsPrivacyPoliceSetups
                                                              .webUrl,
                                                    ),
                                                  ).then((value) {
                                                    //callback(route);
                                                  });
                                                  break;
                                                }
                                              }
                                            } else {
                                              showToast(
                                                  context: context,
                                                  msg:
                                                      "Sorry, something went wrong");
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        } else {
                                          myLog("dashboard screen not in");
                                        }
                                      },
                                    );
                                  }
                                })
                            : BtnOutline(
                                txt: "View",
                                txtColor: Colors.red,
                                borderColor: Colors.black87,
                                callback: () {
                                  if (mounted) {
                                    PrivacyPolicyAPIMgr().wsOnLoad(
                                      context: context,
                                      callback: (model) {
                                        if (model != null) {
                                          try {
                                            if (model.success) {
                                              debugPrint(
                                                  "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                                              for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                                  in model.responseData
                                                      .termsPrivacyNoticeSetupsList) {
                                                if (termsPrivacyPoliceSetups
                                                        .type
                                                        .toString() ==
                                                    "Customer Privacy Notice") {
                                                  Get.to(
                                                    () => PDFDocumentPage(
                                                      title: "Privacy",
                                                      url:
                                                          termsPrivacyPoliceSetups
                                                              .webUrl,
                                                    ),
                                                  ).then((value) {
                                                    //callback(route);
                                                  });
                                                  break;
                                                }
                                              }
                                            } else {
                                              showToast(
                                                  context: context,
                                                  msg:
                                                      "Sorry, something went wrong");
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        } else {
                                          myLog("dashboard screen not in");
                                        }
                                      },
                                    );
                                  }
                                }),
                      },
                      /*{
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_eid_icon.png",
                        "title": "Electronic ID Verification",
                        "desc":
                            "You receive this badge when your ID is verified through an EID system.",
                        "btn": isEIDVerified
                            ? BtnOutline(
                                txt: "✓ Verified",
                                txtColor: Colors.green,
                                borderColor: Colors.black87,
                                callback: () {
                                  if (!Server.isOtp) {
                                    Get.to(() => SelfiePage());
                                  }
                                })
                            : BtnOutline(
                                txt: "Verify",
                                txtColor: Colors.black87,
                                borderColor: Colors.black87,
                                callback: () {
                                  //
                                  Get.to(() => SelfiePage());
                                }),
                      },*/
                      //"Application tutorial"
                    ];
                  });
                } catch (e) {}
              } else {
                showToast(context: context, msg: "Sorry, something went wrong");
              }
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog(" badge screen not in");
          }
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listItems = null;
    listUserBadgeModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      updateUserBadges();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                    elevation: 0,
                    backgroundColor: MyTheme.themeData.accentColor,
                    iconTheme: IconThemeData(
                        color: MyTheme.brandColor //change your color here
                        ),
                    pinned: true,
                    floating: false,
                    snap: false,
                    forceElevated: false,
                    centerTitle: false,
                    title: UIHelper().drawAppbarTitle(title: "Badges")),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          primary: true,
          children: [
            Txt(
                txt:
                    "Badges are issued when specific requirements are met. A green tick shows that the verification is currently active.",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                //txtLineSpace: 1.2,
                isBold: false),
            SizedBox(height: 20),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                final item = listItems[index];
                BtnOutline btn = item['btn'] as BtnOutline;

                return Card(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: ListTile(
                      // leading: Image.asset(item['icon']),
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              width: getWP(context, 10),
                              height: getWP(context, 10),
                              child: Image.asset(item['icon'])),
                          SizedBox(width: 10),
                          Expanded(
                            child: Txt(
                                txt: item["title"],
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isOverflow: true,
                                isBold: true),
                          ),
                        ],
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Txt(
                                txt: item["desc"],
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                //txtLineSpace: 1.4,
                                isBold: false),
                          ),
                          btn ?? SizedBox(),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
