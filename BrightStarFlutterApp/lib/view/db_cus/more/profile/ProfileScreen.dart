import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/db_cus/tab_more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/tab_more/settings/GetProfileAPIMgr.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/db_cus/more/badges/BadgeScreen.dart';
import 'package:aitl/view/db_cus/more/profile/EditProfileScreen.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with Mixin {
  List<dynamic> listItems = [];
  bool isLoading = false;
  bool isLoadingBadge = false;
  UserModel userModel;

  List<UserBadgeModel> listUserBadgeModel = [];
  List<UserBadgeModel> listUserBadgeModelShowList = [];
  UserBadgeModel userBadgeModel;

  getProfile() {
    GetProfileAPIMgr().wsGetProfileAPI(
      context: context,
      userID: userData.userModel.id.toString(),
      callback: (model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                userModel = model.responseData.user;
                myLog("UserInfo data parse = " + model.responseData.toString());
                setState(() async {
                  isLoading = false;
                  await DBMgr.shared
                      .setUserProfile(user: model.responseData.user);
                  await userData.setUserModel();
                });
              } catch (e) {
                myLog("Error UserInfo data parse = " + e.toString());
                setState(() {
                  isLoading = false;
                });
              }
            } else {
              setState(() {
                isLoading = false;
              });

              try {
                if (mounted) {
                  final err = model.errorMessages.post_user[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  updateUserBadges() async {
    isLoadingBadge = true;
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;
                for (UserBadgeModel userBadge in listUserBadgeModel) {
                  if (userBadge.isVerified) {
                    listUserBadgeModelShowList.add(userBadge);
                  }
                }
                setState(() {
                  isLoadingBadge = false;
                });
              } else {
                showToast(context: context, msg: "Sorry, something went wrong");
                setState(() {
                  isLoadingBadge = false;
                });
              }
            } catch (e) {
              setState(() {
                isLoadingBadge = false;
              });
              myLog(e.toString());
            }
          } else {
            setState(() {
              isLoadingBadge = false;
            });
            myLog("profile screen new not in");
          }
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listItems = null;
    super.dispose();
  }

  appInit() async {
    isLoading = true;

    await getProfile();
    await updateUserBadges();

    try {
      listItems = [
        {
          "icon": "assets/images/screens/db_cus/more/badge/phone_verified.png",
          "title": "Mobile",
          "desc":
              "You have received this badge because you're mobile number is verified.",
          // "You will receive this badge when you mobile number is verified. This will also allow you to receive Case related notifications and make calls* from the portal.",
          "btnTitle": null,
        },
        {
          "icon": "assets/images/screens/db_cus/more/badge/gmail_verified.png",
          "title": "Email",
          "desc": "You have received this badge, your email is verified.",

          // "You receive this badge upon verification of your email address. This will also allow you to receive case related notifications and send or receive emails from the portal.",
        },
        {
          "icon":
              "assets/images/screens/db_cus/more/badge/passport_verified.png",
          "title": "Passport / Photo ID",
          "desc": "You have received this badge, your ID Card is verified",
          // "You receive this badge when your ID is verified with your Passport, Driving License or any other acceptable form of ID.",
        },
        {
          "icon":
              "assets/images/screens/db_cus/more/badge/facebook_verified.png",
          "title": "Facebook",
          "desc":
              "You have received this badge, connected with Facebook account.",
          // "You receive this badge when you connect your Facebook account.",
        },
        {
          "icon":
              "assets/images/screens/db_cus/more/badge/customer_privacy_verified.png",
          "title": "Customer Privacy",
          "desc":
              "You have received this badge, Accepted Customer Privacy Statement",
          // "You receive this badge when you agree to the Customer Privacy Statement. Customer Privacy accepted at 16-Mar-2020 18:43",
        },
        {
          "icon": "assets/images/screens/db_cus/more/badge/eid_verified.png",
          "title": "Electronic ID Verification",
          "desc":
              "You have received this badge, Your ID is verified through an EID system.",
          // "You receive this badge when your ID is verified through an EID system.",
        },
        //"Application tutorial"
      ];
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        //resizeToAvoidBottomPadding: true,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                elevation: MyTheme.appbarElevation,
                backgroundColor: MyTheme.themeData.accentColor,
                iconTheme: IconThemeData(
                    color: MyTheme.brandColor //change your color here
                    ),
                automaticallyImplyLeading: false,
                leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () async {
                      Get.back();
                    }),
                title: UIHelper().drawAppbarTitle(title: "Profile"),
                actions: [
                  IconButton(
                    iconSize: 25,
                    icon: Icon(
                      Icons.edit,
                      color: MyTheme.brandColor,
                    ),
                    onPressed: () {
                      Get.to(() => EditProfileScreen()).then((value) {
                        getProfile();
                      });
                    },
                  )
                ],
                expandedHeight: getHP(context, 0),
                floating: false,
                pinned: true,
              ),
            ];
          },
          body: (isLoading || isLoadingBadge)
              ? SizedBox()
              : ListView(
                  shrinkWrap: true,
                  primary: true,
                  children: [
                    Card(
                      elevation: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 8.0, top: 15.0, bottom: 15.0, right: 5.0),
                        child: Row(
                          children: [
                            Container(
                              decoration: MyTheme.picEmboseCircleDeco,
                              child: CircleAvatar(
                                radius: 30,
                                backgroundColor: Colors.transparent,
                                backgroundImage: new CachedNetworkImageProvider(
                                  MyNetworkImage.checkUrl(
                                      userModel.profileImageURL != null &&
                                              userModel
                                                  .profileImageURL.isNotEmpty
                                          ? userModel.profileImageURL
                                          : Server.MISSING_IMG),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Expanded(
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Txt(
                                      maxLines: 1,
                                      txt: userModel.name,
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.start,
                                      isBold: true,
                                    ),
                                    Txt(
                                      maxLines: 1,
                                      txt: Common.getPhoneNumber(
                                          userModel.mobileNumber),
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                    ),
                                    Txt(
                                      maxLines: 2,
                                      txt: userModel.email,
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 20, left: 15.0),
                            child: Txt(
                              txt: "Basic Information :",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true,
                            ),
                          ),
                          userModel.address.isNotEmpty &&
                                  userModel.address != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
                          //address

                          userModel.address.isNotEmpty &&
                                  userModel.address != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.home_outlined,color: Colors.black,size: 36,),
                                      Image.asset(
                                        "assets/images/icons/home_icon.png",
                                        width: 36,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Txt(
                                          txt: "${userModel.address}",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                          maxLines: 5,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                          userModel.countryofResidency.isNotEmpty &&
                                  userModel.countryofResidency != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
//country
                          userModel.countryofResidency.isNotEmpty &&
                                  userModel.countryofResidency != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.flag,color: Colors.black,size: 36,),
                                      Image.asset(
                                        "assets/images/icons/glob_ic.png",
                                        width: 25,
                                        color: Colors.grey,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Txt(
                                        txt: "${userModel.countryofResidency}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                          userModel.dateofBirth.isNotEmpty &&
                                  userModel.dateofBirth != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
                          //Date of birth

                          userModel.dateofBirth.isNotEmpty &&
                                  userModel.dateofBirth != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.date_range,color: Colors.black,size: 36,),

                                      Image.asset(
                                        "assets/images/icons/date_icon.png",
                                        width: 36,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Txt(
                                        txt: "${userModel.dateofBirth}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),

                          userModel.maritalStatus.isNotEmpty &&
                                  userModel.maritalStatus != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
                          //marital status

                          userModel.maritalStatus.isNotEmpty &&
                                  userModel.maritalStatus != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.update,color: Colors.black,size: 36,),
                                      Image.asset(
                                        "assets/images/icons/gender_icon.png",
                                        width: 36,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Txt(
                                        txt: "${userModel.maritalStatus}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            // margin: EdgeInsets.all(8.0),
                            color: Colors.grey[200],
                            child: Row(
                              // crossAxisAlignment: CrossAxisAlignment.s,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Txt(
                                  txt: "Badges",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                GestureDetector(
                                    onTap: () {},
                                    child: Btn(
                                        txt: "Add More",
                                        txtColor: Colors.white,
                                        bgColor: MyTheme.brandColor,
                                        width: null, //155,
                                        height: getHP(context, 5),
                                        callback: () {
                                          Get.to(() => BadgeScreen())
                                              .then((value) {});
                                        })),
                              ],
                            ),
                          ),

                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: listUserBadgeModelShowList.length,
                            itemBuilder: (context, index) {
                              var item;

                              if (listUserBadgeModelShowList[index].type ==
                                  "Mobile") {
                                item = listItems[0];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "Email") {
                                item = listItems[1];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "Passport") {
                                item = listItems[2];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "Facebook") {
                                item = listItems[3];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "CustomerPrivacy") {
                                item = listItems[4];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "NationalIDCard") {
                                item = listItems[5];
                              }

                              return (item == null)
                                  ? SizedBox()
                                  : Card(
                                      elevation: 0,
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: ListTile(
                                          title: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                  width: getWP(context, 8),
                                                  height: getWP(context, 8),
                                                  child: Image.asset(
                                                      item['icon'])),
                                              SizedBox(width: 10),
                                              Expanded(
                                                child: Txt(
                                                    txt: item["title"],
                                                    txtColor: Colors.black,
                                                    txtSize:
                                                        MyTheme.txtSize + .4,
                                                    txtAlign: TextAlign.start,
                                                    isOverflow: true,
                                                    isBold: false),
                                              ),
                                            ],
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10, bottom: 10),
                                                child: Txt(
                                                    txt: item["title"] ==
                                                            "Customer Privacy"
                                                        ? item["desc"] +
                                                            " at ${DateFormat('dd-MMM-yyyy').format(DateTime.parse(listUserBadgeModelShowList[index].creationDate.toString()))}"
                                                        : item["desc"],
                                                    txtColor: Colors.grey,
                                                    txtSize:
                                                        MyTheme.txtSize - .2,
                                                    txtAlign: TextAlign.start,
                                                    //txtLineSpace: 1.4,
                                                    isBold: false),
                                              ),
                                              Container(
                                                color: Colors.grey[200],
                                                height: 1,
                                                width: getW(context),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
