import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/config/db_cus/TimelineCfg.dart';
import 'package:aitl/controller/api/db_cus/timeline/TimelineAPIMgr.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';

import 'MessageTypeList.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<TimeLineTab> with Mixin, StateListener {
  StateProvider _stateProvider;

  List<TimelineUserModel> listTimelineUserModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  int caseStatus = NewCaseCfg.ALL;

  int totalMsg = 4;
  int totalNoti = 2;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_dashboard_reload) {
      Get.back();
    }
  }

  onPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        status: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> timelineUserModel =
                      model.responseData.users;
                  if (timelineUserModel != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (timelineUserModel.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (TimelineUserModel user in timelineUserModel) {
                        listTimelineUserModel.add(user);
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }
                    myLog(listTimelineUserModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(context: context, msg: "Timelines not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            myLog("not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTimelineUserModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimelineUserModel = null;
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white, //MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: Colors.white,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: UIHelper().drawAppbarTitle(title: "Private Messages"),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: new Size(
                          getW(context), getHP(context, (isLoading) ? .5 : 0)),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (listTimelineUserModel.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    pageStart++;
                    onPageLoad();
                  }
                }
                return true;
              },
              child: RefreshIndicator(
                color: Colors.white,
                backgroundColor: MyTheme.brandColor,
                onRefresh: _getRefreshData,
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: listTimelineUserModel.length,
                    itemBuilder: (context, index) {
                      return drawRecentCaseItem(index);
                    },
                  ),
                ),
              ),
            )
          : (!isLoading)
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          child: Image.asset(
                            'assets/images/screens/home/my_cases/case_nf.png',
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt:
                                  "Looks like you haven't got any timeline yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize + .2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        GestureDetector(
                          onTap: () {
                            onPageLoad();
                          },
                          child: Txt(
                              txt: "Refresh",
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ],
                    ),
                  ),
                )
              : SizedBox(),
    );
  }

  drawRecentCaseItem(index) {
    try {
      TimelineUserModel timelineUserModel = listTimelineUserModel[index];
      final unreadMessageCount = (timelineUserModel.unreadMessageCount == 0);
      final doubletik =
          (unreadMessageCount) ? "check2_icon.png" : "check1_icon.png";
      String lastLoginDate = timelineUserModel.lastChatDateTime ?? '';
      lastLoginDate = Jiffy(lastLoginDate).format("HH:mm");
      bool isHideFieldsIfLessByYear = false;
      try {
        DateTime checkedTime = DateTime.parse(lastLoginDate);
        DateTime currentTime = DateTime.now();
        final diff = currentTime.difference(checkedTime);
        var year = ((diff.inDays) / 365).round();
        if (year > TimelineCfg.hideFieldsIfLessByYear) {
          isHideFieldsIfLessByYear = true;
        }
      } catch (e) {}
      try {
        lastLoginDate = DateFun.getDateWhatsAppFormat(lastLoginDate);
      } catch (e) {}
      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
              () => MessageTypeList(
                title: timelineUserModel.name,
                online: timelineUserModel.isOnline,
                adviserOrIntroducerId: timelineUserModel.id,
                pictureUrl: timelineUserModel.profileImageUrl,
                mobileNo: timelineUserModel.mobileNumber,
              ),
            ).then((value) {
              //callback(route);
            });
          } catch (e) {
            myLog(e.toString());
          }
        },
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Container(
                        decoration: MyTheme.picEmboseCircleDeco,
                        child: CircleAvatar(
                          radius: 25,
                          backgroundColor: Colors.transparent,
                          backgroundImage: new CachedNetworkImageProvider(
                            MyNetworkImage.checkUrl(
                                (timelineUserModel.profileImageUrl != null)
                                    ? timelineUserModel.profileImageUrl
                                    : MyDefine.MISSING_IMG),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      flex: (!isHideFieldsIfLessByYear) ? 5 : 9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5),
                          Txt(
                              txt: timelineUserModel.name,
                              txtColor: Colors.black87,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                              txt: timelineUserModel
                                  .community, //caseModel.companyName,
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize - .4,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 5),
                          (!isHideFieldsIfLessByYear)
                              ? Row(
                                  children: [
                                    Image.asset(
                                      "assets/images/icons/" + doubletik,
                                      width: 25,
                                      height: 25,
                                      color: (unreadMessageCount)
                                          ? Colors.green
                                          : Colors.grey,
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                        child: Txt(
                                      txt: timelineUserModel.lastMessage,
                                      txtColor: Colors.grey,
                                      txtSize: 1.5,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                      isOverflow: true,
                                    )),
                                  ],
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                    //Spacer(),
                    //SizedBox(width: 10),
                    (!isHideFieldsIfLessByYear)
                        ? Expanded(
                            flex: 4,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(height: 5),
                                Container(
                                  //color: Colors.black,
                                  child: Txt(
                                      txt: lastLoginDate,
                                      txtColor: (timelineUserModel
                                                  .unreadMessageCount >
                                              0)
                                          ? Colors.green
                                          : Colors.grey,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.end,
                                      isBold: false),
                                ),
                                SizedBox(height: 10),
                                (timelineUserModel.unreadMessageCount > 0)
                                    ? Container(
                                        width: 30,
                                        height: 30,
                                        child: Center(
                                          child: Txt(
                                              txt: timelineUserModel
                                                  .unreadMessageCount
                                                  .toString(),
                                              txtColor: Colors.white,
                                              txtSize: MyTheme.txtSize - .4,
                                              txtAlign: TextAlign.start,
                                              isBold: true),
                                        ),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.green),
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          )
                        : SizedBox(),
                    //SizedBox(width: 5),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: 10, left: getWP(context, 15), right: 20),
                  child: Container(color: Colors.grey, height: .5),
                ),
                //SizedBox(height: 20),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
  }
}
