import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/timeline/MessageTypeAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/MessageTypeAdviserModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/timeline/chat/TimeLinePostScreenNew.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class MessageTypeList extends StatefulWidget {
  String title;
  int adviserOrIntroducerId;
  bool online;
  String pictureUrl;
  String mobileNo;

  MessageTypeList(
      {this.title,
      this.online,
      this.adviserOrIntroducerId,
      this.pictureUrl,
      this.mobileNo});

  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<MessageTypeList>
    with Mixin, StateListener {
  StateProvider _stateProvider;
  bool isLoading = false;
  MessageTypeAdviserModel messageTypeAdviserModel;

  List<MessageTypeAdviserModel> messageTypeAdviserModelList = [];

  onPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });
      MessageTypeAPIMgr().wsOnMessageTypeLoad(
        context: context,
        adviserOrIntroducerId: widget.adviserOrIntroducerId,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                messageTypeAdviserModel = new MessageTypeAdviserModel(
                    id: 0,
                    title: "General Chat",
                    description: "",
                    addressOfPropertyToBeMortgaged: "General Address",
                    howMuchDoYouWishToBorrow: "0");
                messageTypeAdviserModelList.add(messageTypeAdviserModel);
                try {
                  final List<dynamic> modelList =
                      model.responseData.messageTypeAdviserModelList;
                  print("messageTypeAdvieAdvisermodelList.length}");
                  if (modelList != null && mounted) {
                    try {
                      for (MessageTypeAdviserModel messageType in modelList) {
                        messageTypeAdviserModelList.add(messageType);
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }
                    myLog("Message Type Response = " +
                        messageTypeAdviserModelList.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(context: context, msg: "Timelines not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            myLog("not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    isLoading = true;
    messageTypeAdviserModelList.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    messageTypeAdviserModelList = null;

    _stateProvider.unsubscribe(this);
    _stateProvider = null;

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        Get.back();
                      }),
                  title: UIHelper().drawAppbarTitle(title: widget.title),

                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: new Size(
                          getW(context), getHP(context, (isLoading) ? .5 : 0)),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        height: getH(context),
        child: NotificationListener(
          child: RefreshIndicator(
            color: Colors.white,
            backgroundColor: MyTheme.brandColor,
            onRefresh: _getRefreshData,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: ListView.builder(
                addAutomaticKeepAlives: true,
                cacheExtent: AppConfig.page_limit.toDouble(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: messageTypeAdviserModelList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, right: 8.0, top: 20, bottom: 20),
                    child: GestureDetector(
                      onTap: () {
                        Get.to(
                          () => TimeLinePostScreenNew(
                            title: messageTypeAdviserModelList[index].title,
                            taskId: messageTypeAdviserModelList[index].id,
                            adviserID: widget.adviserOrIntroducerId,
                            online: widget.online,
                            imageurl: widget.pictureUrl,
                            name: widget.title,
                            mobileNo: widget.mobileNo,
                          ),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: 20),
                        width: getWP(context, 100),
                        height: getHP(context, 17),
                        decoration: BoxDecoration(
                            color: MyTheme.brandColor,
                            borderRadius: new BorderRadius.circular(10)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 7,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  messageTypeAdviserModelList[index].id != 0
                                      ? Txt(
                                          txt:
                                              "Case No ${messageTypeAdviserModelList[index].id}",
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.left,
                                          isBold: true,
                                        )
                                      : SizedBox(),
                                  Txt(
                                    txt:
                                        "${messageTypeAdviserModelList[index].title}",
                                    txtColor: Colors.white,
                                    txtSize:
                                        messageTypeAdviserModelList[index].id ==
                                                0
                                            ? MyTheme.txtSize
                                            : MyTheme.txtSize - .1,
                                    txtAlign: TextAlign.center,
                                    isBold:
                                        messageTypeAdviserModelList[index].id ==
                                                0
                                            ? true
                                            : false,
                                  ),
                                  messageTypeAdviserModelList[index].id == 0
                                      ? SizedBox()
                                      : Txt(
                                          maxLines: 2,
                                          txt:
                                              "${messageTypeAdviserModelList[index].addressOfPropertyToBeMortgaged.trim()}",
                                          txtColor: Colors.white60,
                                          txtSize:
                                              messageTypeAdviserModelList[index]
                                                          .id ==
                                                      0
                                                  ? MyTheme.txtSize
                                                  : MyTheme.txtSize - .3,
                                          txtAlign: TextAlign.left,
                                          isBold:
                                              messageTypeAdviserModelList[index]
                                                          .id ==
                                                      0
                                                  ? true
                                                  : false,
                                        ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Icon(
                                  Icons.message,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    /*child: Btn(
                        txt: "Case No ${messageTypeAdviserModelList[index].id} \n${messageTypeAdviserModelList[index].title}",
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        width: getW(context),
                        height: 75,
                        callback: () {

                          navTo(
                            context: context,
                            page: () => TimeLinePostScreenNew(
                              title: messageTypeAdviserModelList[index].title,
                              taskId: messageTypeAdviserModelList[index].id,
                              adviserID: widget.adviserOrIntroducerId,
                                online: widget.online
                            ),
                          ).then((value) {
                            //callback(route);
                          });




                        }),*/
                  );
                },
              ),
            ),
          ),
        ));
  }

  @override
  void onStateChanged(ObserverState state) {
    // TODO: implement onStateChanged
  }
}
