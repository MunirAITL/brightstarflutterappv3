import 'dart:convert';
import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/PubNubCfg.dart';
import 'package:aitl/controller/api/misc/DeviceInfoAPIMgr.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/observer/StateListnerIntro.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/db_cus/my_cases/MyCaseTab.dart';
import 'package:aitl/view/db_cus/noti/NotiTab.dart';
import 'package:aitl/view/db_cus/timeline/TimeLineTab.dart';
import 'package:aitl/view/welcome/WelcomeScreen.dart';
import 'package:aitl/view/widgets/dialog/HelpTutDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
//import 'package:flutter_incoming_call/flutter_incoming_call.dart';
import 'package:pubnub/pubnub.dart';
import '../widgets/tabs_nav/intro/bottom_nav_Intro.dart';
import '../widgets/tabs_nav/intro/tab_item_intro.dart';
import 'caselead/LeadSummaryTab.dart';
import 'createlead/LeadsScreen.dart';
import 'customer/CreateLeadCase.dart';
import 'mycases/mycases_tab.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class MainIntroducerScreen extends StatefulWidget {
  MainIntroducerScreen();

  @override
  State createState() => MainIntroducerScreenState();
}

class MainIntroducerScreenState extends State<MainIntroducerScreen>
    with Mixin, StateListenerIntro, TickerProviderStateMixin {
  static const platform = const MethodChannel('flutter.native/tokbox');
  var _androidAppRetain = MethodChannel("android_app_retain");

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProviderIntro _stateProvider;
  bool isDialogHelpOpenned = false;
  static int currentTab = 0;

  //  Convage OpenTok Incoming Start
  //IncomingCallHandler incomingCallHandler;

  /*BaseCallEvent _lastEvent;
  CallEvent _lastCallEvent;
  HoldEvent _lastHoldEvent;
  MuteEvent _lastMuteEvent;
  DmtfEvent _lastDmtfEvent;
  AudioSessionEvent _lastAudioSessionEvent;*/

  var pubnub;
  var myChannel;
  var isOnCall = false;

  /*initIncoming() async {
    final sender_id = userData.userModel.id.toString();
    pubnub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKeyIncomingApp,
            publishKey: PubNubCfg.publishKeyIncomingApp,
            uuid: UUID(sender_id)));

    var subscription = pubnub.subscribe(channels: {sender_id});
    /*subscription.messages.take(1).listen((envelope) async {
      print(envelope.payload);
      //await subscription.dispose();
    });*/

    //await Future.delayed(Duration(seconds: 3));

    // Publish a message
    //await pubnub.publish(
    //receiver_id.toString(), {'message': 'My message BY flutter app1!'});

    // Channel abstraction for easier usage
    //var channel = pubnub.channel('test');

    //await channel.publish({'message': 'Another message BY flutter app2'});

    // Work with channel History API
    //var history = channel.messages();
    //var count = await history.count();

    //print('Messages on test channel: $count');

    subscription.messages.listen((envelope) async {
      print('sent a message full : ${envelope.payload}');
      //startIncoming("");
    });
  }*/

  /*void startIncoming(message) async {
    print(message);
    //if (incomingCallHandler == null) {
    incomingCallHandler = IncomingCallHandler();
    await FlutterIncomingCall.configure(
        appName: AppDefine.APP_NAME,
        duration: 30000,
        android: ConfigAndroid(
          vibration: true,
          ringtonePath: 'default',
          channelId: 'calls',
          channelName: 'Calls channel name',
          channelDescription: 'Calls channel description',
        ),
        ios: ConfigIOS(
          iconName: 'AppIcon40x40',
          ringtonePath: null,
          includesCallsInRecents: false,
          supportsVideo: true,
          maximumCallGroups: 2,
          maximumCallsPerCallGroup: 1,
        ));
    FlutterIncomingCall.onEvent.listen((event) {
      print("incoming event = " + event.toString());
      setState(() {
        _lastEvent = event;
      });
      if (event is CallEvent) {
        //setState(() async {
        _lastCallEvent = event;
        if (event.action == CallAction.accept) {
          OpenTokConfig().set(
              "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9NWMwNTAwMGVhNjFjMDliNjY0ODExYmVjYmU5NjljZGE3MTA0M2ZiZDpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzODAyMTA1Jm5vbmNlPTAuMzc4NTI4MDIwNzA3NTQ2NzYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTY0NjM5NDEwNCZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==");
          if (Platform.isIOS) {
            // iOS-specific code
            final param = {
              'apiKey': OpenTokConfig.API_KEY,
              'sessionId': OpenTokConfig.SESSION_ID,
              'token': OpenTokConfig.TOKEN
            };
            platform.invokeMethod('start', param);
          } else {
            Get.to(() => CallWidget());
          }
        }
        //});
      } else if (event is HoldEvent) {
        setState(() {
          _lastHoldEvent = event;
        });
      } else if (event is MuteEvent) {
        setState(() {
          _lastMuteEvent = event;
        });
      } else if (event is DmtfEvent) {
        setState(() {
          _lastDmtfEvent = event;
        });
      } else if (event is AudioSessionEvent) {
        setState(() {
          _lastAudioSessionEvent = event;
        });
      }
    });
    await incomingCallHandler.invoke(message: message);
    //}
  }*/

  //  Convage OpenTok Incoming End

  @override
  onStateChanged(ObserverStateIntro state) async {
    int tabIndex = 0;
    if (state == ObserverStateIntro.STATE_CHANGED_logout) {
      await CookieMgr().delCookiee();
      await DBMgr.shared.delTable("User");
      Get.offAll(() => WelcomeScreen());
    } else if (state == ObserverStateIntro.STATE_CHANGED_tabbar1) {
      tabIndex = 0;
      setState(() {
        selectTab(tabIndex);
      });
    } else if (state == ObserverStateIntro.STATE_CHANGED_tabbar2) {
      tabIndex = 1;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverStateIntro.STATE_CHANGED_tabbar3) {
      tabIndex = 2;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverStateIntro.STATE_CHANGED_tabbar4) {
      tabIndex = 3;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverStateIntro.STATE_CHANGED_tabbar5) {
      tabIndex = 4;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    }
  }

  //  Tabbar stuff start here...
  final List<TabItemIntro> tabsItem = [
    TabItemIntro(
      tabName: "Dashboard",
      icon: AssetImage("assets/images/tabbar/createlead_icon.png"),
      page: MainCustomerScreen(), //MyCaseTab(),
    ),
    /*TabItemIntro(
      tabName: "New Case",
      icon: AssetImage("assets/images/tabbar/my_cases_icon.png"),
      page: CreateLeadCase(isNewCase: true), //MyCasesTab(),
    ),*/
    TabItemIntro(
      tabName: "Lead",
      icon: AssetImage("assets/images/tabbar/new_case_icon.png"),
      page: LeadsScreen(),
    ),
    /*TabItemIntro(
      tabName: "New Case",
      icon: AssetImage("assets/images/tabbar/my_cases_icon.png"),
      page: CreateLeadCase(isNewCase: true), //MyCasesTab(),
    ),*/
    TabItemIntro(
      tabName: "Summary",
      icon: AssetImage("assets/images/tabbar/my_cases_icon.png"),
      //page: NewCaseTab(),
      page: SummaryTab(),
    ),
    /*TabItemIntro(
      tabName: "Messages",
      icon: AssetImage("assets/images/tabbar/msg_icon.png"),
      page: Container(), //TimeLineTab(),
    ),
    TabItem(
      tabName: "Notifications",
      icon: AssetImage("assets/images/tabbar/noti_icon.png"),
      page: NotiTab(),
    ),
    TabItemIntro(
      tabName: "More",
      icon: AssetImage("assets/images/tabbar/more_icon.png"),
      page: MoreTab(),
    )*/
  ];

  MainIntroducerScreenState() {
    tabsItem.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  // sets current tab index
  // and update state
  selectTab(int index) {
    if (mounted) {
      switch (index) {
        case 0:
          StateProviderIntro()
              .notify(ObserverStateIntro.STATE_CHANGED_tabbar1_reload_case_api);
          break;
        case 1:
          StateProviderIntro()
              .notify(ObserverStateIntro.STATE_CHANGED_tabbar2_reload_case_api);
          break;
        case 2:
          StateProviderIntro()
              .notify(ObserverStateIntro.STATE_CHANGED_tabbar3_reload_case_api);
          break;
        case 3:
          StateProviderIntro()
              .notify(ObserverStateIntro.STATE_CHANGED_tabbar4_reload_case_api);
          break;
        default:
      }

      if (index == currentTab) {
        // pop to first route
        // if the user taps on the active tab
        print("Current State 3 = " +
            MainIntroducerScreenState.currentTab.toString());
        print("Current State 33 = " + index.toString());

        tabsItem[index].key.currentState.popUntil((route) => route.isFirst);
        //setState(() {});
      } else {
        // update the state
        // in order to repaint
        print("Current State 4 = " + index.toString());

        if (mounted) {
          setState(() => currentTab = index);
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
    //initIncoming();
  }

  @mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    //if (incomingCallHandler != null) {
    //incomingCallHandler.endAllCalls();
    //}
    super.dispose();
  }

  appInit() async {
    try {
      //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      //SystemChrome.setSystemUIOverlayStyle(
      //SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: MyTheme.titleColor,
      ));
      _stateProvider = new StateProviderIntro();
      _stateProvider.subscribe(this);

      DeviceInfoAPIMgr().wsFcmDeviceInfo(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {}
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog("main customer screen new not in");
          }
        },
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (Platform.isAndroid) {
          if (Navigator.of(context).canPop()) {
            return Future.value(true);
          } else {
            _androidAppRetain.invokeMethod("sendToBackground");
            return Future.value(false);
          }
        } else {
          return Future.value(true);
        }
      },
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor2,
          body: IndexedStack(
            index: currentTab,
            children: tabsItem.map((e) => e.page).toList(),
          ),
          // Bottom navigation
          bottomNavigationBar: BottomNavigationIntro(
            context: context,
            onSelectTab: selectTab,
            tabs: tabsItem,
            isHelpTut: isDialogHelpOpenned,
            totalMsg: 0,
            totalNoti: 0,
          ),
        ),
      ),
    );
  }
}
