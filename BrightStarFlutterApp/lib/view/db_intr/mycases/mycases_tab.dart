import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:flutter/material.dart';
import '../../../config/AppConfig.dart';
import '../../../config/MyTheme.dart';
import '../../../config/ServerIntr.dart';
import '../../../config/db_cus/NewCaseCfg.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../controller/observer/StateListnerIntro.dart';
import '../../../model/data/UserData.dart';
import '../../../model/json/db_cus/tab_mycases/TaskInfoSearchAPIModel.dart';
import '../../../model/json/db_cus/tab_newcase/LocationsModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../../view_model/helper/ui_helper.dart';
import 'package:get/get.dart';
import '../../widgets/dropdown/DropDownButton2.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/txt/Txt.dart';
import '../createlead/leaddetails/create_lead.dart';
import 'mycases_base.dart';

class MyCasesTab extends StatefulWidget {
  const MyCasesTab({Key key}) : super(key: key);
  @override
  State<MyCasesTab> createState() => _MyCasesTabState();
}

class _MyCasesTabState extends BaseMyCases<MyCasesTab> {
  List<LocationsModel> listTaskInfoSearchModel = [];
  final searchByTxt = TextEditingController();

  int pageStart = 0;
  int pageCount = AppConfig.page_limit;
  bool isPageDone = false;
  bool isLoading = false;

  bool isCustomDate = false;

  String selectedCase = "All cases";
  final List<String> listCaseType = [];

  String selectedStatus = "All cases";
  final listCaseStatus = [
    "All cases",
    "Submitted",
    "Recommendation",
    "FMA",
    "Completed",
    "Ongoing",
    "AIP",
    "Valuation instructed",
    "Valuation satisfied",
    "Offered",
    "Cancelled",
    "Declined",
  ];

  Future<void> refreshData() async {
    setState(() {
      pageStart = 0;
      isPageDone = false;
      isLoading = true;
      listTaskInfoSearchModel.clear();
    });
    onLazyLoadAPI();
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
    }
    try {
      var status = IntroCfg.ALL;
      switch (selectedStatus) {
        case "All cases":
          status = IntroCfg.ALL;
          break;
        case "Submitted":
          status = IntroCfg.SUBMITTED;
          break;
        case "Recommendation":
          status = IntroCfg.RECOMMENDTION;
          break;
        case "FMA":
          status = IntroCfg.FMA;
          break;
        case "Completed":
          status = IntroCfg.COMPLETED;
          break;
        case "Ongoing":
          status = IntroCfg.ONGOING;
          break;
        case "AIP":
          status = IntroCfg.AIP;
          break;
        case "Valuation instructed":
          status = IntroCfg.VALUATION_INSTRUCTED;
          break;
        case "Valuation satisfied":
          status = IntroCfg.VALUATION_SATISFIED;
          break;
        case "Offered":
          status = IntroCfg.OFFERED;
          break;
        case "Cancelled":
          status = IntroCfg.CANCELLED;
          break;
        case "Declined":
          status = IntroCfg.DECLINED;
          break;
        default:
          break;
      }
      var url = ServerIntr.GET_CASEINFOBYCOMPANYADMINSEARCH_URL;
      url += "?SearchText=" +
          searchByTxt.text.trim() +
          "&Distance=5&Location=&InPersonOrOnline=0&FromPrice=20&ToPrice=100000&IsHideAssignTask=0&Latitude=0&Longitude=0&UserId=undefined" +
          //userData.userModel.id.toString() +
          "&Page=" +
          pageStart.toString() +
          "&Count=" +
          pageCount.toString() +
          "&Status=" +
          status.toString() +
          "&FromDateTime=" +
          date1 +
          "&ToDateTime=" +
          date2 +
          "&UserTaskCategoryId=0&UserTaskItemId=0&UserCompanyId=" +
          userData.userModel.userCompanyID.toString() +
          "&IsSpecificDate=" +
          (isCustomDate ? "Specific Date" : "All") +
          "&AdviserId=0&IntroducerId=0&Title=" +
          selectedCase.replaceAll("All cases", "All");

      await APIViewModel().req<TaskInfoSearchAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                final List<dynamic> locations = model.responseData.locations;
                if (locations != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (locations.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (LocationsModel location in locations) {
                      listTaskInfoSearchModel.add(location);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                  myLog(listTaskInfoSearchModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              }
            }
          });
    } catch (e) {}
    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      resetDate();

      listCaseType.add(selectedCase);
      for (var map in NewCaseCfg.listCreateNewCase) {
        listCaseType.add(map['title']);
      }

      refreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          elevation: 1,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                UIHelper().drawAppbarTitle(title: "Manage cases"),
                /*Spacer(),
                GestureDetector(
                  onTap: () {
                    Get.to(() => CreateLeadPage()).then((value) {
                      StateProviderIntro()
                          .notify(ObserverStateIntro.STATE_CHANGED_tabbar2);
                    });
                  },
                  child: Container(
                    margin: const EdgeInsets.all(1),
                    padding: const EdgeInsets.all(1),
                    decoration: BoxDecoration(
                        border: Border.all(color: MyTheme.brandColor)),
                    child: Icon(Icons.add_outlined,
                        color: MyTheme.brandColor, size: 20),
                  ),
                )*/
              ]),
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
          backgroundColor: MyTheme.titleColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            //print('Widget has started scrolling');
          } else if (scrollNotification is ScrollEndNotification) {
            if (!isPageDone && listTaskInfoSearchModel.length > 0) {
              pageStart++;
              onLazyLoadAPI();
            }
          }
          return true;
        },
        child: SingleChildScrollView(
            primary: true,
            child: Column(children: [
              drawSearchHeader(),
              (listTaskInfoSearchModel.length > 0)
                  ? Card(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Table(
                          border: TableBorder(
                              horizontalInside: BorderSide(
                                  width: .5,
                                  color: Colors.grey,
                                  style: BorderStyle
                                      .solid)), // Allows to add a border decoration around your table
                          children: [
                            TableRow(
                                decoration:
                                    BoxDecoration(color: MyTheme.brandColor),
                                children: [
                                  getTableHeading('Type'),
                                  getTableHeading('Case No'),
                                  getTableHeading('Status'),
                                  getTableHeading('Client Name'),
                                ]),
                            ...listTaskInfoSearchModel.map((model) {
                              return drawItems(model);
                            })
                          ],
                        ),
                      ),
                    )
                  : (isLoading)
                      ? CircularProgressIndicator()
                      : drawNF(),
            ])));
  }

  drawSearchHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
      child: Container(
        width: getW(context),
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                    txt: "Search by date range",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.center,
                    isBold: false),
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                          child: GestureDetector(
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                isCustomDate = false;
                                setState(() {});
                              },
                              child: radioButtonitem2(
                                  context: context,
                                  text: "All",
                                  txtSize: 1.7,
                                  bgColor: !isCustomDate
                                      ? MyTheme.purpleColor
                                      : Color(0xFF000),
                                  textColor: Colors.black))),
                      SizedBox(width: 10),
                      Flexible(
                          child: GestureDetector(
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                isCustomDate = true;
                                setState(() {});
                              },
                              child: radioButtonitem2(
                                  context: context,
                                  text: "Specific date",
                                  txtSize: 1.7,
                                  bgColor: isCustomDate
                                      ? MyTheme.purpleColor
                                      : Color(0xFF000),
                                  textColor: Colors.black))),
                    ],
                  ),
                ),
                isCustomDate
                    ? Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: drawCustomDate())
                    : SizedBox(),
              ],
            ),
            SizedBox(height: 5),
            listCaseType != null
                ? drawDDList(
                    context: context,
                    title: "Case type",
                    items: listCaseType,
                    indexHint: 0,
                    selectedValue: selectedCase,
                    callback: (value) {
                      selectedCase = value;
                      setState(() {});
                    })
                : SizedBox(),
            SizedBox(height: 5),
            drawDDList(
                context: context,
                title: "Case status",
                items: listCaseStatus,
                indexHint: 0,
                selectedValue: selectedStatus,
                callback: (value) {
                  selectedStatus = value;
                  setState(() {});
                }),
            SizedBox(height: 5),
            SizedBox(
              height: 40,
              child: TextField(
                controller: searchByTxt,
                style: TextStyle(color: Colors.black, fontSize: 14),
                decoration: new InputDecoration(
                  isDense: true,
                  counterText: "",
                  contentPadding: EdgeInsets.zero,
                  prefixIcon: new Icon(Icons.search, color: Colors.grey),
                  suffixIcon: GestureDetector(
                      onTap: () {
                        searchByTxt.clear();
                      },
                      child: Icon(
                        Icons.close,
                        color: Colors.black,
                      )),
                  hintText: "Case id/name/email/phone/broker name/spv company",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: const BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                      child: OutlinedButton(
                          onPressed: () {
                            isCustomDate = false;
                            selectedCase = "All cases";
                            selectedStatus = "All cases";
                            searchByTxt.clear();
                            resetDate();
                            refreshData();
                          },
                          style: OutlinedButton.styleFrom(
                            backgroundColor:
                                MyTheme.purpleColor.withOpacity(.9),
                            side: BorderSide(width: 1, color: Colors.white),
                          ),
                          child: Text(
                            "Refresh",
                            style: TextStyle(color: Colors.white),
                          ))),
                  Flexible(
                    child: OutlinedButton(
                      onPressed: () {
                        refreshData();
                      },
                      style: OutlinedButton.styleFrom(
                        backgroundColor: MyTheme.purpleColor,
                        side: BorderSide(width: 1, color: Colors.white),
                      ),
                      child: const Text(
                        "Search",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
