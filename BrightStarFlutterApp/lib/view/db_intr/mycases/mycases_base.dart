import 'package:aitl/view/db_intr/intro_mixin.dart';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../config/db_intro/intro_cfg.dart';
import '../../../controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import '../../../model/json/db_cus/tab_newcase/LocationsModel.dart';
import '../../widgets/txt/Txt.dart';
import 'package:get/get.dart';

import '../../widgets/webview/WebScreen.dart';

abstract class BaseMyCases<T extends StatefulWidget> extends State<T>
    with Mixin, IntroMixin {
  final DateTime dateNow = DateTime.now();

  var date1 = "";
  var date2 = "";

  var datelast;
  var datefirst;
  var datelast2;
  var datefirst2;

  resetDate() {
    datelast = DateTime(dateNow.year, dateNow.month, dateNow.day);
    datefirst = DateTime(dateNow.year - 10, dateNow.month, dateNow.day - 7);
    datelast2 = DateTime(dateNow.year, dateNow.month, dateNow.day);
    datefirst2 = DateTime(dateNow.year - 10, dateNow.month, dateNow.day);
    date1 = DateFormat('dd-MMM-yyyy')
        .format(DateTime.now().subtract(new Duration(days: 7)));
    date2 = DateFormat('dd-MMM-yyyy').format(DateTime.now());
  }

  drawCustomDate() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
              child: _drawCusDateRow(
                  "From", date1, datefirst, datelast, datefirst, (DateTime d) {
            setState(() {
              date1 = DateFormat('dd-MMM-yyyy').format(d).toString();
            });
          })),
          SizedBox(width: 10),
          Flexible(
              child: _drawCusDateRow(
                  "To", date2, datefirst2, datelast2, DateTime.now(), (d) {
            setState(() {
              date2 = DateFormat('dd-MMM-yyyy').format(d).toString();
            });
          })),
        ],
      ),
    );
  }

  _drawCusDateRow(String title, String dLabel, fdate, ldate, initialDate,
      Function(DateTime) callback) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          //width: getWP(context, 25),
          decoration: BoxDecoration(
              color: Colors.grey.shade200,
              border: Border.all(
                color: Colors.black26,
              ),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                bottomLeft: Radius.circular(5),
              )),
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .5,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
        Expanded(
          flex: 3,
          child: GestureDetector(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: fdate,
                lastDate: ldate,
                fieldHintText: "dd/mm/yyyy",
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(
                        // change the border color
                        primary: MyTheme.brandColor,
                        // change the text color
                        onSurface: Colors.black,
                      ),
                      // button colors
                      buttonTheme: ButtonThemeData(
                        colorScheme: ColorScheme.light(
                          primary: Colors.green,
                        ),
                      ),
                    ),
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: Container(
              width: getW(context),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: .5),
                  borderRadius: BorderRadius.all(Radius.circular(0))),
              child: Padding(
                padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  dLabel,
                  style: TextStyle(color: Colors.black, fontSize: 13),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  TableRow drawItems(LocationsModel model) {
    return TableRow(children: [
      Padding(
        padding: const EdgeInsets.all(5),
        child: Txt(
            txt: model.title ?? '',
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .6,
            txtAlign: TextAlign.center,
            isBold: false),
      ),
      Padding(
        padding: const EdgeInsets.all(5),
        child: Txt(
            txt: model.id.toString() ?? '0',
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .6,
            txtAlign: TextAlign.center,
            isBold: false),
      ),
      Padding(
        padding: const EdgeInsets.all(5),
        child: Txt(
            txt: IntroCfg().getStatusName(model.status),
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .6,
            txtAlign: TextAlign.center,
            isBold: false),
      ),
      GestureDetector(
        onTap: () {
          Get.to(
            () => WebScreen(
              title: model.title,
              caseID: model.id.toString(),
              url: CaseDetailsWebHelper()
                  .getLink(title: model.title, taskId: model.id),
            ),
            fullscreenDialog: true,
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Row(
            children: [
              Flexible(
                child: Txt(
                    txt: model.ownerName ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ),
              Icon(Icons.arrow_forward, color: MyTheme.brandColor, size: 15),
            ],
          ),
        ),
      ),
    ]);
  }

  getTableHeading(String txt) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(txt,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 15)),
    );
  }

  drawNF() {
    return Container(
      width: getW(context),
      //height: getH(context) / 2,
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        //shrinkWrap: true,
        children: [
          Container(
            width: getWP(context, 50),
            height: getWP(context, 30),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                    "assets/images/screens/db_cus/my_cases/case_nf2.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          //SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Container(
              child: Txt(
                txt: "Manage cases not found.\nTry with other options",
                txtColor: MyTheme.mycasesNFBtnColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
