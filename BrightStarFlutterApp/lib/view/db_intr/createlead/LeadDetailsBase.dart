import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/view/db_intr/createlead/leaddetails/edit_lead.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:pluto_menu_bar/pluto_menu_bar.dart';

import '../../../config/ServerIntr.dart';
import '../../../config/db_intro/intro_cfg.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../model/json/db_intr/createlead/UserNotesAPIModel.dart';
import '../../../model/json/misc/CommonAPIModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../../view_model/generic/enum_gen.dart';
import '../../widgets/dialog/ConfirmationDialog.dart';
import '../../widgets/txt/Txt.dart';

abstract class BaseLeadDetails<T extends StatefulWidget> extends State<T>
    with Mixin {
  getLeadbyResCompanyIdAPI();

  ResolutionModel resolutions;
  List<UserNotes> listUserNotes = [];
  //  list dropdown case owner
  DropListModel ddCaseOwner = DropListModel([]);
  OptionItem optCaseOwner = OptionItem(id: "0", title: "Select case owner");

  String selectedKey;
  int botIndex = 0;

  final botIconMapActivity = {
    'Phone': Icon(Icons.phone_in_talk_outlined, color: Colors.grey, size: 15),
    'Email': Icon(Icons.email_outlined, color: Colors.grey, size: 15),
    'Text': Icon(Icons.sms_outlined, color: Colors.grey, size: 15),
    'Face to Face':
        Icon(Icons.tag_faces_outlined, color: Colors.grey, size: 15),
    'Video Call': Icon(Icons.video_call_outlined, color: Colors.grey, size: 15),
  };

  final botIconMapNote = {
    'Phone': Icon(Icons.phone_in_talk_outlined, color: Colors.black, size: 20),
    'Email': Icon(Icons.email_outlined, color: Colors.black, size: 20),
    'Text': Icon(Icons.sms_outlined, color: Colors.black, size: 20),
    'Face to Face':
        Icon(Icons.tag_faces_outlined, color: Colors.black, size: 20),
    'Video Call':
        Icon(Icons.video_call_outlined, color: Colors.black, size: 20),
  };

  final topMenuItems = [
    {'Add': Icons.add},
    {'Edit': Icons.edit},
    {'Junk', Icons.recycling},
    {'Delete', Icons.remove}
  ];

  var listTopBtn = [
    "Added",
    "Qualifying",
    "Processing",
    "Converted",
    //"Junk Lead?"
  ];

  go2PostUserNote();
  doAdded();
  doQualifying();
  doProcessing();
  doConverted();
  doJunkLead();
  doJunk();

  message(context, s) {}

  drawTopbar(ResolutionModel resolutions) {
    return PlutoMenuBar(
      borderColor: Colors.transparent,
      menuIconColor: Colors.white,
      backgroundColor: MyTheme.brandColor,
      moreIconColor: Colors.white,
      menus: [
        /*MenuItem(
          title: 'More',
          icon: Icons.arrow_drop_down,
          children: [
            MenuItem(
              title: 'Add Note',
              icon: Icons.note_outlined,
              onTap: () => go2PostUserNote(),
            ),
            MenuItem(
              title: 'Add Email',
              icon: Icons.email_outlined,
              onTap: () => message(context, 'Menu 1-2 tap'),
            ),
            MenuItem(
              title: 'Add SMS',
              icon: Icons.sms_outlined,
              onTap: () => message(context, 'Menu 1-2 tap'),
            ),
          ],
        ),*/
        MenuItem(
          title: 'Add Note',
          icon: Icons.add,
          onTap: () => go2PostUserNote(),
        ),
        MenuItem(
          title: 'Edit',
          icon: Icons.edit_outlined,
          onTap: () => Get.to(() => EditLeadPage(
                resolutions: resolutions,
                ddCaseOwner: ddCaseOwner,
                optCaseOwner: optCaseOwner,
              )).then((value) async {
            if (value != null) {
              resolutions = value;
              await getLeadbyResCompanyIdAPI();
              setState(() {});
            }
          }),
        ),
        /*MenuItem(
          title: 'Junk',
          icon: Icons.recycling_outlined,
          onTap: () => Get.to(() => ChangeStage(
              eStage: eStageName.JunkLead,
              resolutions: resolutions)).then((res) {
            if (res != null) {
              resolutions = res;
              setState(() {});
            }
          }),
        ),*/
        MenuItem(
          title: 'Delete',
          icon: Icons.close_outlined,
          onTap: () => confirmDialog(
              context: context,
              callbackYes: () async {
                await APIViewModel().req<CommonAPIModel>(
                  context: context,
                  url: ServerIntr.DEL_RESOLUTION_URL
                      .replaceAll("#resId#", resolutions.id.toString()),
                  reqType: ReqType.Delete,
                  callback: (model) async {
                    if (mounted && model != null) {
                      if (model.success) {
                        showToast(
                            context: context,
                            msg: 'Lead has been removed successfully',
                            which: 1);
                        Future.delayed(
                            Duration(seconds: AppConfig.AlertDismisSec), () {
                          if (mounted) Get.back();
                        });
                      } else {
                        showToast(
                            context: context,
                            msg: 'Lead has been failed to remove',
                            which: 0);
                      }
                    }
                  },
                );
              },
              title: "Delete Lead",
              msg: "Are you sure you wish to delete this lead?"),
        ),
      ],
    );
  }

  drawCircleButtons(ResolutionModel resolutions) {
    if (resolutions.stage == EnumGen.getEnum2Str(eLeadStage.Junk)) {
      listTopBtn = ["Added", "Junk"];
    }
    return Padding(
      padding: const EdgeInsets.only(left: 1, right: 1, top: 10, bottom: 10),
      child: Container(
        width: getW(context),
        height: 30,
        child: Center(
          child: ListView.builder(
            itemCount: listTopBtn.length,
            scrollDirection: Axis.horizontal,
            primary: false,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final txt = listTopBtn[index];
              Color bgColor = Colors.grey.shade300;
              Color txtColor = Colors.black;
              Color circleColor = Colors.blueAccent;
              Color borderColor = Colors.orange;
              double border = 1;
              bool isUnderline = false;
              dynamic callback;

              if (txt == EnumGen.getEnum2Str(eLeadStage.Added)) {
                bgColor = resolutions.stage ==
                        EnumGen.getEnum2Str(eLeadStage.Added)
                    ? IntroCfg.stageColor[EnumGen.getEnum2Str(eLeadStage.Added)]
                    : Colors.blue;
                txtColor = Colors.white;
                circleColor = Colors.white;
                borderColor = Colors.transparent;
                border = 0;
                callback = () => doAdded();
              }

              if (txt == EnumGen.getEnum2Str(eLeadStage.Qualifying)) {
                bgColor = (resolutions.stage ==
                            EnumGen.getEnum2Str(eLeadStage.Qualifying) ||
                        resolutions.stage ==
                            EnumGen.getEnum2Str(eLeadStage.Processing) ||
                        resolutions.stage ==
                            EnumGen.getEnum2Str(eLeadStage.Converted))
                    ? IntroCfg
                        .stageColor[EnumGen.getEnum2Str(eLeadStage.Qualifying)]
                    : Colors.grey.shade200;
                txtColor = Colors.black;
                circleColor = Colors.white;
                borderColor = Colors.orange;
                border = 1;
                callback = () => doQualifying();
              }
              if (txt == EnumGen.getEnum2Str(eLeadStage.Processing)) {
                bgColor = (resolutions.stage ==
                            EnumGen.getEnum2Str(eLeadStage.Processing) ||
                        resolutions.stage ==
                            EnumGen.getEnum2Str(eLeadStage.Converted))
                    ? IntroCfg
                        .stageColor[EnumGen.getEnum2Str(eLeadStage.Processing)]
                    : Colors.grey[300];
                txtColor = Colors.black;
                circleColor = Colors.white;
                borderColor = Colors.yellow;
                border = 1;
                callback = () => doProcessing();
              }
              if (txt == EnumGen.getEnum2Str(eLeadStage.Converted)) {
                bgColor = resolutions.stage ==
                        EnumGen.getEnum2Str(eLeadStage.Converted)
                    ? IntroCfg
                        .stageColor[EnumGen.getEnum2Str(eLeadStage.Converted)]
                    : Colors.white70;
                txtColor = resolutions.stage ==
                        EnumGen.getEnum2Str(eLeadStage.Converted)
                    ? Colors.black
                    : Colors.black;
                circleColor = Colors.white;
                borderColor = Colors.greenAccent;
                border = 1;
                callback = () => doConverted();
              }
              /*if (txt == EnumGen.getEnum2Str(eLeadStage.Junk) + " Lead?") {
                bgColor = Colors.transparent;
                txtColor = Colors.black;
                circleColor = Colors.transparent;
                borderColor = Colors.transparent;
                border = 0;
                isUnderline = true;
                callback = () => doJunkLead();
              }*/
              if (txt == EnumGen.getEnum2Str(eLeadStage.Junk)) {
                bgColor = Colors.black;
                txtColor = Colors.white;
                circleColor = Colors.white;
                borderColor = Colors.transparent;
                border = 1;
                callback = () => doJunk();
              }
              return GestureDetector(
                onTap: () {
                  Function.apply(callback, []);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: bgColor,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: borderColor, width: border)),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 10, right: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        //color: bgColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(2),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(width: 5),
                            Container(
                              decoration: BoxDecoration(
                                border: (isUnderline)
                                    ? Border(
                                        bottom: BorderSide(
                                          color: Colors.black,
                                          width: 1,
                                        ),
                                      )
                                    : null,
                              ),
                              child: Txt(
                                  txt: txt,
                                  txtColor: txtColor,
                                  txtSize: MyTheme.txtSize -
                                      (!isUnderline ? 0.5 : 0.4),
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ),
                            circleColor != null
                                ? Row(
                                    children: [
                                      SizedBox(width: 5),
                                      drawCircle(
                                          context: context,
                                          color: circleColor,
                                          size: 4),
                                      SizedBox(width: 2),
                                    ],
                                  )
                                : SizedBox(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  drawTitleTxtRow(String title, String txt) {
    return Row(
      children: [
        title != null
            ? Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Txt(
                    txt: title,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .5,
                    txtAlign: TextAlign.start,
                    isBold: false),
              )
            : SizedBox(),
        Txt(
            txt: txt ?? '',
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .5,
            txtAlign: TextAlign.start,
            isBold: true)
      ],
    );
  }

  drawTitleTxtTable(String title, String txt) {
    return txt != null
        ? txt != '' && txt != AppDefine.CUR_SIGN
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  child: Table(children: [
                    TableRow(children: [
                      Txt(
                          txt: title ?? '',
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .5,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      Txt(
                          txt: txt ?? '',
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .5,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ]),
                  ]),
                ),
              )
            : SizedBox()
        : SizedBox();
  }

  drawAreYourCharging(String txt1, String txt2) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          txt1 != null
              ? Flexible(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: 'Are you charging a fee?',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .5,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        SizedBox(height: 5),
                        Txt(
                            txt: txt1 ?? '',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .5,
                            txtAlign: TextAlign.start,
                            isBold: false)
                      ],
                    ),
                  ),
                )
              : SizedBox(),
          SizedBox(width: 10),
          txt2 != null
              ? Flexible(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: 'Are you charging another fee?',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .5,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        SizedBox(height: 5),
                        Txt(
                            txt: txt2 ?? '',
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .5,
                            txtAlign: TextAlign.start,
                            isBold: false)
                      ],
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }

  //  ******************************************************  Bottom View

  drawContactActivityView(UserNotes userNoteModel) {
    final noteCreatedByName = userNoteModel.initiatorName;
    final dt = DateFun.getDate(userNoteModel.creationDate, "dd-MMM-yyyy HH:mm");
    final title = userNoteModel.title;
    final priority = userNoteModel.priority;
    final noteCategory = userNoteModel.noteCategory;
    final comments = userNoteModel.comments;
    var icon;
    try {
      if (checkHTML(comments)) {
        icon = botIconMapNote[userNoteModel.type] ??
            Icon(Icons.copy_outlined, color: Colors.black, size: 20);
      } else {
        icon = botIconMapActivity[userNoteModel.type] ??
            Icon(Icons.copy_outlined, color: Colors.black, size: 20);
      }
    } catch (e) {}
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: IntrinsicHeight(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  checkHTML(comments)
                      ? icon
                      : Icon(Icons.copy_rounded, color: Colors.black, size: 20),
                  SizedBox(height: 5),
                  Expanded(
                      child: Container(
                    color: Colors.black,
                    width: 1,
                  )),
                ],
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          !checkHTML(comments)
                              ? icon != null
                                  ? icon
                                  : SizedBox()
                              : SizedBox(),
                          SizedBox(width: 5),
                          Expanded(
                              flex: 4,
                              child: makeRichText4ContactActivity(
                                  noteCreatedByName)),
                          SizedBox(width: 10),
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: dt,
                                txtColor: Colors.grey.shade500,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.left,
                                isBold: false),
                          )
                        ],
                      ),
                      priority != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow("Priority:", priority),
                            )
                          : SizedBox(),
                      noteCategory != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow(
                                  "Note category:", noteCategory),
                            )
                          : SizedBox(),
                      title != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow(null, title),
                            )
                          : SizedBox(),
                      comments != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: checkHTML(comments)
                                  ? Html(
                                      data: comments,
                                      style: {
                                        "p": Style(
                                          color: Colors.black,
                                        ),
                                      },
                                    )
                                  : Txt(
                                      txt: comments,
                                      txtColor: Colors.black54,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.left,
                                      isBold: false),
                            )
                          : SizedBox(),
                    ]),
              ),
            ]),
      ),
    );
  }

  drawUserNoteView(UserNotes userNoteModel) {
    final noteCreatedByName = userNoteModel.initiatorName;
    final dt = DateFun.getDate(userNoteModel.creationDate, "dd-MMM-yyyy HH:mm");
    final title = userNoteModel.title;
    final priority = userNoteModel.priority;
    final noteCategory = userNoteModel.noteCategory;
    final comments = userNoteModel.comments;

    if (checkHTML(comments)) return SizedBox();

    var icon;
    try {
      icon = botIconMapNote[userNoteModel.type] ??
          Icon(Icons.copy_outlined, color: Colors.black, size: 20);
    } catch (e) {}
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: IntrinsicHeight(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  icon != null ? icon : SizedBox(),
                  SizedBox(height: 5),
                  Expanded(
                      child: Container(
                    color: Colors.black,
                    width: 1,
                  )),
                ],
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 4,
                              child: makeRichText4ContactActivity(
                                  noteCreatedByName)),
                          SizedBox(width: 10),
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: dt,
                                txtColor: Colors.grey.shade500,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.left,
                                isBold: false),
                          )
                        ],
                      ),
                      priority != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow("Priority:", priority),
                            )
                          : SizedBox(),
                      noteCategory != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow(
                                  "Note category:", noteCategory),
                            )
                          : SizedBox(),
                      title != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow(null, title),
                            )
                          : SizedBox(),
                      comments != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Txt(
                                  txt: comments,
                                  txtColor: Colors.black54,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.left,
                                  isBold: false),
                            )
                          : SizedBox(),
                    ]),
              ),
            ]),
      ),
    );
  }

  drawEmailView(UserNotes userNoteModel) {
    final comments = userNoteModel.comments;

    final noteCreatedByName = userNoteModel.initiatorName;
    final dt = DateFun.getDate(userNoteModel.creationDate, "dd-MMM-yyyy HH:mm");
    final title = userNoteModel.title;
    final priority = userNoteModel.priority;
    final noteCategory = userNoteModel.noteCategory;

    if (!checkHTML(comments)) return SizedBox();

    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: IntrinsicHeight(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.email_outlined, color: Colors.black, size: 20),
                  SizedBox(height: 5),
                  Expanded(
                      child: Container(
                    color: Colors.black,
                    width: 1,
                  )),
                ],
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 4,
                              child: makeRichText4ContactActivity(
                                  noteCreatedByName)),
                          SizedBox(width: 10),
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: dt,
                                txtColor: Colors.grey.shade500,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.left,
                                isBold: false),
                          )
                        ],
                      ),
                      priority != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow("Priority:", priority),
                            )
                          : SizedBox(),
                      noteCategory != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow(
                                  "Note category:", noteCategory),
                            )
                          : SizedBox(),
                      title != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: drawTitleTxtRow(null, title),
                            )
                          : SizedBox(),
                      comments != null
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: checkHTML(comments)
                                  ? Html(
                                      data: comments,
                                      style: {
                                        "p": Style(
                                          color: Colors.black,
                                        ),
                                      },
                                    )
                                  : Txt(
                                      txt: comments,
                                      txtColor: Colors.black54,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.left,
                                      isBold: false),
                            )
                          : SizedBox(),
                    ]),
              ),
            ]),
      ),
    );
  }

  drawSMSView(UserNotes userNoteModel) {
    final comments = userNoteModel.comments;

    final noteCreatedByName = userNoteModel.initiatorName;
    final dt = DateFun.getDate(userNoteModel.creationDate, "dd-MMM-yyyy HH:mm");
    final title = userNoteModel.title;
    final priority = userNoteModel.priority;
    final noteCategory = userNoteModel.noteCategory;

    //if (!checkHTML(comments)) return SizedBox();

    return SizedBox();

    /*return Padding(
      padding: const EdgeInsets.only(bottom:10),
      child:IntrinsicHeight(
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.email_outlined, color: Colors.black, size: 20),
                SizedBox(height: 5),
                Expanded(
                    child: Container(
                  color: Colors.black,
                  width: 1,
                )),
              ],
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                            flex: 4,
                            child: makeRichText4ContactActivity(
                                noteCreatedByName)),
                        SizedBox(width: 10),
                        Expanded(
                          flex: 2,
                          child: Txt(
                              txt: dt,
                              txtColor: Colors.grey.shade500,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.left,
                              isBold: false),
                        )
                      ],
                    ),
                    priority != null
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: drawTitleTxtRow("Priority:", priority),
                          )
                        : SizedBox(),
                    noteCategory != null
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child:
                                drawTitleTxtRow("Note category:", noteCategory),
                          )
                        : SizedBox(),
                    title != null
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: drawTitleTxtRow(null, title),
                          )
                        : SizedBox(),
                    comments != null
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: checkHTML(comments)
                                ? Html(
                                    data: comments,
                                    style: {
                                      "p": Style(
                                        color: Colors.black,
                                      ),
                                    },
                                  )
                                : Txt(
                                    txt: comments,
                                    txtColor: Colors.black54,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.left,
                                    isBold: false),
                          )
                        : SizedBox(),
                  ]),
            ),
          ]),
    ));*/
  }

  checkHTML(String str) {
    if (str != null) {
      if (str.contains("<") && str.contains(">")) {
        return true;
      }
    }
    return false;
  }

  makeRichText4ContactActivity(String noteCreatedByName) {
    return RichText(
      text: new TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: new TextStyle(
          fontSize: 13.5,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(
              text: "Note",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
          new TextSpan(
              text: ' created by ', style: new TextStyle(color: Colors.black)),
          new TextSpan(
              text: noteCreatedByName,
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }

  drawBotButton(
    int index,
    String label,
    IconData icon,
    Function callback,
  ) {
    return Flexible(
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              botIndex = index;
              callback();
            },
            child: Container(
              width: getWP(context, 10),
              height: getWP(context, 10),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: MyTheme.purpleColor),
                color: (botIndex == index) ? MyTheme.purpleColor : Colors.white,
              ),
              child: Icon(
                icon,
                color: (botIndex == index) ? Colors.white : Colors.black,
              ),
            ),
          ),
          SizedBox(height: 5),
          Txt(
              txt: label,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false),
        ],
      ),
    );
  }
}
