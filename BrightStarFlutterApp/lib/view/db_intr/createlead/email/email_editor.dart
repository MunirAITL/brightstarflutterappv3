import 'package:aitl/Mixin.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tuple/tuple.dart';
import '../../../../config/MyTheme.dart';
import '../../../../config/ServerIntr.dart';
import '../../../../controller/network/NetworkMgr.dart';
import '../../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../../model/json/db_intr/createlead/email/GetEmailSmsTemplatesAPIModel.dart';
import '../../../../view_model/api/api_view_model.dart';
import '../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../widgets/dropdown/DropListModel.dart';
import '../../../widgets/txt/Txt.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'dart:async';

class EmailEditorPage extends StatefulWidget {
  final ResolutionModel resolutions;
  const EmailEditorPage({Key key, @required this.resolutions})
      : super(key: key);

  @override
  State<EmailEditorPage> createState() => _EmailEditorPageState();
}

class _EmailEditorPageState extends State<EmailEditorPage> with Mixin {
  QuillController _controller = QuillController.basic();
  final scrollController = ScrollController();
  final FocusNode _focusNode = FocusNode();

  StreamSubscription<bool> keyboardSubscription;

  //  dropdown
  DropListModel ddTitle;
  OptionItem optTitle;

  final listSubject = [];

  bool isKBVisible = false;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    _controller.dispose();
    keyboardSubscription.cancel();
    scrollController.dispose();
    _focusNode.dispose();
    ddTitle = null;
    optTitle = null;
    super.dispose();
  }

  initPage() async {
    try {
      var keyboardVisibilityController = KeyboardVisibilityController();
      // Query
      print(
          'Keyboard visibility direct query: ${keyboardVisibilityController.isVisible}');

      // Subscribe
      keyboardSubscription =
          keyboardVisibilityController.onChange.listen((bool visible) {
        print('Keyboard visibility update. Is visible: $visible');
        if (WidgetsBinding.instance.window.viewInsets.bottom > 0.0) {
          if (mounted) {
            isKBVisible = visible;
            if (!isKBVisible) {
              scrollController
                  .jumpTo(scrollController.position.minScrollExtent);
              setState(() {});
            }
          }
        }
      });

      await APIViewModel().req<GetEmailSmsTemplatesAPIModel>(
        context: context,
        url: ServerIntr.GET_EMAILSMS_TEMPLATE_URL +
            "?UserCompanyId=" +
            userData.userModel.userCompanyID.toString() +
            "&Category=Email&Type=Lead",
        reqType: ReqType.Get,
        callback: (model) async {
          if (mounted && model != null) {
            if (model.success) {
              final list = <OptionItem>[];
              listSubject.clear();
              int i = 0;
              for (EmailAndSMSTemplates emailAndSMSTemplates
                  in model.responseData.emailAndSMSTemplates) {
                final opt = OptionItem(
                    id: i.toString(), title: emailAndSMSTemplates.title);
                if (i == 0) optTitle = opt;
                list.add(opt);
                listSubject.add(emailAndSMSTemplates.subject);
                i++;
              }
              ddTitle = DropListModel(list);
              setState(() {});
            }
          }
        },
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        //resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.titleColor,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          title: Txt(
              txt: "Send email to customer",
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            isKBVisible
                ? GestureDetector(
                    child: Txt(
                        txt: "Hide Keyboard",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize - .7,
                        txtAlign: TextAlign.center,
                        isBold: false),
                    onTap: () {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                      setState(() {});
                    })
                : SizedBox(),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor2,
          child: Padding(
            padding: EdgeInsets.only(
                left: getWP(context, 30),
                right: getWP(context, 30),
                top: 5,
                bottom: 5),
            child: MMBtn(
                txt: "Send Email",
                width: getW(context),
                height: getHP(context, 6),
                callback: () {}),
          ),
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      controller: scrollController,
      child: Column(
        children: [
          !isKBVisible
              ? Padding(
                  padding: const EdgeInsets.all(20), child: drawTopPanel())
              : SizedBox(),
          QuillToolbar.basic(
            controller: _controller,
            showAlignmentButtons: true,
            showSmallButton: true,
            iconTheme: QuillIconTheme(
                iconUnselectedFillColor: Colors.white,
                iconSelectedFillColor: Colors.black,
                iconUnselectedColor: Colors.black,
                iconSelectedColor: Colors.white,
                disabledIconFillColor: Colors.black,
                disabledIconColor: Colors.black),
            dialogTheme: QuillDialogTheme(
                inputTextStyle: TextStyle(color: Colors.black),
                labelTextStyle: TextStyle(color: Colors.black),
                dialogBackgroundColor: MyTheme.bgColor2),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Container(
                height: getHP(context, 40),
                color: Colors.white,
                child: QuillEditor(
                  controller: _controller,
                  scrollController: ScrollController(),
                  scrollable: true,
                  focusNode: _focusNode,
                  autoFocus: false,
                  readOnly: false,
                  placeholder: 'Your comments here',
                  expands: false,
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  customStyles: DefaultStyles(
                      h1: DefaultTextBlockStyle(
                          TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w300,
                              color: Colors.black),
                          Tuple2(16, 0),
                          Tuple2(0, 0),
                          null),
                      h2: DefaultTextBlockStyle(
                          TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w300,
                              color: Colors.black),
                          Tuple2(16, 0),
                          Tuple2(0, 0),
                          null),
                      h3: DefaultTextBlockStyle(
                          TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.w300,
                              color: Colors.black),
                          Tuple2(16, 0),
                          Tuple2(0, 0),
                          null),
                      paragraph: DefaultTextBlockStyle(
                          TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w300,
                              color: Colors.black),
                          Tuple2(16, 0),
                          Tuple2(0, 0),
                          null),
                      strikeThrough: TextStyle(
                          color: Colors.black,
                          decoration: TextDecoration.lineThrough),
                      sizeSmall: TextStyle(color: Colors.black),
                      italic: TextStyle(
                          color: Colors.black, fontStyle: FontStyle.italic),
                      bold: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                      underline: TextStyle(
                          color: Colors.black,
                          decoration: TextDecoration.underline),
                      color: Colors.black),
                )),
          ),
        ],
      ),
    );
  }

  getStyle(double fontSize) {
    return DefaultTextBlockStyle(
        TextStyle(
          fontSize: fontSize,
          color: Colors.black,
          height: 1,
          fontWeight: FontWeight.w300,
        ),
        Tuple2(fontSize, 0),
        const Tuple2(0, 0),
        null);
    // true for view only mode
  }

  drawTopPanel() {
    return listSubject.length > 0
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "Title",
                  txtColor: MyTheme.inputColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 5),
              DropDownListDialog(
                context: context,
                title: optTitle.title,
                ddTitleList: ddTitle,
                radius: 5,
                callback: (optionItem) {
                  optTitle = optionItem;
                  setState(() {});
                },
              ),
              SizedBox(height: 10),
              Txt(
                  txt: "Subject",
                  txtColor: MyTheme.inputColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 5),
              TxtBox(
                  txt: listSubject[int.parse(optTitle.id)],
                  height: 7,
                  radius: 5),
              SizedBox(height: 5),
            ],
          )
        : SizedBox();
  }
}
