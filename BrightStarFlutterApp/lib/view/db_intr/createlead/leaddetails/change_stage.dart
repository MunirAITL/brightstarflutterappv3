import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/db_intr/customer/CreateLeadProfile.dart';
import 'package:aitl/view/db_intr/intro_mixin.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../config/ServerIntr.dart';
import '../../../../controller/form_validator/UserProfileVal.dart';
import '../../../../controller/network/NetworkMgr.dart';
import '../../../../model/data/PrefMgr.dart';
import '../../../../model/json/db_intr/createlead/ChangeStageAPIModel.dart';
import '../../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../../view_model/api/api_view_model.dart';
import '../../../widgets/btn/MMBtn.dart';
import '../../../widgets/input/InputTitleBox.dart';
import '../../../widgets/txt/Txt.dart';

class ChangeStage extends StatefulWidget {
  final eStageName eStage;
  ResolutionModel resolutions;
  ChangeStage({Key key, @required this.eStage, @required this.resolutions})
      : super(key: key);

  @override
  State createState() => _ChangeStageState();
}

class _ChangeStageState extends State<ChangeStage> with Mixin, IntroMixin {
  final email = TextEditingController();
  final mobile = TextEditingController();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();

  int rbVal = 0;
  var listRadio = [
    {'index': 0, 'txt': 'Wrong Number', 'desc': ''},
    {'index': 1, 'txt': 'Unsolicited call', 'desc': ''},
    {'index': 2, 'txt': 'Not interested', 'desc': ''},
    {'index': 3, 'txt': 'Does Not Qualify', 'desc': ''},
    {
      'index': 4,
      'txt': 'Changed Circumstances',
      'desc':
          'Client information is assessed and established as potential future lead.'
    },
    {'index': 5, 'txt': 'No Longer Proceeding', 'desc': ''},
    {'index': 6, 'txt': 'Gone Elsewhere', 'desc': ''},
    {'index': 7, 'txt': 'Lost to competition', 'desc': ''},
    {'index': 8, 'txt': 'Do not contact again', 'desc': ''},
    {
      'index': 9,
      'txt': 'Remove from list',
      'desc': 'Client has requested to remove the name from the database.'
    },
    {
      'index': 10,
      'txt': 'Have not found property',
      'desc': 'I have called and customer requests to contact in the future.'
    },
    {
      'index': 11,
      'txt': 'Will wait for now',
      'desc':
          'Circumstances have been changed and the client wants to hold for the time being.'
    },
    {
      'index': 12,
      'txt': 'Convert to Customer',
      'desc': 'Client has decided to proceed.Converted the lead to a customer.'
    },
  ];

  //  dropdown
  DropListModel ddNoteType = DropListModel([
    OptionItem(id: "1", title: "Phone"),
    OptionItem(id: "2", title: "Email"),
    OptionItem(id: "2", title: "Text"),
    OptionItem(id: "3", title: "Face to Face"),
    OptionItem(id: "4", title: "Video Call"),
    OptionItem(id: "5", title: "Other"),
  ]);
  OptionItem optNoteType = OptionItem(id: "1", title: "Phone");

  final desc = TextEditingController();

  bool isUpdated = false;
  bool isEmailEmpty = false;
  bool isMobileEmpty = false;

  String countryCode = "+44";
  String countryName = "GB";

  putLeadStatusConversation() async {
    try {
      final resolution = widget.resolutions;
      final param = {
        "UserId": userData.userModel.id,
        "CreationDate": DateTime.now().toString(),
        "Status": 101,
        "Title": resolution.title ?? '',
        "Description": resolution.description ?? '',
        "Remarks": resolution.remarks ?? '',
        "EmailAddress": email.text.trim() ?? '',
        "PhoneNumber": mobile.text.trim() ?? '',
        "InitiatorId": userData.userModel.id,
        "ServiceDate": resolution.serviceDate ?? DateTime.now().toString(),
        "ResolutionType": resolution != null
            ? resolution.resolutionType
            : 'Lead From Introducer',
        "ParentId": resolution.parentId ?? 0,
        "ProfileImageUrl": resolution.profileImageUrl ?? '',
        "ProfileOwnerName": resolution.profileOwnerName ?? '',
        "Complainee": resolution.complainee ?? '',
        "ComplaineeUrl": resolution.complaineeUrl ?? '',
        "AssigneeId": resolution.assigneeId ?? 0,
        "AssigneeName": resolution.assigneeName ?? '',
        "AssignDate": resolution.assignDate ?? DateTime.now().toString(),
        "ResolvedDate": resolution.resolvedDate ?? DateTime.now().toString(),
        "TaskStatus": resolution.taskStatus ?? '',
        "UserComunity": resolution.userComunity ?? '',
        "ResolutionsUrl": resolution.resolutionsUrl ?? '',
        "UserCompanyId": userData.userModel.userCompanyID,
        "LeadStatus": listRadio[rbVal]['txt'],
        "LeadNote": desc.text.trim(),
        "FirstName": resolution.firstName ?? '',
        "MiddleName": resolution.middleName ?? '',
        "NamePrefix": resolution.namePrefix ?? '',
        "LastName": resolution.lastName ?? '',
        "InitiatorImageUrl": resolution.initiatorImageUrl ?? '',
        "InitiatorName": resolution.initiatorName ?? '',
        "InitiatorCompanyName": resolution.initiatorCompanyName ?? '',
        "Stage": Common.getEnum2Str(widget.eStage),
        "Probability": resolution.probability ?? 0,
        "CompanyName": resolution.companyName ?? '',
        "FileUrl": resolution.fileUrl ?? '',
        "LeadReferenceId": resolution.leadReferenceId ?? '',
        "DateOfBirth": resolution.dateOfBirth ?? '',
        "Address": resolution.address ?? '',
        "MortgageLengthLeft": resolution.mortgageLengthLeft ?? '',
        "HomeValue": resolution.homeValue ?? '',
        "MortgagePurpose": resolution.mortgagePurpose ?? '',
        "MortgageAmount": resolution.mortgageAmount ?? '',
        "MortgageBorrowLength": resolution.mortgageBorrowLength ?? '',
        "CreditRating": resolution.creditRating ?? '',
        "EmploymentStatus": resolution.employmentStatus ?? '',
        "YearlyIncome": resolution.yearlyIncome ?? '',
        "EmailConsent": resolution.emailConsent ?? 'No',
        "Qualifier": resolution.qualifier ?? '',
        "EstimatedEarning": resolution.estimatedEarning ?? 0,
        "ForwardedCompanyName": resolution.forwardedCompanyName ?? '',
        "CreatedByUserId": userData.userModel.id,
        "CreatedByUserName": userData.userModel.name,
        "IsLockAutoCall": resolution.isLockAutoCall ?? 0,
        "SupportAdminId": resolution.supportAdminId ?? 0,
        "UserNoteEntityName": resolution.userNoteEntityName ?? '',
        "UserNoteEntityId": resolution.userNoteEntityId ?? 0,
        "NegotiatorId": resolution.negotiatorId ?? 0,
        "NegotiatorName": resolution.negotiatorName ?? '',
        "GroupId": resolution.groupId ?? 0,
        "AreYouChargingAFee": resolution.areYouChargingAFee ?? 'No',
        "ChargeFeeAmount": resolution.chargeFeeAmount ?? 0,
        "ChargingFeeWhenPayable": resolution.chargingFeeWhenPayable ?? '',
        "ChargingFeeRefundable": resolution.chargingFeeRefundable ?? '',
        "AreYouChargingAnotherFee": resolution.chargeAnotherFeeAmount ?? 0,
        "ChargeAnotherFeeAmount": resolution.chargeAnotherFeeAmount ?? 0,
        "ChargingAnotherFeeWhenPayable":
            resolution.chargingAnotherFeeWhenPayable ?? '',
        "ChargingAnotherFeeRefundable":
            resolution.chargingAnotherFeeRefundable ?? 'No',
        "Id": resolution.id,
        "UpdatedStage": resolution.stage,
        "Note": desc.text.trim(),
        "CallBackOnDate": "",
        "CallBackOnTime": "",
        "Type": optNoteType.title,
        "NotePlaceHolder": ""
      };
      await APIViewModel().req<ChangeStageAPIModel>(
        context: context,
        url: ServerIntr.PUT_LEADSTATUSCONVERSION_URL,
        reqType: ReqType.Put,
        param: param,
        callback: (model) async {
          if (mounted && model != null) {
            if (model.success) {
              isUpdated = true;
              widget.resolutions = model.responseData.resolution;
              if (widget.eStage == eStageName.Converted) {
                Get.off(() => CreateNewCustomer(
                    resolutions: widget.resolutions, isEditCustomer: true));
              } else {
                showToast(
                    context: context,
                    msg: 'Status has been changed successfully',
                    which: 1);
                Future.delayed(Duration(seconds: 2), () {
                  if (mounted) Get.back(result: model.responseData.resolution);
                });
              }
            } else {
              showToast(
                  context: context,
                  msg: 'Failed to update lead status',
                  which: 0);
            }
          }
        },
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    try {
      if (widget.eStage == eStageName.Added) {
      } else if (widget.eStage == eStageName.Qualifying ||
          widget.eStage == eStageName.Junk_to_Qualifying) {
        listRadio = [
          {
            'index': 0,
            'txt': 'Call back Later',
            'desc':
                'Client was contacted and initial communication was established.'
          },
          {
            'index': 1,
            'txt': 'No Answer',
            'desc':
                'I have called the client and I was advised to call back another day.'
          },
        ];
      } else if (widget.eStage == eStageName.Processing) {
        listRadio = [
          {
            'index': 0,
            'txt': 'Call back Later',
            'desc':
                'Client was contacted and initial communication was established.'
          },
          {
            'index': 1,
            'txt': 'Needs Information',
            'desc': 'Assessing the client requirement.'
          },
          {
            'index': 2,
            'txt': 'Will wait for now',
            'desc':
                'Circumstances have been changed and the client wants to hold for the time being.'
          },
          {'index': 3, 'txt': 'Need to speak to advisor', 'desc': ''},
        ];
      } else if (widget.eStage == eStageName.Converted) {
        listRadio = [
          {
            'index': 0,
            'txt': 'Convert to Customer',
            'desc':
                'Client has decided to proceed.Converted the lead to a customer.'
          },
        ];
      }
      getMobCode();
      try {
        email.text = widget.resolutions.emailAddress ?? '';
        mobile.text = widget.resolutions.phoneNumber ?? '';
        if (email.text.isEmpty) {
          isEmailEmpty = true;
        }
        if (mobile.text.isEmpty) {
          isMobileEmpty = true;
        }
      } catch (e) {}
      final map = listRadio[0];
      desc.text = map['desc'];
      setState(() {});
    } catch (e) {}
  }

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.titleColor,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          leading: IconButton(
              onPressed: () {
                Get.back(result: isUpdated ? widget.resolutions : null);
              },
              icon: Icon(Icons.arrow_back)),
          title: Txt(
              txt: "Change Stage",
              txtColor: MyTheme.brandColor,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    var title = "";
    if (widget.eStage == eStageName.Added) {
    } else if (widget.eStage == eStageName.Qualifying) {
      title = "Changing the stage of this Lead from " +
          widget.resolutions.stage +
          " to Qualifying";
    } else if (widget.eStage == eStageName.Processing) {
      title = "Changing the stage of this Lead from " +
          widget.resolutions.stage +
          " to Processing";
    } else if (widget.eStage == eStageName.Converted) {
      title = "Changing the stage of this Lead from " +
          widget.resolutions.stage +
          " to Converted";
    } else if (widget.eStage == eStageName.JunkLead) {
      title = "Changing the stage of this Lead from " +
          widget.resolutions.stage +
          " to Junk";
    } else if (widget.eStage == eStageName.Junk_to_Qualifying) {
      title = "Changing the stage of this Lead from " +
          widget.resolutions.stage +
          " to Qualifying";
    }

    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Column(
            children: [
              Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Divider(color: Colors.grey, height: 10),
              SizedBox(height: 20),
              drawRadioBox(),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "Note Type",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  DropDownListDialog(
                    context: context,
                    title: optNoteType.title,
                    ddTitleList: ddNoteType,
                    callback: (optionItem) {
                      optNoteType = optionItem;
                      setState(() {});
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "Note",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextField(
                      controller: desc,
                      minLines: 3,
                      maxLines: 5,
                      //expands: true,
                      autocorrect: false,
                      maxLength: 500,
                      keyboardType: TextInputType.multiline,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize - .2),
                      ),
                      decoration: InputDecoration(
                        hintText: '',
                        hintStyle: TextStyle(color: Colors.grey),
                        //labelText: 'Your message',
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 11, top: 11, right: 15),
                      ),
                    ),
                  ),
                ],
              ),
              (widget.eStage == eStageName.Converted)
                  ? Column(
                      children: [
                        isEmailEmpty
                            ? Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: drawInputBox(
                                  context: context,
                                  title: "Email",
                                  input: email,
                                  ph: "",
                                  kbType: TextInputType.emailAddress,
                                  inputAction: mobile.text.isEmpty
                                      ? TextInputAction.next
                                      : TextInputAction.done,
                                  focusNode: focusEmail,
                                  focusNodeNext:
                                      mobile.text.isEmpty ? focusMobile : null,
                                  len: 50,
                                ))
                            : SizedBox(),
                        isMobileEmpty
                            ? Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: drawInputBox(
                                  context: context,
                                  title: "Phone Number",
                                  input: mobile,
                                  kbType: TextInputType.phone,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusMobile,
                                  len: 20,
                                ),
                                /*drawInputBoxWithCountryCode(
                                    context: context,
                                    title: "Mobile",
                                    input: mobile,
                                    ph: "xxxx xxx xxx",
                                    kbType: TextInputType.phone,
                                    inputAction: TextInputAction.done,
                                    focusNode: focusMobile,
                                    //focusNodeNext: focusPwd,
                                    len: 15,
                                    countryCode: countryCode,
                                    countryName: countryName,
                                    getCountryCode: (value) {
                                      countryCode = value.toString();
                                      print(
                                          "Country Code Clik = " + countryCode);
                                      PrefMgr.shared.setPrefStr(
                                          "countryName", value.code);
                                      PrefMgr.shared.setPrefStr(
                                          "countryCode", value.toString());
                                    })*/
                              )
                            : SizedBox(),
                      ],
                    )
                  : SizedBox(),
              SizedBox(height: 20),
              MMBtn(
                txt: "Submit",
                width: getW(context),
                height: getHP(context, 6),
                radius: 10,
                callback: () {
                  if (rbVal == -1) {
                    showToast(
                        context: context, msg: "The Lead Status is required");
                    return;
                  } else if (optNoteType.id == null) {
                    showToast(context: context, msg: "Select Note Type");
                    return;
                  } else if (widget.eStage == eStageName.Converted &&
                      !UserProfileVal().isEmailOK(context, email,
                          "The Email Address is required for converting Lead to customer.")) {
                    return;
                  } else if (!UserProfileVal().isPhoneOK(context, mobile)) {
                    return;
                  } else if (desc.text.trim().length == 0) {
                    showToast(context: context, msg: "The Note is required");
                    return;
                  }
                  putLeadStatusConversation();
                  /*else if (optPriority.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select priority");
                    return;
                  } else if (optNoteCat.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select note category");
                    return;
                  } else if (optNoteType.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select note type");
                    return;
                  } else if (desc.text.trim().length == 0) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please enter description");
                    return;
                  }*/
                },
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawRadioBox() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
              txt: "Lead Status",
              txtColor: MyTheme.inputColor,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.center,
              isBold: false),
          for (int i = 0; i < listRadio.length; i++)
            (i % 2 == 0)
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(child: drawRadio(map: listRadio[i++])),
                        SizedBox(width: 10),
                        (i < listRadio.length)
                            ? Expanded(child: drawRadio(map: listRadio[i]))
                            : SizedBox(width: getWP(context, 43)),
                      ],
                    ),
                  )
                : SizedBox(),
        ],
      ),
    );
  }

  drawRadio({Map<String, Object> map}) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
          rbVal = map['index'];
          desc.text = map['desc'];
          setState(() {});
        },
        child: radioButtonitem(
            context: context,
            text: map['txt'],
            txtSize: 1.7,
            bgColor: rbVal == map['index'] ? '#252551' : '#FFF',
            textColor: rbVal == map['index'] ? Colors.white : Colors.black));
  }
}
