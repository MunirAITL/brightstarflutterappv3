import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import '../../../../config/ServerIntr.dart';
import '../../../../controller/network/NetworkMgr.dart';
import '../../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../../model/json/db_intr/createlead/UserNotesAPIModel.dart';
import '../../../../model/json/db_intr/createlead/usernote/PostCaseNoteAPIModel.dart';
import '../../../../view_model/api/api_view_model.dart';
import '../../../widgets/btn/MMBtn.dart';
import '../../../widgets/input/InputTitleBox.dart';
import '../../../widgets/txt/Txt.dart';
import 'package:get/get.dart';

class LeadDetailsAddNote extends StatefulWidget {
  final ResolutionModel resolutions;
  const LeadDetailsAddNote({Key key, @required this.resolutions})
      : super(key: key);

  @override
  State createState() => _LeadDetailsAddNoteState();
}

class _LeadDetailsAddNoteState extends State<LeadDetailsAddNote> with Mixin {
  final title = TextEditingController();
  final desc = TextEditingController();
  final other = TextEditingController();
  //
  final focusTitle = FocusNode();
  final focusOther = FocusNode();
  final focusDesc = FocusNode();

  //  dropdown
  DropListModel ddPriority = DropListModel([
    OptionItem(id: "1", title: "High"),
    OptionItem(id: "2", title: "Medium"),
    OptionItem(id: "3", title: "Low"),
  ]);
  OptionItem optPriority = OptionItem(id: null, title: "Select");

  //  dropdown
  DropListModel ddNoteCat = DropListModel([
    OptionItem(id: "1", title: "General"),
    OptionItem(id: "2", title: "Complain"),
    OptionItem(id: "3", title: "Other"),
  ]);
  OptionItem optNoteCat = OptionItem(id: null, title: "Select");

  //  dropdown
  DropListModel ddNoteType = DropListModel([
    OptionItem(id: "1", title: "Phone"),
    OptionItem(id: "2", title: "Email"),
    OptionItem(id: "3", title: "Text"),
    OptionItem(id: "4", title: "Face to Face"),
    OptionItem(id: "5", title: "Video Call"),
    OptionItem(id: "6", title: "Other"),
  ]);
  OptionItem optNoteType = OptionItem(id: null, title: "Select Note Type");

  bool isOtherNoteCat = false;

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: MyTheme.titleColor,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          title: Txt(
              txt: "Lead Note",
              txtColor: MyTheme.brandColor,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              drawInputBox(
                context: context,
                title: "Title",
                input: title,
                ph: "",
                kbType: TextInputType.name,
                inputAction: TextInputAction.next,
                focusNode: focusTitle,
                focusNodeNext: !isOtherNoteCat ? focusDesc : focusOther,
                len: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10),
                  Txt(
                      txt: "Priority",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  DropDownListDialog(
                    context: context,
                    title: optPriority.title,
                    ddTitleList: ddPriority,
                    callback: (optionItem) {
                      optPriority = optionItem;
                      setState(() {});
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "Note Category",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  DropDownListDialog(
                    context: context,
                    title: optNoteCat.title,
                    ddTitleList: ddNoteCat,
                    callback: (optionItem) {
                      optNoteCat = optionItem;
                      isOtherNoteCat = false;
                      if (optNoteCat.title == "Other") {
                        isOtherNoteCat = true;
                      }
                      setState(() {});
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
              isOtherNoteCat
                  ? drawInputBox(
                      context: context,
                      title: "Other",
                      input: other,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusOther,
                      focusNodeNext: focusDesc,
                      len: 20,
                    )
                  : SizedBox(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "Note Type",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  DropDownListDialog(
                    context: context,
                    title: optNoteType.title,
                    ddTitleList: ddNoteType,
                    callback: (optionItem) {
                      optNoteType = optionItem;
                      setState(() {});
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "Description",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextField(
                      controller: desc,
                      focusNode: focusDesc,
                      textInputAction: TextInputAction.done,
                      minLines: 2,
                      maxLines: 4,
                      //expands: true,
                      autocorrect: false,
                      maxLength: 500,
                      keyboardType: TextInputType.multiline,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                      ),
                      decoration: InputDecoration(
                        hintText: 'Description',
                        hintStyle: TextStyle(color: Colors.grey),
                        //labelText: 'Your message',
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 11, top: 11, right: 15),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              MMBtn(
                txt: "Submit",
                width: getW(context),
                height: getHP(context, 6),
                radius: 10,
                callback: () async {
                  if (title.text.trim().length == 0) {
                    showToast(context: context, msg: "Missing title");
                    return;
                  }
                  try {
                    final now = DateTime.now().toString();
                    final param = {
                      "CreationDate": now,
                      "InitiatorId": 0,
                      "UserId": userData.userModel.id,
                      "Title": title.text.trim(),
                      "Comments": desc.text.trim(),
                      "ServiceDate": now,
                      "IsTag": 0,
                      "UserCompanyId": userData.userModel.userCompanyID,
                      "Type": optNoteType.title ?? '',
                      "LeadId": widget.resolutions.id,
                      "Priority": optPriority.title ?? '',
                      'NoteCategory': optNoteCat.title ?? '',
                      "Category": "LeadNote",
                      "OtherCategory": "",
                      "ComplainMode": "",
                      "ServiceTime": "09:00 am",
                      "ServiceEndDate": now,
                      "ServiceEndTime": "09:30 am",
                      "AdviserId": 0,
                      "Status": 101,
                      "EntityId": widget.resolutions.id,
                      "EntityName": "Lead"
                    };
                    await APIViewModel().req<PostCaseNoteAPIModel>(
                        context: context,
                        url: ServerIntr.POST_USERNOTE_URL,
                        reqType: ReqType.Post,
                        param: param,
                        callback: (model) async {
                          if (mounted && model != null) {
                            if (model.success) {
                              showToast(
                                  context: context,
                                  msg: "Lead note posted successfully",
                                  which: 1);
                              Future.delayed(
                                  Duration(
                                      seconds: AppConfig.AlertDismisSec - 1),
                                  () {
                                if (mounted) Get.back();
                              });
                            }
                          }
                        });
                  } catch (e) {}

                  /*else if (optPriority.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select priority");
                    return;
                  } else if (optNoteCat.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select note category");
                    return;
                  } else if (optNoteType.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select note type");
                    return;
                  } else if (desc.text.trim().length == 0) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please enter description");
                    return;
                  }*/
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
