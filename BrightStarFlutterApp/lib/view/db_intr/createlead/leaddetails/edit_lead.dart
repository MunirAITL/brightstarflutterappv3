import 'dart:convert';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/observer/StateListnerIntro.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/view/db_intr/intro_mixin.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import '../../../../config/AppDefine.dart';
import '../../../../config/db_cus/NewCaseCfg.dart';
import '../../../../config/db_intro/intro_cfg.dart';
import '../../../../controller/api/db_intr/case_lead/CaseLeadNavigatorAPIMgr.dart';
import '../../../../controller/form_validator/UserProfileVal.dart';
import '../../../../controller/helper/db_intr/case_lead/CaseLeadNegotiatorHelper.dart';
import '../../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../widgets/btn/MMBtn.dart';
import '../../../widgets/gplaces/GPlacesView.dart';
import '../../../widgets/input/InputTitleBox.dart';
import '../../../widgets/input/InputTitleBoxfWithCountryCode.dart';
import '../../../widgets/input/drawInputCurrencyBox.dart';
import '../../../widgets/input/utils/DecimalTextInputFormatter.dart';
import '../../../widgets/txt/IcoTxtIco.dart';
import '../../../widgets/txt/SignText.dart';
import '../../../widgets/txt/Txt.dart';
import 'package:get/get.dart';

class EditLeadPage extends StatefulWidget {
  final ResolutionModel resolutions;
  final DropListModel ddCaseOwner;
  OptionItem optCaseOwner;
  EditLeadPage({Key key, this.resolutions, this.ddCaseOwner, this.optCaseOwner})
      : super(key: key);
  @override
  State createState() => _EditLeadPageState();
}

class _EditLeadPageState extends State<EditLeadPage> with Mixin, IntroMixin {
  bool isChargingFee = false;
  bool isChargingFeeRefundable = false;

  bool isChargingAnotherFee = false;
  bool isChargingAnotherFeeRefundable = false;

  DropListModel ddChargingFee = DropListModel([
    OptionItem(id: "1", title: "Application"),
    OptionItem(id: "2", title: "Offer"),
    OptionItem(id: "3", title: "Completion"),
  ]);
  OptionItem optChargingFee = OptionItem(id: null, title: "Select");

  DropListModel ddChargingAnotherFee = DropListModel([
    OptionItem(id: "1", title: "Application"),
    OptionItem(id: "2", title: "Offer"),
    OptionItem(id: "3", title: "Completion"),
  ]);
  OptionItem optChargingAnotherFee = OptionItem(id: null, title: "Select");

  //DropListModel ddLeadIntr = DropListModel([]);
  //OptionItem optLeadIntr = OptionItem(id: null, title: "Select Initiator");

  DropListModel ddEmpStatus = DropListModel([
    OptionItem(id: "1", title: "Employed"),
    OptionItem(id: "2", title: "Self-employed"),
    OptionItem(id: "3", title: "Contractor"),
    OptionItem(id: "4", title: "Unemployed"),
    OptionItem(id: "5", title: "Retired"),
  ]);
  OptionItem optEmpStatus = OptionItem(id: null, title: "Select");

  //  DOB
  String dob = "";
  String addr = "";
  double ltv = 0;

  bool isddCaseSubTypeTextField = false;

  final title = TextEditingController();
  final fname = TextEditingController();
  final lname = TextEditingController();
  final email = TextEditingController();
  final mobile = TextEditingController();
  final desc = TextEditingController();

  final mortgageTerms = TextEditingController();
  final valuationAmount = TextEditingController();
  final depositAmount = TextEditingController();
  final annualIncome = TextEditingController();
  final loanAmount = TextEditingController();

  //
  final focusTitle = FocusNode();
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusDesc = FocusNode();

  final focusMortgageTerms = FocusNode();
  final focusValuationAmount = FocusNode();
  final focusDepositAmount = FocusNode();
  final focusAnnualIncome = FocusNode();
  final focusLoanAmount = FocusNode();

  //
  final chargingFee = TextEditingController();
  final chargingAnotherFee = TextEditingController();
  final estEarning = TextEditingController();

  //
  final focusChargingFee = FocusNode();
  final focusChargingAnotherFee = FocusNode();
  final focusEstEarning = FocusNode();
  final focusLeadRef = FocusNode();

  //  lead refid
  int caseIndex = -1;

  final mortgageLenLeft = TextEditingController();

  //
  final focusMortgageLenLeft = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  //  dropdown
  DropListModel ddCaseType;
  OptionItem optCaseType;

  DropListModel ddCaseSubType;
  OptionItem optCaseSubType = OptionItem(id: null, title: "Select");

  //  dropdown
  //DropListModel dd = DropListModel([]);
  //OptionItem opt = OptionItem(id: null, title: "Select Negotiator");

  calculation() {
    double ltvAmount = 0;
    try {
      final depositAmt = depositAmount.text.trim();
      final loanAmt = loanAmount.text.trim();
      final valuationAmt = valuationAmount.text.trim();
      double depoistVal = (depositAmt != '') ? double.parse(depositAmt) : 0;
      double loanVal = (loanAmt != '') ? double.parse(loanAmt) : 0;
      double valuationVal =
          (valuationAmt != '') ? double.parse(valuationAmt) : 0;

      ltvAmount = (loanVal / valuationVal) * 100;

      /*if ($scope.mortgageUserRequirementModel.BalanceOutstanding != null &&
          $scope.mortgageUserRequirementModel.BalanceOutstanding > 0) {
        var LTVAmount = parseFloat((parseInt(
                    $scope.mortgageUserRequirementModel.BalanceOutstanding) /
                parseInt($scope.mortgageUserRequirementModel
                    .PriceOfPropertyBeingPurchasedCurrentValuationOfProperty)) *
            100);
        $scope.mortgageUserRequirementModel.LTVAmount = LTVAmount;
      }*/
    } catch (e) {
      ltvAmount = 0;
    }
    ltv = (!ltvAmount.isNaN) ? ltvAmount : 0;
    setState(() {});
  }

  validate() {
    if (optCaseType.id == null) {
      showToast(
          context: context,
          msg: "Please select 'Lead Type' from the above options");
      return false;
    }
    if (optCaseType.title != 'Others') {
      if (ddCaseSubType.listOptionItems.length > 0 &&
          optCaseSubType.id == null) {
        showToast(context: context, msg: "Please choose sub category");
        return false;
      }
    } else {
      if (UserProfileVal().isEmpty(context, title, "Please enter lead title")) {
        return false;
      }
    }
    if (!UserProfileVal().isFNameOK(context, fname)) {
      return false;
    }
    if (!UserProfileVal().isLNameOK(context, lname)) {
      return false;
    }
    if (!UserProfileVal().isEmailOK(context, email, "Invalid email address")) {
      return false;
    }
    if (!UserProfileVal().isPhoneOK(context, mobile)) {
      return false;
    }
    if (mortgageTerms.text.trim().isEmpty) {
      focusMortgageTerms.requestFocus();
      showToast(
          context: context, msg: "The Mortgage Borrow Length is required");
      return false;
    }
    if (optEmpStatus.id == null) {
      showToast(context: context, msg: "The employment status is required");
      return false;
    }
    if (annualIncome.text.trim().isEmpty) {
      focusAnnualIncome.requestFocus();
      showToast(context: context, msg: "The yearly income is required");
      return false;
    }
    if (valuationAmount.text.trim().isEmpty) {
      focusValuationAmount.requestFocus();
      showToast(context: context, msg: "The Home Value is required");
      return false;
    }
    if (loanAmount.text.trim().isEmpty || loanAmount.text.trim() == "0") {
      focusLoanAmount.requestFocus();
      showToast(context: context, msg: "The mortgage amount is required");
      return false;
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
      calculation();
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }

    try {
      optCaseType = OptionItem(id: null, title: "I'm looking for...");
      List<OptionItem> lst = [];
      for (var map in NewCaseCfg.listCreateNewCase) {
        if (map['title'] == widget.resolutions.title) {
          caseIndex = map['index'];
          optCaseType =
              OptionItem(id: map['index'].toString(), title: map['title']);
        }
        lst.add(OptionItem(id: map["index"].toString(), title: map['title']));
      }
      ddCaseType = DropListModel(lst);
      lst = null;
      if (caseIndex != -1)
        reloadCaseSubType(caseIndex, true);
      else {
        final map = NewCaseCfg
            .listCreateNewCase[NewCaseCfg.listCreateNewCase.length - 1];
        optCaseType =
            OptionItem(id: map['index'].toString(), title: map['title']);
        title.text = widget.resolutions.title;
      }
    } catch (e) {}
    try {
      fname.text = widget.resolutions.firstName ?? '';
      lname.text = widget.resolutions.lastName ?? '';
      email.text = widget.resolutions.emailAddress ?? '';
      mobile.text = widget.resolutions.phoneNumber ?? '';
      addr = widget.resolutions.address ?? '';
      desc.text = widget.resolutions.leadNote ?? '';
    } catch (e) {}
    try {
      widget.optCaseOwner = OptionItem(
          id: widget.resolutions.assigneeId.toString(),
          title: widget.resolutions.assigneeName);
    } catch (e) {}
    try {
      estEarning.text = widget.resolutions.estimatedEarning.toStringAsFixed(2);
    } catch (e) {}
    try {
      dob = widget.resolutions.dateOfBirth ?? '';
    } catch (e) {}
    try {
      isChargingFee =
          widget.resolutions.areYouChargingAFee == "Yes" ? true : false;
    } catch (e) {}
    try {
      chargingFee.text = widget.resolutions.chargeFeeAmount.toStringAsFixed(2);
    } catch (e) {}
    try {
      optChargingFee =
          OptionItem(id: "0", title: widget.resolutions.chargingFeeWhenPayable);
    } catch (e) {}
    try {
      isChargingFeeRefundable =
          widget.resolutions.chargingFeeRefundable == "Yes" ? true : false;
    } catch (e) {}
    try {
      isChargingAnotherFee =
          widget.resolutions.areYouChargingAnotherFee == "Yes" ? true : false;
    } catch (e) {}
    try {
      chargingAnotherFee.text =
          widget.resolutions.chargeAnotherFeeAmount.toStringAsFixed(2);
    } catch (e) {}
    try {
      optChargingAnotherFee = OptionItem(
          id: "0", title: widget.resolutions.chargingAnotherFeeWhenPayable);
    } catch (e) {}
    try {
      isChargingAnotherFeeRefundable =
          widget.resolutions.chargingAnotherFeeRefundable == "Yes"
              ? true
              : false;
    } catch (e) {}
    try {
      mortgageTerms.text = widget.resolutions.mortgageBorrowLength ?? '';
    } catch (e) {}
    try {
      mortgageLenLeft.text = widget.resolutions.mortgageLengthLeft ?? '';
    } catch (e) {}
    try {
      optEmpStatus =
          OptionItem(id: "0", title: widget.resolutions.employmentStatus ?? '');
    } catch (e) {}
    try {
      annualIncome.text = widget.resolutions.yearlyIncome ?? "0";
    } catch (e) {}
    try {
      valuationAmount.text = widget.resolutions.homeValue ?? "0";
    } catch (e) {}
    try {
      depositAmount.text = widget.resolutions.mortgagePurpose ?? "0";
    } catch (e) {}
    try {
      loanAmount.text = widget.resolutions.mortgageAmount ?? "0";
    } catch (e) {}
    try {
      ltv = double.parse(widget.resolutions.creditRating) ?? 0;
    } catch (e) {}

    setState(() {});
  }

  reloadCaseSubType(int index, bool isInit) {
    for (var map in NewCaseCfg.listCreateNewCase) {
      if (map['index'] == index) {
        List subList = map['subItem'];
        if (subList.length > 0) {
          if (isInit) {
            title.text = widget.resolutions.description;
            optCaseSubType =
                OptionItem(id: "0", title: widget.resolutions.description);
          } else {
            title.clear();
            optCaseSubType = OptionItem(id: null, title: "Select");
          }
          List<OptionItem> lst = [];
          for (final subMap in subList) {
            lst.add(OptionItem(
                id: subMap['index'].toString(), title: subMap['title']));
          }
          ddCaseSubType = DropListModel(lst);
          isddCaseSubTypeTextField = false;
          lst = null;
        } else {
          if (map['title'] == 'Others')
            isddCaseSubTypeTextField = true;
          else
            isddCaseSubTypeTextField = false;
          ddCaseSubType = DropListModel([]);
        }
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 1,
          backgroundColor: MyTheme.titleColor,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          title: Txt(
              txt: "Edit Lead",
              txtColor: MyTheme.brandColor,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return (ddCaseType != null)
        ? Container(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Case Type",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 10),
                        DropDownListDialog(
                          context: context,
                          title: optCaseType.title,
                          ddTitleList: ddCaseType,
                          callback: (optionItem) {
                            caseIndex = int.parse(optionItem.id);
                            optCaseType = optionItem;
                            optCaseSubType =
                                OptionItem(id: null, title: "Select");
                            reloadCaseSubType(int.parse(optCaseType.id), false);
                            title.clear();
                            setState(() {});
                          },
                        ),
                      ],
                    ),
                    caseIndex != -1
                        ? (ddCaseSubType.listOptionItems.length > 0 &&
                                optCaseType.id != null)
                            ? Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Txt(
                                        txt: "Sub category",
                                        txtColor: MyTheme.inputColor,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                    SizedBox(height: 10),
                                    DropDownListDialog(
                                      context: context,
                                      title: optCaseSubType.title,
                                      ddTitleList: ddCaseSubType,
                                      callback: (optionItem) {
                                        optCaseSubType = optionItem;
                                        title.text = optCaseSubType.title;
                                        setState(() {});
                                      },
                                    ),
                                  ],
                                ),
                              )
                            : (isddCaseSubTypeTextField)
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 20),
                                    child: drawInputBox(
                                      context: context,
                                      title: "Title",
                                      input: title,
                                      ph: "Please specify",
                                      kbType: TextInputType.text,
                                      inputAction: TextInputAction.next,
                                      focusNode: focusTitle,
                                      focusNodeNext: focusFname,
                                      len: 50,
                                    ))
                                : SizedBox()
                        : Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: drawInputBox(
                              context: context,
                              title: "Title",
                              input: title,
                              ph: "Please specify",
                              kbType: TextInputType.text,
                              inputAction: TextInputAction.next,
                              focusNode: focusTitle,
                              focusNodeNext: focusFname,
                              len: 50,
                            )),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Are you charging a fee?",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingFee = true;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "Yes",
                                          txtSize: 1.7,
                                          bgColor: isChargingFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: isChargingFee
                                              ? Colors.white
                                              : Colors.black))),
                              SizedBox(width: 10),
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingFee = false;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "No",
                                          txtSize: 1.7,
                                          bgColor: !isChargingFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: !isChargingFee
                                              ? Colors.white
                                              : Colors.black))),
                            ],
                          ),
                        ),
                      ],
                    ),
                    drawChargingFeeBox(
                        chargingFee,
                        focusChargingFee,
                        ddChargingFee,
                        optChargingFee,
                        isChargingFee,
                        isChargingFeeRefundable, (opt) {
                      optChargingFee.title = opt.title;
                      setState(() {});
                    }, (isRef) {
                      isChargingFeeRefundable = isRef;
                      setState(() {});
                    }),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Are you charging another fee?",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingAnotherFee = true;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "Yes",
                                          txtSize: 1.7,
                                          bgColor: isChargingAnotherFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: isChargingAnotherFee
                                              ? Colors.white
                                              : Colors.black))),
                              SizedBox(width: 10),
                              Flexible(
                                  child: GestureDetector(
                                      onTap: () {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        isChargingAnotherFee = false;
                                        setState(() {});
                                      },
                                      child: radioButtonitem(
                                          context: context,
                                          text: "No",
                                          txtSize: 1.7,
                                          bgColor: !isChargingAnotherFee
                                              ? '#252551'
                                              : '#FFF',
                                          textColor: !isChargingAnotherFee
                                              ? Colors.white
                                              : Colors.black))),
                            ],
                          ),
                        ),
                      ],
                    ),
                    drawChargingFeeBox(
                        chargingAnotherFee,
                        focusChargingAnotherFee,
                        ddChargingAnotherFee,
                        optChargingAnotherFee,
                        isChargingAnotherFee,
                        isChargingAnotherFeeRefundable, (opt) {
                      optChargingAnotherFee.title = opt.title;
                      setState(() {});
                    }, (isRef) {
                      isChargingAnotherFeeRefundable = isRef;
                      setState(() {});
                    }),
                    SizedBox(height: 20),
                    Divider(color: Colors.black),
                    SizedBox(height: 10),
                    drawInputBox(
                      context: context,
                      title: "First Name",
                      input: fname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusFname,
                      focusNodeNext: focusLname,
                      len: 20,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Last Name",
                      input: lname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusLname,
                      focusNodeNext: focusEmail,
                      len: 20,
                    ),
                    /*SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Txt(
                            txt: "Date of birth",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            isBold: false,
                            txtAlign: TextAlign.start,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            showDatePicker(
                              context: context,
                              initialDate: dateDOBlast,
                              firstDate: dateDOBfirst,
                              lastDate: dateDOBlast,
                              fieldHintText: "dd/mm/yyyy",
                              builder: (context, child) {
                                return Theme(
                                  data: ThemeData.light().copyWith(
                                    colorScheme: ColorScheme.light(
                                        primary: MyTheme.brandColor),
                                    buttonTheme: ButtonThemeData(
                                        textTheme: ButtonTextTheme.primary),
                                  ), // This will change to light theme.
                                  child: child,
                                );
                              },
                            ).then((value) {
                              if (value != null) {
                                dob = DateFormat('dd-MM-yyyy')
                                    .format(value)
                                    .toString();
                                setState(() {});
                              }
                            });
                          },
                          child: IcoTxtIco(
                            leftIcon: Icons.calendar_today,
                            txt: dob == "" ? 'Select date of birth' : dob,
                            txtSize: MyTheme.txtSize,
                            txtColor: Colors.black,
                            rightIcon: Icons.keyboard_arrow_down,
                            txtAlign: TextAlign.left,
                            rightIconSize: 30,
                            leftIconSize: 20,
                          ),
                        ),
                      ],
                    ),*/
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Email",
                      input: email,
                      ph: "",
                      kbType: TextInputType.emailAddress,
                      inputAction: TextInputAction.next,
                      focusNode: focusEmail,
                      focusNodeNext: focusMobile,
                      len: 50,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Phone Number",
                      input: mobile,
                      kbType: TextInputType.phone,
                      inputAction: TextInputAction.next,
                      focusNode: focusMobile,
                      focusNodeNext: focusMortgageTerms,
                      len: 20,
                    ),
                    /*drawInputBoxWithCountryCode(
                        context: context,
                        title: "Phone Number",
                        input: mobile,
                        ph: "xxxx xxx xxx",
                        kbType: TextInputType.phone,
                        inputAction: TextInputAction.next,
                        focusNode: focusMobile,
                        focusNodeNext: focusMortgageTerms,
                        len: 15,
                        countryCode: countryCode,
                        countryName: countryName,
                        getCountryCode: (value) {
                          countryCode = value.toString();
                          print("Country Code Clik = " + countryCode);
                          PrefMgr.shared.setPrefStr("countryName", value.code);
                          PrefMgr.shared
                              .setPrefStr("countryCode", value.toString());
                        }),*/
                    widget.ddCaseOwner != null
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: "Choose your consultant",
                                    txtColor: MyTheme.inputColor,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                SizedBox(height: 10),
                                DropDownListDialog(
                                  radius: 5,
                                  context: context,
                                  title: widget.optCaseOwner.title,
                                  //id: widget.optCaseOwner.id,
                                  ddTitleList: widget.ddCaseOwner,
                                  callback: (optionItem) {
                                    widget.optCaseOwner = optionItem;
                                    setState(() {});
                                  },
                                ),
                              ],
                            ),
                          )
                        : SizedBox(),
                    SizedBox(height: 20),
                    GPlacesView(
                        title: "Address",
                        address: addr,
                        callback: (String address, Location loc) {
                          addr = address;
                          setState(() {});
                        }),
                    SizedBox(height: 10),
                    Divider(color: Colors.black),
                    SizedBox(height: 10),
                    drawInputBox(
                      context: context,
                      title: "Mortgage term (months)",
                      input: mortgageTerms,
                      ph: "",
                      kbType: TextInputType.text,
                      inputAction: TextInputAction.next,
                      focusNode: focusMortgageTerms,
                      focusNodeNext: focusMortgageLenLeft,
                      len: 50,
                    ),
                    caseIndex == 1 || caseIndex == 4
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: drawInputBox(
                              context: context,
                              title: "Mortgage length left",
                              input: mortgageLenLeft,
                              ph: "",
                              kbType: TextInputType.text,
                              inputAction: TextInputAction.next,
                              focusNode: focusMortgageLenLeft,
                              focusNodeNext: focusAnnualIncome,
                              len: 50,
                            ))
                        : SizedBox(),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "Employment status",
                              txtColor: MyTheme.inputColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 10),
                          DropDownListDialog(
                            radius: 5,
                            context: context,
                            title: optEmpStatus.title,
                            id: null,
                            ddTitleList: ddEmpStatus,
                            callback: (optionItem) {
                              optEmpStatus = optionItem;
                              setState(() {});
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    drawInputCurrencyBox(
                      context: context,
                      labelTxt: "Annual income",
                      tf: annualIncome,
                      focusNode: focusAnnualIncome,
                      focusNodeNext: focusValuationAmount,
                      len: 10,
                    ),
                    SizedBox(height: 20),
                    drawInputCurrencyBox(
                        context: context,
                        labelTxt: "Valuation / purchase price",
                        tf: valuationAmount,
                        focusNode: focusValuationAmount,
                        focusNodeNext: focusDepositAmount,
                        len: 8,
                        onChange: (v) {
                          try {
                            var valuationAmt = 0;
                            var depositAmt = 0;
                            if (v == '')
                              valuationAmt = 0;
                            else
                              valuationAmt = int.parse(v.trim());
                            final vv = depositAmount.text.trim();
                            depositAmt = vv != '' ? int.parse(v) : 0;
                            loanAmount.text =
                                (valuationAmt - depositAmt).toStringAsFixed(0);
                          } catch (e) {
                            depositAmount.clear();
                            loanAmount.clear();
                          }
                          calculation();
                        }),
                    SizedBox(height: 20),
                    drawInputCurrencyBox(
                        context: context,
                        labelTxt: caseIndex == 2 || caseIndex == 6
                            ? "Current mortgage balance"
                            : "Deposit amount",
                        tf: depositAmount,
                        focusNode: focusDepositAmount,
                        focusNodeNext: focusLoanAmount,
                        len: 10,
                        onChange: (v) {
                          try {
                            var valuationAmt = 0;
                            var depositAmt = 0;
                            if (valuationAmount.text.trim() == '')
                              valuationAmt = 0;
                            else
                              valuationAmt =
                                  int.parse(valuationAmount.text.trim());
                            depositAmt = v != '' ? int.parse(v) : 0;
                            if (depositAmt <= valuationAmt)
                              loanAmount.text = (valuationAmt - depositAmt)
                                  .toStringAsFixed(0);
                            else {
                              depositAmount.clear();
                            }
                          } catch (e) {
                            depositAmount.clear();
                            loanAmount.clear();
                          }
                          calculation();
                        }),
                    SizedBox(height: 20),
                    drawInputCurrencyBox(
                        context: context,
                        labelTxt: "Loan amount",
                        tf: loanAmount,
                        focusNode: focusLoanAmount,
                        focusNodeNext: focusDesc,
                        len: 10,
                        onChange: (v) {
                          try {
                            var valuationAmt = 0;
                            var loanAmt = 0;
                            if (valuationAmount.text.trim() == '')
                              valuationAmt = 0;
                            else
                              valuationAmt =
                                  int.parse(valuationAmount.text.trim());
                            loanAmt = v != '' ? int.parse(v) : 0;
                            if (loanAmt <= valuationAmt)
                              depositAmount.text =
                                  (valuationAmt - loanAmt).toStringAsFixed(0);
                            else
                              loanAmount.clear();
                          } catch (e) {
                            depositAmount.clear();
                            loanAmount.clear();
                          }
                          calculation();
                        }),
                    SizedBox(height: 20),
                    drawSignText(
                        title: "LTV", txt: ltv.toStringAsFixed(2), sign: "%"),
                    SizedBox(height: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: "Lead Notes",
                            txtColor: MyTheme.inputColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 10),
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: TextField(
                            controller: desc,
                            focusNode: focusDesc,
                            minLines: 2,
                            maxLines: 3,
                            //expands: true,
                            autocorrect: false,
                            maxLength: 255,
                            keyboardType: TextInputType.multiline,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: getTxtSize(
                                  context: context, txtSize: MyTheme.txtSize),
                            ),
                            decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(color: Colors.grey),
                              //labelText: 'Your message',
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    MMBtn(
                      txt: "Submit",
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 10,
                      callback: () {
                        if (validate()) {
                          final param = CaseLeadNegotiatorHelper().putParam(
                            caseType: optCaseType.title,
                            title: title.text.trim(),
                            fname: fname.text.trim(),
                            mname: "",
                            lname: lname.text.trim(),
                            email: email.text.trim(),
                            mobile: mobile.text.trim(),
                            note: desc.text.trim(),
                            dob: dob,
                            //
                            negotiatorId: 0,
                            groupId: 0,
                            //
                            leadReferenceId: "",
                            leadAddr: addr,
                            leadMortgageLengthLeft: mortgageLenLeft.text.trim(),
                            leadHomeValue: valuationAmount.text.trim(),
                            leadMortgagePurpose: depositAmount.text.trim(),
                            leadMortgageAmount: loanAmount.text.trim(),
                            leadMortgageBorrowLength: mortgageTerms.text.trim(),
                            leadCreditRating: ltv.toStringAsFixed(2),
                            leadEmploymentStatus: optEmpStatus.title,
                            leadYearlyIncome: annualIncome.text.trim(),
                            leadEmailConsent: "No",
                            estimatedEarning: estEarning.text.trim(),
                            //
                            areYouChargingAFee: isChargingFee ? 'Yes' : 'No',
                            chargeFeeAmount: chargingFee.text.trim(),
                            chargingFeeWhenPayable: optChargingFee.title,
                            chargingFeeRefundable:
                                isChargingFeeRefundable ? 'Yes' : 'No',
                            areYouChargingAnotherFee:
                                isChargingAnotherFee ? 'Yes' : 'No',
                            chargeAnotherFeeAmount:
                                chargingAnotherFee.text.trim(),
                            chargingAnotherFeeWhenPayable:
                                optChargingAnotherFee.title,
                            chargingAnotherFeeRefundable:
                                isChargingAnotherFeeRefundable ? 'Yes' : 'No',
                            //
                            resolution: widget.resolutions,
                            caseOwnerId: widget.optCaseOwner != null
                                ? widget.optCaseOwner.id != null
                                    ? int.parse(widget.optCaseOwner.id ?? 0)
                                    : 0
                                : 0,
                            caseOwnerName: widget.optCaseOwner != null
                                ? widget.optCaseOwner.title ?? ''
                                : '',
                          );
                          myLog(json.encode(param));
                          CaseLeadNavigatorAPIMgr().wsPutResolutionAPI(
                            context: context,
                            param: param,
                            callback: (model) {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    //final msg = model
                                    //.messages.resolution_post[0]
                                    //.toString();
                                    showToast(
                                        context: context,
                                        msg:
                                            "Lead has been updated successfully.",
                                        which: 1);
                                    Future.delayed(
                                        Duration(
                                            seconds: AppConfig.AlertDismisSec -
                                                1), () {
                                      Get.back(
                                          result:
                                              model.responseData.resolution);
                                    });
                                  } else {
                                    final err = model
                                        .errorMessages.resolution_post[0]
                                        .toString();
                                    showToast(
                                        context: context, msg: err, which: 0);
                                  }
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            },
                          );
                        }
                      },
                    )
                  ],
                ),
              ),
            ),
          )
        : SizedBox();
  }

  drawChargingFeeBox(
      TextEditingController tf,
      FocusNode focus,
      DropListModel dd,
      OptionItem opt,
      bool isCharging,
      bool isRef,
      Function(OptionItem) callback,
      Function(bool) callbackRefunable) {
    return isCharging
        ? Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                    txt: "How much?",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10)),
                        color: Colors.grey.shade400,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Txt(
                            txt: AppDefine.CUR_SIGN,
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize + .3,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        textInputAction: TextInputAction.next,
                        focusNode: focus,
                        controller: tf,
                        onEditingComplete: () {
                          FocusScope.of(context).requestFocus(focusFname);
                        },
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2)
                        ],
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        maxLength: 8,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          height: MyTheme.txtLineSpace,
                        ),
                        decoration: new InputDecoration(
                          counterText: "",
                          isDense: true,
                          hintText: "0",
                          hintStyle: new TextStyle(
                            color: Colors.grey,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            //height: MyTheme.txtLineSpace,
                          ),
                          contentPadding: EdgeInsets.only(
                              top: 10, bottom: 10, left: 20, right: 20),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            borderSide:
                                BorderSide(width: .5, color: Colors.black),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            borderSide:
                                BorderSide(width: .5, color: Colors.grey),
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              borderSide: BorderSide(
                                width: .5,
                              )),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Txt(
                    txt: "When payable?",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                DropDownListDialog(
                  context: context,
                  title: opt.title,
                  ddTitleList: dd,
                  callback: (optionItem) {
                    //opt = optionItem;
                    callback(optionItem);
                  },
                ),
                SizedBox(height: 20),
                Txt(
                    txt: "Refundable",
                    txtColor: MyTheme.inputColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                        child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              callbackRefunable(true);
                            },
                            child: radioButtonitem2(
                                context: context,
                                text: "Yes",
                                txtSize: 1.7,
                                bgColor: isRef
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black))),
                    SizedBox(width: 10),
                    Flexible(
                        child: GestureDetector(
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              callbackRefunable(false);
                            },
                            child: radioButtonitem2(
                                context: context,
                                text: "No",
                                txtSize: 1.7,
                                bgColor: !isRef
                                    ? MyTheme.purpleColor
                                    : Color(0xFF000),
                                textColor: Colors.black))),
                  ],
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
