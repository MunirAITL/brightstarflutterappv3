import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/api/auth/otp/Sms3APIMgr.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOTPModel.dart';
import 'package:aitl/view/auth/LoginScreen.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/db_intro/intro_cfg.dart';
import '../../db_intr/MainIntroducerScreen.dart';

class Sms3Screen extends StatefulWidget {
  final bool isBack;
  final MobileUserOTPModel mobileUserOTPModel;

  const Sms3Screen({
    Key key,
    @required this.mobileUserOTPModel,
    this.isBack = false,
  }) : super(key: key);

  @override
  State createState() => new _Sms3ScreenState();
}

class _Sms3ScreenState extends State<Sms3Screen>
    with SingleTickerProviderStateMixin, Mixin {
  // final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = false;

  String countryCode = "+44";
  String countryName = "GB";

  // Variables
  static const int SMS_AUTH_CODE_LEN = 6;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fiveDigit;
  int _sixDigit;

  var userId = 0;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    super.dispose();
  }

  void clearOtp() {
    _sixDigit = null;
    _fiveDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {
    try {
      userId = userData.userModel.id;
    } catch (e) {}
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;

          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
  }

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
        _otpTextField(_fiveDigit),
        _otpTextField(_sixDigit),
      ],
    );
  }

  // Returns "Resend" button
  get _getResendButton {
    return Center(
      child: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        child: Btn(
            txtColor: Colors.white,
            bgColor: MyTheme.brandColor,
            txt: (!widget.isBack) ? "Continue" : "Back",
            height: getHP(context, 6),
            width: getW(context) / 2,
            callback: () {
              if (!widget.isBack) {
                Get.offAll(() => LoginScreen());
              } else {
                Get.back();
              }
            }),
      ),

      /* child: Txt(
        txt: "Resend code",
        txtColor: Colors.black,
        txtSize: MyTheme.txtSize - .2,
        txtAlign: TextAlign.center,
        isBold: false,
      ),*/
    );
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 80,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new SizedBox(
                    width: 80.0,
                  ),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            if (_sixDigit != null) {
                              _sixDigit = null;
                            } else if (_fiveDigit != null) {
                              _fiveDigit = null;
                            } else if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          iconTheme:
              IconThemeData(color: MyTheme.brandColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.themeData.accentColor,
          child: _getOtpKeyboard,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        // width: getW(context),
        // height: getH(context),
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Column(
                children: [
                  Txt(
                    txt: "Verify yourself",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1,
                    txtAlign: TextAlign.center,
                    isBold: true,
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Center(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: new TextSpan(
                          children: <TextSpan>[
                            new TextSpan(
                                text:
                                    "We have sent a code in your mobile number. Please enter the code here.\n${widget.mobileUserOTPModel.mobileNumber.toString().substring(0, 3)}****${widget.mobileUserOTPModel.mobileNumber.toString().substring(widget.mobileUserOTPModel.mobileNumber.length - 5)}   ",
                                style: new TextStyle(
                                  fontSize: getTxtSize(
                                      context: context,
                                      txtSize: MyTheme.txtSize - .2),
                                  color: Colors.grey,
                                  fontWeight: FontWeight.normal,
                                )),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: ' Resend Code',
                                  recognizer: new TapGestureRecognizer()
                                    ..onTap = () {
                                      Sms2APIMgr().wsLoginMobileOtpPostAPI(
                                        context: context,
                                        countryCode: countryCode,
                                        mobile: widget
                                            .mobileUserOTPModel.mobileNumber,

                                        // mobile: getFullPhoneNumber(countryCodeTxt: countryCode,number: _phoneController.text.trim()),
                                        callback: (model) {
                                          if (model != null && mounted) {
                                            try {
                                              if (model.success) {
                                                try {
                                                  //final msg = model.messages.postUserotp[0].toString();
                                                  //showToast(txtColor: Colors.white, bgColor: MyTheme.redColor,msg: msg, which: 1);
                                                  if (mounted) {
                                                    Sms2APIMgr()
                                                        .wsSendOtpNotiAPI(
                                                            context: context,
                                                            otpId: model
                                                                .responseData
                                                                .userOTP
                                                                .id,
                                                            callback: (model) {
                                                              if (model !=
                                                                      null &&
                                                                  mounted) {
                                                                try {
                                                                  if (model
                                                                      .success) {
                                                                    try {
                                                                      showToast(
                                                                          context:
                                                                              context,
                                                                          msg:
                                                                              "OPT has been sent");
                                                                    } catch (e) {
                                                                      myLog(e
                                                                          .toString());
                                                                    }
                                                                  } else {
                                                                    try {
                                                                      if (mounted) {
                                                                        final err = model
                                                                            .messages
                                                                            .postUserotp[0]
                                                                            .toString();
                                                                        showToast(
                                                                            context:
                                                                                context,
                                                                            msg:
                                                                                err);
                                                                      }
                                                                    } catch (e) {
                                                                      myLog(e
                                                                          .toString());
                                                                    }
                                                                  }
                                                                } catch (e) {
                                                                  myLog(e
                                                                      .toString());
                                                                }
                                                              }
                                                            });
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              } else {
                                                try {
                                                  if (mounted) {
                                                    final err = model
                                                        .messages.postUserotp[0]
                                                        .toString();
                                                    showToast(
                                                        context: context,
                                                        msg: err);
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        },
                                      );
                                    },
                                  style: new TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: getTxtSize(
                                        context: context,
                                        txtSize: MyTheme.txtSize - .1),
                                    color: MyTheme.brandColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            _getInputField,
            //SizedBox(height: 20),
            _getResendButton,
            /*SizedBox(height: 20),
            Center(
              child: CupertinoButton(
                onPressed: () => {Get.offAll(() => RegScreen())},
                color: MyTheme.redColor,
                borderRadius: new BorderRadius.circular(20.0),
                child: new Text(
                  "Continue",
                  textAlign: TextAlign.center,
                  style: new TextStyle(color: Colors.white),
                ),
              ),
            ),
            SizedBox(height: 20),*/
          ],
        ),
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    int boxSpace = 2 * 6;
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      child: Txt(
          txt: digit != null ? digit.toString() : "",
          txtColor: Colors.white,
          txtSize: MyTheme.txtSize + 1,
          txtAlign: TextAlign.start,
          isBold: true),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.red),
        color: MyTheme.brandColor,
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: Txt(
                txt: label,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + 1.8,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;
        } else if (_fiveDigit == null) {
          _fiveDigit = _currentDigit;
        } else if (_sixDigit == null) {
          _sixDigit = _currentDigit;

          final otpCode = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString() +
              _fiveDigit.toString() +
              _sixDigit.toString();
          if (otpCode.length == SMS_AUTH_CODE_LEN) {
            callAllApi(otpCode);
          }
        }
      });
    }
  }

  void callAllApi(String otpCode) {
    Sms3APIMgr().wsLoginMobileOtpPutAPI(
      context: context,
      otpCode: otpCode,
      userID: userId,
      mobile: widget.mobileUserOTPModel.mobileNumber,
      callback: (model) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                //final msg = model.messages.postUserotp[0].toString();
                //showToast(txtColor: Colors.white, bgColor: MyTheme.redColor,msg: msg, which: 1);
                if (mounted) {
                  Sms3APIMgr().wsLoginMobileFBPostAPI(
                    context: context,
                    mobileUserOTPModel: model.responseData.userOTP,
                    otpCode: otpCode,
                    callback: (model2) async {
                      if (model2 != null && mounted) {
                        try {
                          if (model2.success) {
                            try {
                              await DBMgr.shared.setUserProfile(
                                  user: model2.responseData.user);
                              await userData.setUserModel();

                              if (model2.responseData.user.communityID ==
                                  IntroCfg.INTRO_COMMUNITYID.toString()) {
                                appData.isIntroducer = true;
                                Get.offAll(() => MainIntroducerScreen());
                              } else {
                                appData.isIntroducer = false;
                                Get.offAll(
                                  () => MainCustomerScreen(),
                                );
                              }
                            } catch (e) {
                              myLog(e.toString());
                            }
                          } else {
                            try {
                              //final err =
                              //model.errorMessages.login[0].toString();
                              //debugPrint("Error message = ${err}");
                              /* Get.defaultDialog(
                                    title: "Alert !",
                                    content: Text(
                                        "User not found with this number ${widget.mobileUserOTPModel.mobileNumber}.Press Ok to create account"),
                                    textConfirm: "Ok",
                                    onConfirm: () {
                                      navTo(context: context, isRep: true, page: () => RegScreen(phoneNumber:widget.mobileUserOTPModel.mobileNumber.toString().substring(2)));
                                    });*/

                              Get.off(RegScreen());
                              // navTo(context: context, isRep: false, page: () => RegScreen(phoneNumber:widget.mobileUserOTPModel.mobileNumber.toString().substring(2),));

                              /*  Get.dialog(AlrtDialogUserNotFound(
                                  bgColor: MyTheme.redColor,
                                  mobileNumber: widget.mobileUserOTPModel.mobileNumber.toString(),
                                msg: "User not found with this number.\nPress Ok to create account",));*/

                              /*   showToast(
                                    txtColor: Colors.white,
                                    bgColor: MyTheme.redColor,
                                    msg:
                                        "We were unable to verify you. You may have entered a wrong code."
                                        "If you do not have access to the registered mobile phone, please contact your adviser/ company admin.");*/

                            } catch (e) {
                              myLog(e.toString());
                            }
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      }
                    },
                  );
                }
              } catch (e) {
                myLog(e.toString());
              }
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.postUserotp[0];
                  showToast(
                    context: context,
                    msg: err,
                  );
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }
}

class AlrtDialogUserNotFound extends StatefulWidget {
  final String msg;
  final Color bgColor;
  String mobileNumber;

  AlrtDialogUserNotFound(
      {Key key,
      @required this.msg,
      @required this.bgColor,
      @required this.mobileNumber})
      : super(key: key);
  @override
  State createState() => _AlertDlgState();
}

class _AlertDlgState extends State<AlrtDialogUserNotFound> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: widget.bgColor,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  widget.msg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    height: 1.5,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: getWP(context, 25), right: getWP(context, 25)),
                child: MaterialButton(
                  onPressed: () {
                    Get.to(() => RegScreen());
                  },
                  child: Text(
                    "Ok",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
