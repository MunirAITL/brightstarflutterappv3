import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/api/auth/RegAPIMgr.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxfWithCountryCode.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/switchview/SwitchView.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/TCView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../config/db_intro/intro_cfg.dart';
import '../db_intr/MainIntroducerScreen.dart';

class RegScreen extends StatefulWidget {
  final bool isOTPComeThrough;

  const RegScreen({Key key, this.isOTPComeThrough = false}) : super(key: key);
  @override
  State createState() => _RegScreenState();
}

enum genderEnum { male, female }

class _RegScreenState extends State<RegScreen> with Mixin {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  final _compName = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusPwd = FocusNode();
  final focusCompName = FocusNode();

  bool _isSwitch = true;

  String dob = "";

  String dobDD = "";

  String dobMM = "";

  String dobYY = "";

  String countryCode = "+44";
  String countryName = "GB";

  genderEnum _gender; // = genderEnum.male;

  bool isStep2 = false;
  bool isLoading = false;

  final ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    _compName.dispose();
    _scrollController.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    getMobCode();
  }

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;

    return fullNumber;
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, _email, "Email address is required")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, _pwd)) {
      return false;
    } else if (_isSwitch && !UserProfileVal().isComNameOK(context, _compName)) {
      return false;
    }
    return true;
  }

  /*validate2() {
    if (!UserProfileVal().isDOBOK(context, dob, MyTheme.brandColor)) {
      return false;
    }
    return true;
  }*/

  refreshStep1() {
    setState(() {
      isStep2 = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.themeData.accentColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  //expandedHeight: 60,
                  elevation: 0,
                  //toolbarHeight: getHP(context, 10),
                  backgroundColor: MyTheme.appbarColor,
                  iconTheme: IconThemeData(
                      color: MyTheme.brandColor //change your color here
                      ),
                  pinned: true,
                  floating: false,
                  snap: false,
                  forceElevated: false,
                  centerTitle: false,
                  leading: widget.isOTPComeThrough
                      ? SizedBox()
                      : IconButton(
                          icon: Icon(
                            (isStep2) ? Icons.arrow_back : Icons.close,
                          ),
                          onPressed: () async {
                            (!isStep2) ? Get.back() : refreshStep1();
                          }),

                  title: UIHelper()
                      .drawAppbarTitle(title: "Join " + AppDefine.APP_NAME),
                  /*actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        _drawerKey.currentState.openDrawer();
                      },
                      icon: Icon(Icons.menu),
                    )
                  ],*/
                  bottom: PreferredSize(
                      preferredSize: new Size(
                          getW(context), getHP(context, (isLoading) ? .5 : 0)),
                      child: (isLoading)
                          ? AppbarBotProgBar(
                              backgroundColor: MyTheme.appbarProgColor,
                            )
                          : SizedBox()),
                ),
              ];
            },
            body: (!isStep2) ? drawRegView1() : drawRegView2(),
          ),
        ),
      ),
    );
  }

  drawRegView1() {
    /*Future.delayed(Duration.zero, () async {
      _scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });*/
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
      child: Container(
        width: getW(context),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: [
                      SizedBox(height: 20),
                      drawInputBox(
                        context: context,
                        title: "First name",
                        input: _fname,
                        kbType: TextInputType.name,
                        inputAction: TextInputAction.next,
                        focusNode: focusFname,
                        focusNodeNext: focusLname,
                        len: 20,
                      ),
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Last name",
                        input: _lname,
                        kbType: TextInputType.name,
                        inputAction: TextInputAction.next,
                        focusNode: focusLname,
                        focusNodeNext: focusEmail,
                        len: 20,
                      ),
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Email",
                        input: _email,
                        kbType: TextInputType.emailAddress,
                        inputAction: TextInputAction.next,
                        focusNode: focusEmail,
                        focusNodeNext: focusMobile,
                        len: 50,
                      ),
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Phone Number",
                        input: _mobile,
                        kbType: TextInputType.phone,
                        inputAction: TextInputAction.next,
                        focusNode: focusMobile,
                        focusNodeNext: focusPwd,
                        len: 20,
                      ),
                      /*drawInputBoxWithCountryCode(
                          context: context,
                          title: "Phone Number",
                          input: _mobile,
                          ph: "xxxx xxx xxx",
                          kbType: TextInputType.phone,
                          inputAction: TextInputAction.next,
                          focusNode: focusMobile,
                          focusNodeNext: focusPwd,
                          len: 15,
                          countryCode: countryCode,
                          countryName: countryName,
                          getCountryCode: (value) {
                            countryCode = value.toString();
                            print("Country Code Clik = " + countryCode);
                            PrefMgr.shared
                                .setPrefStr("countryName", value.code);
                            PrefMgr.shared
                                .setPrefStr("countryCode", value.toString());
                          }),*/
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Password",
                        input: _pwd,
                        kbType: TextInputType.text,
                        inputAction: _isSwitch
                            ? TextInputAction.next
                            : TextInputAction.done,
                        focusNode: focusPwd,
                        focusNodeNext: _isSwitch ? focusCompName : null,
                        len: 20,
                        isPwd: true,
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10),
              drawCompSwitch(),
              SizedBox(height: 20),
              TCView(screenName: 'Continue', txt2Color: MyTheme.brandColor),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
                child: BSBtn(
                    txt: "Continue",
                    height: getHP(context, 6),
                    callback: () {
                      if (validate1()) {
                        isStep2 = true;
                        getMobCode();
                        setState(() {});
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    Future.delayed(Duration.zero, () async {
      _scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });

    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
      child: Container(
        width: getW(context),
        //height: getH(context),
        child: SingleChildScrollView(
          controller: _scrollController,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Txt(
                      txt: "Provide a date of birth (Optional)",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Txt(
                      txt:
                          "To signup, you must be 18 or older. Other people won't see your birthday.",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - 0.4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: DatePickerView(
                    cap: null,
                    dt: (dob == '') ? 'Select a date' : dob,
                    initialDate: dateDOBlast,
                    firstDate: dateDOBfirst,
                    lastDate: dateDOBlast,
                    callback: (value) {
                      if (mounted) {
                        setState(() {
                          try {
                            dob = DateFormat('dd-MMM-yyyy')
                                .format(value)
                                .toString();
                            final dobArr = dob.toString().split('-');
                            this.dobDD = dobArr[0];
                            this.dobMM = dobArr[1];
                            this.dobYY = dobArr[2];
                          } catch (e) {
                            myLog(e.toString());
                          }
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 20),
                drawGender(),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: BSBtn(
                      txt: "Get Started",
                      height: getHP(context, 6),
                      callback: () async {
                        //if (validate2()) {
                        setState(() {
                          isLoading = true;
                        });
                        RegAPIMgr().wsRegAPI(
                            context: context,
                            email: _email.text.trim(),
                            pwd: _pwd.text.trim(),
                            fname: _fname.text.trim(),
                            lname: _lname.text.trim(),
                            mobile: getFullPhoneNumber(
                                    countryCodeTxt: countryCode,
                                    number: _mobile.text.toString())
                                .trim(),
                            dob: dob,
                            dobDD: dobDD,
                            dobMM: dobMM,
                            dobYY: dobYY,
                            companyName: _compName.text.trim(),
                            countryCode: countryCode,
                            cohort: _gender != null
                                ? (_gender == genderEnum.male)
                                    ? "Male"
                                    : "Female"
                                : "",
                            callback: (model) async {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    //  recall login to get cookie
                                    LoginAPIMgr().wsLoginAPI(
                                        context: context,
                                        email: _email.text.trim(),
                                        pwd: _pwd.text.trim(),
                                        callback: (model2) async {
                                          if (model2 != null && mounted) {
                                            try {
                                              setState(() {
                                                isLoading = false;
                                              });
                                              if (model2.success) {
                                                try {
                                                  await DBMgr.shared
                                                      .setUserProfile(
                                                    user: model2
                                                        .responseData.user,
                                                  );
                                                  await userData.setUserModel();

                                                  if (!Server.isOtp) {
                                                    if (model.responseData.user
                                                            .communityID ==
                                                        IntroCfg
                                                            .INTRO_COMMUNITYID
                                                            .toString()) {
                                                      Get.off(() =>
                                                          MainIntroducerScreen());
                                                    } else {
                                                      Get.off(() =>
                                                          MainCustomerScreen());
                                                    }
                                                  } else {
                                                    if (model2.responseData.user
                                                        .isMobileNumberVerified) {
                                                      if (model
                                                              .responseData
                                                              .user
                                                              .communityID ==
                                                          IntroCfg
                                                              .INTRO_COMMUNITYID
                                                              .toString()) {
                                                        Get.off(() =>
                                                            MainIntroducerScreen());
                                                      } else {
                                                        Get.off(() =>
                                                            MainCustomerScreen());
                                                      }
                                                    } else {
                                                      Sms2APIMgr()
                                                          .wsLoginMobileOtpPostAPI(
                                                        context: context,
                                                        countryCode: "",
                                                        mobile:
                                                            _mobile.text.trim(),
                                                        callback: (model3) {
                                                          if (model3 != null &&
                                                              mounted) {
                                                            try {
                                                              if (model3
                                                                  .success) {
                                                                try {
                                                                  if (mounted) {
                                                                    Sms2APIMgr().wsSendOtpNotiAPI(
                                                                        context: context,
                                                                        otpId: model3.responseData.userOTP.id,
                                                                        callback: (model4) {
                                                                          if (model4 != null &&
                                                                              mounted) {
                                                                            try {
                                                                              if (model4.success) {
                                                                                try {
                                                                                  Get.to(
                                                                                    () => Sms3Screen(
                                                                                      mobileUserOTPModel: model4.responseData.userOTP,
                                                                                    ),
                                                                                  );
                                                                                } catch (e) {
                                                                                  myLog(e.toString());
                                                                                }
                                                                              } else {
                                                                                try {
                                                                                  if (mounted) {
                                                                                    final err = model4.messages.postUserotp[0].toString();
                                                                                    showToast(context: context, msg: err);
                                                                                  }
                                                                                } catch (e) {
                                                                                  myLog(e.toString());
                                                                                }
                                                                              }
                                                                            } catch (e) {
                                                                              myLog(e.toString());
                                                                            }
                                                                          }
                                                                        });
                                                                  }
                                                                } catch (e) {
                                                                  myLog(e
                                                                      .toString());
                                                                }
                                                              } else {
                                                                try {
                                                                  if (mounted) {
                                                                    final err = model3
                                                                        .messages
                                                                        .postUserotp[
                                                                            0]
                                                                        .toString();
                                                                    showToast(
                                                                        context:
                                                                            context,
                                                                        msg:
                                                                            err);
                                                                  }
                                                                } catch (e) {
                                                                  myLog(e
                                                                      .toString());
                                                                }
                                                              }
                                                            } catch (e) {
                                                              myLog(
                                                                  e.toString());
                                                            }
                                                          }
                                                        },
                                                      );
                                                    }
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              } else {
                                                try {
                                                  if (mounted) {
                                                    final err = model2
                                                        .errorMessages.login[0]
                                                        .toString();
                                                    showToast(
                                                        context: context,
                                                        msg: err);
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        });
                                  } else {
                                    try {
                                      setState(() {
                                        isLoading = false;
                                      });
                                      if (mounted) {
                                        final err = model
                                            .errorMessages.register[0]
                                            .toString();
                                        showToast(context: context, msg: err);
                                      }
                                    } catch (e) {
                                      myLog(e.toString());
                                    }
                                  }
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            });
                      }
                      //},
                      ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20),
      child: Container(
        //color: Colors.yellow,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
              txt: "Gender (Optional)",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Theme(
              data: MyTheme.radioThemeData,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _gender = genderEnum.male;
                        });
                      }
                    },
                    child: Radio(
                      value: genderEnum.male,
                      groupValue: _gender,
                      onChanged: (genderEnum value) {
                        if (mounted) {
                          setState(() {
                            _gender = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Male",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _gender = genderEnum.female;
                        });
                      }
                    },
                    child: Radio(
                      value: genderEnum.female,
                      groupValue: _gender,
                      onChanged: (genderEnum value) {
                        if (mounted) {
                          setState(() {
                            _gender = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Female",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawCompSwitch() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Txt(
                      txt:
                          "Do you have a mortgage company? Otherwise we will recommend to you.",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false,
                      txtLineSpace: 1.2,
                    ),
                  ),
                  SizedBox(width: 5),
                  SwitchView(
                    isSelected: _isSwitch,
                    list: ["Yes", "No"],
                    onChanged: (value) {
                      _isSwitch = value;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            (_isSwitch)
                ? Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, top: 10),
                    child: InputBox(
                      context: context,
                      ctrl: _compName,
                      lableTxt: "Company Name",
                      kbType: TextInputType.text,
                      inputAction: TextInputAction.done,
                      focusNode: focusCompName,
                      len: 100,
                      isPwd: false,
                      //leftPad: 0,
                      //rightPad: 0,
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
