import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:aitl/controller/api/auth/ForgotAPIMgr.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/auth/ForgotScreen.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../db_intr/MainIntroducerScreen.dart';
import 'otp/Sms3Screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  State createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Mixin {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();
  final focusEmail = FocusNode();
  final focusPwd = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;

          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    try {
      if (!Server.isOtp) {
        //_email.text = "rc.cust.23@mymail.infos.st"; //  live
        //_email.text = "occbs.cust0081@yopmail.com";
        //"test@testuser.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        _email.text =
            "ROsa.MASINT001@HELP.MORTGAGE-MAGIC.CO.UK"; //"occbs.cust0067@yopmail.com";
        _pwd.text = "12345678";
      }
    } catch (e) {}
  }

  validate() {
    if (UserProfileVal().isEmpty(context, _email, 'Email/Phone is required')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, _pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    //  mamun.masn001@yopmail.com/123 -> introducer login
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.loginBGColor,
        resizeToAvoidBottomInset: true,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Center(
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: getWP(context, 14),
                      width: getWP(context, 14),
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: new AssetImage("assets/images/logo/eplus.png"),
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 10),
                      child: Card(
                        child: Container(
                          color: Colors.white,
                          width: getW(context),
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              children: [
                                SizedBox(height: 40),
                                drawInputBox(
                                  context: context,
                                  title: "Email Address",
                                  input: _email,
                                  kbType: TextInputType.emailAddress,
                                  inputAction: TextInputAction.next,
                                  focusNode: focusEmail,
                                  focusNodeNext: focusPwd,
                                  len: 50,
                                ),
                                SizedBox(height: 10),
                                drawInputBox(
                                  context: context,
                                  title: "Password",
                                  input: _pwd,
                                  kbType: TextInputType.text,
                                  inputAction: TextInputAction.done,
                                  focusNode: focusPwd,
                                  len: 20,
                                  isPwd: true,
                                ),
                                SizedBox(height: 20),
                                BSBtn(
                                    txt: "Sign in",
                                    height: getHP(context, 6),
                                    callback: () {
                                      if (validate()) {
                                        LoginAPIMgr().wsLoginAPI(
                                          context: context,
                                          email: _email.text.trim(),
                                          pwd: _pwd.text.trim(),
                                          callback: (model) async {
                                            if (model != null && mounted) {
                                              if (model.success) {
                                                await DBMgr.shared
                                                    .setUserProfile(
                                                        user: model
                                                            .responseData.user);
                                                await userData.setUserModel();
                                                if (!Server.isOtp) {
                                                  if (model.responseData.user
                                                          .communityID ==
                                                      IntroCfg.INTRO_COMMUNITYID
                                                          .toString()) {
                                                    appData.isIntroducer = true;
                                                    Get.offAll(() =>
                                                        MainIntroducerScreen());
                                                  } else {
                                                    appData.isIntroducer =
                                                        false;
                                                    Get.offAll(() =>
                                                        MainCustomerScreen());
                                                  }
                                                } else {
                                                  if (!model.responseData.user
                                                      .isMobileNumberVerified) {
                                                    Sms2APIMgr()
                                                        .wsLoginMobileOtpPostAPI(
                                                      context: context,
                                                      countryCode: countryCode,
                                                      mobile: model.responseData
                                                          .user.mobileNumber,
                                                      // mobile: getFullPhoneNumber(countryCodeTxt: countryCode,number: _phoneController.text.trim()),
                                                      callback: (model) {
                                                        if (model != null &&
                                                            mounted) {
                                                          try {
                                                            if (model.success) {
                                                              try {
                                                                //final msg = model.messages.postUserotp[0].toString();
                                                                //showToast(txtColor: Colors.white, bgColor: MyTheme.redColor,msg: msg, which: 1);
                                                                if (mounted) {
                                                                  Sms2APIMgr()
                                                                      .wsSendOtpNotiAPI(
                                                                          context:
                                                                              context,
                                                                          otpId: model
                                                                              .responseData
                                                                              .userOTP
                                                                              .id,
                                                                          callback:
                                                                              (model) {
                                                                            if (model != null &&
                                                                                mounted) {
                                                                              try {
                                                                                if (model.success) {
                                                                                  try {
                                                                                    Get.off(
                                                                                      () => Sms3Screen(
                                                                                        mobileUserOTPModel: model.responseData.userOTP,
                                                                                      ),
                                                                                    );
                                                                                  } catch (e) {
                                                                                    myLog(e.toString());
                                                                                  }
                                                                                } else {
                                                                                  try {
                                                                                    if (mounted) {
                                                                                      final err = model.messages.postUserotp[0].toString();
                                                                                      showToast(context: context, msg: err);
                                                                                    }
                                                                                  } catch (e) {
                                                                                    myLog(e.toString());
                                                                                  }
                                                                                }
                                                                              } catch (e) {
                                                                                myLog(e.toString());
                                                                              }
                                                                            }
                                                                          });
                                                                }
                                                              } catch (e) {
                                                                myLog(e
                                                                    .toString());
                                                              }
                                                            } else {
                                                              try {
                                                                if (mounted) {
                                                                  final err = model
                                                                      .messages
                                                                      .postUserotp[
                                                                          0]
                                                                      .toString();
                                                                  showToast(
                                                                      context:
                                                                          context,
                                                                      msg: err);
                                                                }
                                                              } catch (e) {
                                                                myLog(e
                                                                    .toString());
                                                              }
                                                            }
                                                          } catch (e) {
                                                            myLog(e.toString());
                                                          }
                                                        }
                                                      },
                                                    );
                                                  } else {
                                                    if (model.responseData.user
                                                            .communityID ==
                                                        IntroCfg
                                                            .INTRO_COMMUNITYID
                                                            .toString()) {
                                                      Get.off(() =>
                                                          MainIntroducerScreen());
                                                    } else {
                                                      Get.off(() =>
                                                          MainCustomerScreen());
                                                    }
                                                  }
                                                }
                                              } else {
                                                try {
                                                  if (mounted) {
                                                    final err = model
                                                        .errorMessages.login[0]
                                                        .toString();
                                                    showToast(
                                                        context: context,
                                                        msg: err);
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              }
                                            }
                                          },
                                        );
                                      }
                                    }),
                                //drawOtpBtn(),
                                SizedBox(height: 50),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: GestureDetector(
                              onTap: () {
                                Get.to(() => ForgotScreen());
                              },
                              child: Txt(
                                  txt: "Forgotten password",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 20, right: 20, bottom: 20),
                            child: GestureDetector(
                              onTap: () async {
                                Get.to(() => RegScreen(),
                                        fullscreenDialog: true)
                                    .then((value) {
                                  //callback(route);
                                });
                              },
                              child: Txt(
                                  txt: "Create an account",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
