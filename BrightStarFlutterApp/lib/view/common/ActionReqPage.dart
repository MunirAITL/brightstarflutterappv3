import 'package:aitl/model/json/db_cus/action_req/DashBoardListType.dart';
import 'package:aitl/view/common/dashboard_main_base.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

import '../../config/MyTheme.dart';
import '../../controller/helper/db_cus/tab_newcase/action_req_helper.dart';
import '../../model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import '../../model/json/db_cus/tab_newcase/MortgageCaseRecomendationInfos.dart';
import '../../view_model/helper/ui_helper.dart';

class ActionReqPage extends StatefulWidget {
  @override
  State<ActionReqPage> createState() => _ActionReqPageState();
}

class _ActionReqPageState extends BaseMainDashboard<ActionReqPage> {
  @override
  refreshData() async {
    try {
      await ActionReqHelper().wsActionReqAPIcall(
        context: context,
        callback: (
          List<DashBoardListType> listUserList,
          List<dynamic> listUserNotesModel,
          List<MortgageCaseRecomendationInfos> caseReviewModelList,
          List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
        ) {
          if (mounted && listUserList != null) {
            if (listUserList.length > 0) {
              drawActionRequiredItems(
                  listUserList,
                  listUserNotesModel,
                  caseReviewModelList,
                  caseMortgageAgreementReviewInfosList,
                  false);
            }
          }
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        },
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    widActionRequired = null;
    super.dispose();
  }

  initPage() async {
    try {
      setState(() {
        isLoading = true;
      });
      await refreshData();
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Action Required"),
          iconTheme: IconThemeData(color: MyTheme.brandColor),
          backgroundColor: MyTheme.titleColor,
          bottom: PreferredSize(
              preferredSize:
                  new Size(getW(context), getHP(context, (isLoading) ? .5 : 0)),
              child: (isLoading)
                  ? AppbarBotProgBar(
                      backgroundColor: MyTheme.appbarProgColor,
                    )
                  : SizedBox()),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return isLoading
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
                primary: true,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20, left: 20, right: 20, bottom: 10),
                        child: Txt(
                            txt:
                                "A green tick shows that the verification is currently active.",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                      (widActionRequired != null)
                          ? Container(child: widActionRequired)
                          : SizedBox(),
                    ])));
  }
}
