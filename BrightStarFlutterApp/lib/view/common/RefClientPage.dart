import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_intr/createlead/usernote/PostCaseNoteAPIModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/view/common/dashboard_main_base.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RefClientPage extends StatefulWidget {
  const RefClientPage({Key key}) : super(key: key);
  @override
  State<RefClientPage> createState() => _RefClientPageState();
}

class _RefClientPageState extends BaseMainDashboard<RefClientPage> {
  //  dropdown
  DropListModel ddDiv = DropListModel([
    OptionItem(id: "43", title: "P_Test"),
    OptionItem(id: "33", title: "Residential"),
    OptionItem(id: "31", title: "Buy to Let"),
    OptionItem(
        id: "30", title: "Second charge mortgages- 2nd Charge Rest & BTL"),
    OptionItem(
        id: "29",
        title:
            "Short term lending- Bridging, Business lending, Developmet finance"),
    OptionItem(id: "28", title: "Commercial finance- Commercial, Business"),
    OptionItem(id: "27", title: "Master Panel -All types of cases"),
  ]);
  var optDiv = OptionItem(id: null, title: "Select Division").obs;

  DropListModel ddCont = DropListModel([
    OptionItem(id: "1", title: "Before"),
    OptionItem(id: "2", title: "After"),
  ]);
  var optCont = OptionItem(id: null, title: "Select contact type").obs;

  final name = TextEditingController();
  final mobile = TextEditingController();
  final phone = TextEditingController();
  final email = TextEditingController();
  final notes = TextEditingController();

  @override
  refreshData() {}

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    widActionRequired = null;
    super.dispose();
  }

  initPage() async {
    try {
      setState(() {
        isLoading = true;
      });
      await refreshData();
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Refer a client"),
          iconTheme: IconThemeData(color: MyTheme.brandColor),
          backgroundColor: MyTheme.titleColor,
          bottom: PreferredSize(
              preferredSize:
                  new Size(getW(context), getHP(context, (isLoading) ? .5 : 0)),
              child: (isLoading)
                  ? AppbarBotProgBar(
                      backgroundColor: MyTheme.appbarProgColor,
                    )
                  : SizedBox()),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        child: SingleChildScrollView(
            primary: true,
            child: Obx((() => Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Txt(
                            txt: "Division",
                            txtColor: MyTheme.statusBarColor,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 3),
                        DropDownListDialog(
                          context: context,
                          title: optDiv.value.title,
                          ddTitleList: ddDiv,
                          callback: (optionItem) {
                            optDiv.value = optionItem;
                          },
                        ),
                        SizedBox(height: 10),
                        drawInputBox(
                          context: context,
                          title: "Client name",
                          input: name,
                          kbType: TextInputType.name,
                          inputAction: TextInputAction.next,
                          focusNode: null,
                          focusNodeNext: null,
                          len: 50,
                        ),
                        SizedBox(height: 10),
                        drawInputBox(
                          context: context,
                          title: "Client mobile",
                          input: mobile,
                          kbType: TextInputType.phone,
                          inputAction: TextInputAction.next,
                          focusNode: null,
                          focusNodeNext: null,
                          len: 20,
                        ),
                        SizedBox(height: 10),
                        drawInputBox(
                          context: context,
                          title: "Client phone",
                          input: phone,
                          kbType: TextInputType.phone,
                          inputAction: TextInputAction.next,
                          focusNode: null,
                          focusNodeNext: null,
                          len: 20,
                        ),
                        SizedBox(height: 10),
                        drawInputBox(
                          context: context,
                          title: "Client email",
                          input: email,
                          kbType: TextInputType.emailAddress,
                          inputAction: TextInputAction.next,
                          focusNode: null,
                          focusNodeNext: null,
                          len: 50,
                        ),
                        SizedBox(height: 10),
                        Txt(
                            txt:
                                "Would you like us to contact you before/ after calling your client?",
                            txtColor: MyTheme.statusBarColor,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 3),
                        DropDownListDialog(
                          context: context,
                          title: optCont.value.title,
                          ddTitleList: ddCont,
                          callback: (optionItem) {
                            optCont.value = optionItem;
                          },
                        ),
                        SizedBox(height: 10),
                        Txt(
                            txt: "Notes",
                            txtColor: MyTheme.statusBarColor,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 3),
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: TextField(
                            controller: notes,
                            textInputAction: TextInputAction.newline,
                            minLines: 2,
                            maxLines: 4,
                            //expands: true,
                            autocorrect: false,
                            maxLength: 500,
                            keyboardType: TextInputType.multiline,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: getTxtSize(
                                  context: context, txtSize: MyTheme.txtSize),
                            ),
                            decoration: InputDecoration(
                              hintText: 'Description',
                              hintStyle: TextStyle(color: Colors.grey),
                              //labelText: 'Your message',
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        BSBtn(
                          txt: "Refer",
                          height: getHP(context, 6),
                          radius: 10,
                          callback: () async {
                            if (optDiv.value.id == null) {
                              showToast(
                                  context: context,
                                  msg: "Please select division");
                              return;
                            }
                            if (name.text.trim().length == 0) {
                              showToast(
                                  context: context,
                                  msg: "Name field is required");
                              return;
                            }
                            if (mobile.text.trim().length == 0) {
                              showToast(
                                  context: context,
                                  msg: "Mobile field is required");
                              return;
                            }
                            if (email.text.trim().length == 0) {
                              showToast(
                                  context: context,
                                  msg: "Email field is required");
                              return;
                            }

                            try {
                              final param = {
                                "ClientName": name.text.trim(),
                                "MobileNo": mobile.text.trim(),
                                "PhoneNo": phone.text.trim(),
                                "EmailAddress": email.text.trim(),
                                "Notes": notes.text.trim(),
                                "UserCompanyId":
                                    userData.userModel.userCompanyInfo.id,
                                "UserId": userData.userModel.id,
                                "DepartmentId": optDiv.value.id,
                                "ContactType": optCont.value.title,
                              };
                              await APIViewModel().req<CommonAPIModel>(
                                  context: context,
                                  url: ServerIntr.POST_REFER_CLIENTR_URL,
                                  reqType: ReqType.Post,
                                  param: param,
                                  callback: (model) async {
                                    if (mounted && model != null) {
                                      if (model.success) {
                                        showToast(
                                            context: context,
                                            msg: "Email Send Successfully",
                                            which: 1);
                                        Future.delayed(
                                            Duration(
                                                seconds: AppConfig
                                                    .AlertDismisSec), () {
                                          if (mounted) Get.back();
                                        });
                                      }
                                    }
                                  });
                            } catch (e) {}

                            /*else if (optPriority.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select priority");
                    return;
                  } else if (optNoteCat.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select note category");
                    return;
                  } else if (optNoteType.id == null) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please select note type");
                    return;
                  } else if (desc.text.trim().length == 0) {
                    showToast(
                        txtColor: Colors.white,
                        bgColor: MyTheme.redColor,
                        msg: "Please enter description");
                    return;
                  }*/
                          },
                        )
                      ]),
                )))));
  }
}
