import 'dart:io';
import 'package:aitl/model/data/PrefMgr.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:aitl/mixin.dart';
//  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1

enum enumFCM { onMessage, onLaunch, onResume }

class PushNotificationService with Mixin {
  // final FirebaseMessaging _fcm;
  // PushNotificationService(this._fcm);
  static const String fcmTokenKey = "fcmTokenKey";

  Future initialise({Function callback}) async {
    if (Platform.isIOS) {
      // _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    // If you want to test the push notification locally,
    // you need to get the token and input to the Firebase console
    // https://console.firebase.google.com/project/YOUR_PROJECT_ID/notification/compose
    // String token = await _fcm.getToken();

    //  store in shared pref
    // await PrefMgr.shared.setPrefStr(fcmTokenKey, token);
    // myLog("FirebaseMessaging token: $token");

    // _fcm.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     myLog("onMessage: $message");
    //     callback(enumFCM.onMessage, message);
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     myLog("onLaunch: $message");
    //     callback(enumFCM.onLaunch, message);
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     myLog("onResume: $message");
    //     callback(enumFCM.onResume, message);
    //   },
    // );
  }
}
