import 'package:aitl/controller/api/db_cus/noti/DeleteNotification.dart';
import 'package:aitl/model/json/auth/ForgotAPIModel.dart';
import 'package:aitl/model/json/auth/LoginAPIModel.dart';
import 'package:aitl/model/json/auth/RegAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/action_req/GetMortgageCaseInfoLocTaskidUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/case_lead/CaseLeadNegotiatorUserAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/model/json/db_cus/submit_case/SubmitCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/settings/ChangePwdAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseReviewAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/MessageTypeAdvisorAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotePutAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/PostCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNoteByEntityAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLinePostAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDDelAPIModel.dart';
import 'package:aitl/model/json/comp_acc/CompAccAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/EditCaseAPIModel.dart';

import '../../model/json/db_intr/caselead/CasePayInfoAPIModel.dart';
import '../../model/json/db_intr/caselead/GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.dart';
import '../../model/json/db_intr/createlead/CaseOwnerAPIModel.dart';
import '../../model/json/db_intr/createlead/ChangeStageAPIModel.dart';
import '../../model/json/db_intr/createlead/CreateLeadAPIModel.dart';
import '../../model/json/db_intr/createlead/GetAllResSupportAdminAPIModel.dart';
import '../../model/json/db_intr/createlead/GetCaseTypeByUserIdAPIModel.dart';
import '../../model/json/db_intr/createlead/GetLeadByResCompanyIdAPIModel.dart';
import '../../model/json/db_intr/createlead/GetTwilioPhoneNumberAPIModel.dart';
import '../../model/json/db_intr/createlead/GetUserTaskCatAPIModel.dart';
import '../../model/json/db_intr/createlead/PostCaseAPIModel2.dart';
import '../../model/json/db_intr/createlead/PostLeadGenAPIModel.dart';
import '../../model/json/db_intr/createlead/UpdateCaseOwner4MeAPIModel.dart';
import '../../model/json/db_intr/createlead/UserDetailsDataAutoSuggAPIModel.dart';
import '../../model/json/db_intr/createlead/UserNotesAPIModel.dart';
import '../../model/json/db_intr/createlead/email/GetEmailSmsTemplatesAPIModel.dart';
import '../../model/json/db_intr/createlead/email/GetSyncEmailRefreshAPIModel.dart';
import '../../model/json/db_intr/createlead/email/SendInvitationEmailAPIModel.dart';
import '../../model/json/db_intr/createlead/usernote/PostCaseNoteAPIModel.dart';
import '../../model/json/db_intr/leadfetch/LeadecaseApiModel.dart';

//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
/*
class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      //  misc
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserProfileAPIModel)) {
      return UserProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNoteByEntityAPIModel)) {
      return UserNoteByEntityAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotePutAPIModel)) {
      return UserNotePutAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, EditCaseAPIModel)) {
      return EditCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAdvisorAPIModel)) {
      return TimeLineAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, MessageTypeAdvisorAPIModel)) {
      return MessageTypeAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, CompAccAPIModel)) {
      return CompAccAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeAPIModel)) {
      return BadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeEmailAPIModel)) {
      return BadgeEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDAPIModel)) {
      return BadgePhotoIDAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDDelAPIModel)) {
      // return BadgePhotoIDDelAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseReviewAPIModel)) {
      return CaseReviewAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseReviewAPIModel)) {
      return CaseReviewAPIModel.fromJson(json) as T;
    }else if (identical(T, DeleteNotificationModel)) {
      return DeleteNotificationModel.fromJson(json) as T;
    }else if (identical(T, SubmitCaseAPIModel)) {
      return SubmitCaseAPIModel.fromJson(json) as T;
    }
  }
}
*/

class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, ChangePwdAPIModel)) {
      return ChangePwdAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserProfileAPIModel)) {
      return UserProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNoteByEntityAPIModel)) {
      return UserNoteByEntityAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotePutAPIModel)) {
      return UserNotePutAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, EditCaseAPIModel)) {
      return EditCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAdvisorAPIModel)) {
      return TimeLineAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, GetMortgageCaseInfoLocTaskidUseridAPIModel)) {
      return GetMortgageCaseInfoLocTaskidUseridAPIModel.fromJson(json) as T;
    } else if (identical(T, CompAccAPIModel)) {
      return CompAccAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeAPIModel)) {
      return BadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeEmailAPIModel)) {
      return BadgeEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDAPIModel)) {
      return BadgePhotoIDAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDDelAPIModel)) {
      // return BadgePhotoIDDelAPIModel.fromJson(json) as T;
    } else if (identical(T, DeleteNotificationModel)) {
      return DeleteNotificationModel.fromJson(json) as T;
    } else if (identical(T, CaseReviewAPIModel)) {
      return CaseReviewAPIModel.fromJson(json) as T;
    } else if (identical(T, MessageTypeAdvisorAPIModel)) {
      return MessageTypeAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, SubmitCaseAPIModel)) {
      return SubmitCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseLeadNegotiatorUserAPIModel)) {
      return CaseLeadNegotiatorUserAPIModel.fromJson(json) as T;
    } else if (identical(T, PrivacyPolicyAPIModel)) {
      return PrivacyPolicyAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSelfieByBase64DataAPIModel)) {
      return PostSelfieByBase64DataAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocByBase64BitAPIModel)) {
      return PostDocByBase64BitAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocVerifyAPIModel)) {
      return PostDocVerifyAPIModel.fromJson(json) as T;
    }

    //  ********************************************  DB_INTR
    else if (identical(T, SubmitCaseAPIModel)) {
      return SubmitCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, GetUserTaskCatAPIModel)) {
      return GetUserTaskCatAPIModel.fromJson(json) as T;
    } else if (identical(T, GetAllResSupportAdminAPIModel)) {
      return GetAllResSupportAdminAPIModel.fromJson(json) as T;
    } else if (identical(T, GetTwilioPhoneNumberAPIModel)) {
      return GetTwilioPhoneNumberAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseOwnerAPIModel)) {
      return CaseOwnerAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotesAPIModel)) {
      return UserNotesAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseLeadNegotiatorUserAPIModel)) {
      return CaseLeadNegotiatorUserAPIModel.fromJson(json) as T;
    } else if (identical(T, CasePayInfoAPIModel)) {
      return CasePayInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, GetlLeadStatusIntrwiseReportDatabyIntrAPIModel)) {
      return GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.fromJson(json) as T;
    } else if (identical(T, PrivacyPolicyAPIModel)) {
      return PrivacyPolicyAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSelfieByBase64DataAPIModel)) {
      return PostSelfieByBase64DataAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocByBase64BitAPIModel)) {
      return PostDocByBase64BitAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocVerifyAPIModel)) {
      return PostDocVerifyAPIModel.fromJson(json) as T;
    } else if (identical(T, CreateLeadAPIModel)) {
      return CreateLeadAPIModel.fromJson(json) as T;
    } else if (identical(T, LeadCaseApiModel)) {
      return LeadCaseApiModel.fromJson(json) as T;
    } else if (identical(T, PostLeadGenAPIModel)) {
      return PostLeadGenAPIModel.fromJson(json) as T;
    } else if (identical(T, ChangeStageAPIModel)) {
      return ChangeStageAPIModel.fromJson(json) as T;
    } else if (identical(T, GetCaseTypeByUserIdAPIModel)) {
      return GetCaseTypeByUserIdAPIModel.fromJson(json) as T;
    } else if (identical(T, UserDetailsDataAutoSuggAPIModel)) {
      return UserDetailsDataAutoSuggAPIModel.fromJson(json) as T;
    } else if (identical(T, SendInvitationEmailAPIModel)) {
      return SendInvitationEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, UpdateCaseOwner4MeAPIModel)) {
      return UpdateCaseOwner4MeAPIModel.fromJson(json) as T;
    } else if (identical(T, GetLeadByResCompanyIdAPIModel)) {
      return GetLeadByResCompanyIdAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseNoteAPIModel)) {
      return PostCaseNoteAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSyncEmailRefreshAPIModel)) {
      return GetSyncEmailRefreshAPIModel.fromJson(json) as T;
    } else if (identical(T, GetEmailSmsTemplatesAPIModel)) {
      return GetEmailSmsTemplatesAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel2)) {
      return PostCaseAPIModel2.fromJson(json) as T;
    }
  }
}
