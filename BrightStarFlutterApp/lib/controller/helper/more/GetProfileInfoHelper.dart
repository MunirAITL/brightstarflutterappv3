import 'package:aitl/config/Server.dart';

class GetProfileInfoHelper {
 static getUrl({String userId}) {
    var url = Server.Get_PROFILE_INFO_URL;
    url = url.replaceAll("#UserID#", userId.toString());
    return url;
  }
}
