import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NotiCfg.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotiHelper with Mixin {
  getUrl({pageStart, pageCount, caseStatus}) {
    var url = Server.NOTI_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    return url;
  }

  Map<String, dynamic> getNotiMap({NotiModel model}) {
    Map<String, dynamic> notiMap = {};
    try {
      //print(NotiCfg.EVENT_LIST.toString());
      for (var map in NotiCfg.EVENT_LIST) {
        final int listCommunityId = map["communityId"];
        int notificationEventId = map['notificationEventId'];
        if (listCommunityId == int.parse(userData.userModel.communityID)) {
          if (model.entityName == map["entityName"] &&
              (model.notificationEventId == notificationEventId ||
                  notificationEventId == 0)) {
            String txt = map["txt"];
            txt = txt.replaceAll(
                "#InitiatorDisplayName#", model.initiatorDisplayName);
            txt = txt.replaceAll("#EventName#", model.eventName);
            txt = txt.replaceAll('[] ', '');

            //  text processing
            notiMap['txt'] = txt ?? '';
            notiMap['publishDateTime'] =
                DateFun.getTimeAgoTxt(model.publishDateTime);

            //  event processing
            //notiMap['communityId'] = map['communityId'];
            //notiMap['desc'] = map['desc'];
            //notiMap['notificationEventId'] = map['notificationEventId'];
            notiMap['route'] =
                (map['route2'] == null) ? map['route'] : map['route2'];

            break;
          }
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
    return notiMap;
  }

  setRoute({
    BuildContext context,
    NotiModel notiModel,
    TimelineUserModel timelineUserModel,
    Map<String, dynamic> notiMap,
    Function callback,
  }) async {
    try {
      Type route = notiMap['route'];
      //if (identical(route, WebScreen)) {
      Get.to(
        () => WebScreen(
          title: notiModel.message,
          url: Server.BASE_URL_NOTI_WEB +
              notiModel.webUrl
                  .toString()
                  .replaceAll("/application/about-me", ""),
        ),
      ).then((value) {
        //callback(route);
      });
    } catch (e) {}
  }
}

/*extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}*/
