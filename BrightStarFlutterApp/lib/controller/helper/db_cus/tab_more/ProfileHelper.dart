import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:polygon_clipper/polygon_border.dart';

class ProfileHelper with Mixin {
  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            Icons.star,
            color: (i < rate) ? colr : Colors.grey,
            size: 20,
          ),
      ],
    );
  }

  getStarRatingView({int rate, int reviews}) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getStarsRow(rate, MyTheme.brandColor),
          SizedBox(width: 10),
          Txt(
              txt: reviews.toString() + " Reviews",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getCompletionText({int pa}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(
              txt: pa.toString() + "% Completion Rate",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          IconButton(
            icon: Icon(
              Icons.info,
              color: Colors.grey, //MyTheme.themeData.accentColor,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  getAvatorStarView({int rate}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(18.0),
            margin: EdgeInsets.only(right: 16.0),
            decoration: ShapeDecoration(
                shape: PolygonBorder(
                    sides: 6,
                    borderRadius: 1,
                    border: BorderSide(color: Colors.grey, width: 0.5))),
            child: Image.asset(
              "assets/images/icons/user_icon.png",
              width: 40,
              height: 40,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              getStarsRow(rate, Colors.cyan),
              SizedBox(height: 10),
              Txt(
                  txt: DateFun.getTimeAgoTxt("2021-01-27T16:16:33.47Z"),
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          )
        ],
      ),
    );
  }
}
