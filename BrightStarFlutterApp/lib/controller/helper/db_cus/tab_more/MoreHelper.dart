import 'package:aitl/config/Server.dart';
import 'package:aitl/view/db_cus/more/help/HelpScreen.dart';
import 'package:aitl/view/db_cus/more/profile/ProfileScreen.dart';
import 'package:aitl/view/db_cus/more/reviews/ReviewsScreen.dart';
import 'package:aitl/view/db_cus/more/settings/SettingsScreen.dart';
import 'package:aitl/view/db_cus/noti/NotiTab.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MoreHelper {
  static const List<Map<String, dynamic>> listMore = [
    {"title": "Profile", "route": ProfileScreen},
    //{"title": "Reviews", "route": ReviewsScreen},
    {"title": "Notifications", "route": NotiTab},
    {"title": "Settings", "route": SettingsScreen},
    {"title": "Help", "route": HelpScreen},
    {"title": "Privacy", "route": WebScreen},
    {"title": "Log out", "route": null},
  ];

  setRoute({
    Type route,
    BuildContext context,
    Function callback,
  }) async {
    try {
      if (identical(route, ProfileScreen)) {
        Get.to(() => ProfileScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, ReviewsScreen)) {
        Get.to(() => ReviewsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, SettingsScreen)) {
        Get.to(() => SettingsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, HelpScreen)) {
        Get.to(() => HelpScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, WebScreen)) {
        Get.to(() => WebScreen(
              title: "Privacy",
              url: Server.PRIVACY_URL,
            )).then((value) {
          callback(route);
        });
      }
    } catch (e) {}
  }
}
