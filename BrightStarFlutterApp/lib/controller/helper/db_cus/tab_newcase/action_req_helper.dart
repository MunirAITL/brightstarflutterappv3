import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/UserNotesCfg.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/api/db_cus/new_case/CaseDigitalSignByCustomerURLhelper.dart';
import 'package:aitl/controller/api/db_cus/new_case/CaseReviewAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/new_case/ClientAgreementURLhelper.dart';
import 'package:aitl/controller/api/db_cus/tab_more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/usercases/UserCaseAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/action_req/DashBoardListType.dart';
import 'package:aitl/model/json/db_cus/action_req/GetMortgageCaseInfoLocTaskidUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseRecomendationInfos.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotesModel.dart';
import 'package:aitl/view/widgets/btn/BtnOutlineBadge.dart';
import 'package:aitl/view/widgets/dialog/PrivacyPoliceAcceptDialog.dart';
import 'package:aitl/view/widgets/dialog/TaskDetailsDialog.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class ActionReqHelper {
  var isMobileVerified = false.obs;
  var isEmailVerified = false.obs;
  var isPrivacyAccepted = false.obs;
  var isEIDVerified = false.obs;

  wsActionReqAPIcall(
      {context,
      Function(List<DashBoardListType>, List<dynamic>,
              List<MortgageCaseRecomendationInfos>, List<MortgageCaseInfos>)
          callback}) async {
    await loadBadgeAPI4Verify(context);

    final List<DashBoardListType> list = [];
    List<dynamic> listUserNotesModel = [];
    List<MortgageCaseRecomendationInfos> caseReviewModelList = [];
    List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList = [];
    await privacyPolicyFetchAPI(context);
    await userNotesAPI(context, (lst, listUserNotesModel2) {
      listUserNotesModel = listUserNotesModel2;
      for (var l in lst) list.add(l);
    });
    await caseReviewAPI(context,
        (lst, caseReviewModelList2, caseMortgageAgreementReviewInfosList2) {
      caseReviewModelList = caseReviewModelList2;
      caseMortgageAgreementReviewInfosList =
          caseMortgageAgreementReviewInfosList2;
      for (var l in lst) list.add(l);
    });
    print(list);
    await updateUserBadgesAPI(context, (lst) {
      for (var l in lst) list.add(l);
    });
    print(list);
    print(listUserNotesModel);
    print(caseReviewModelList);
    print(caseMortgageAgreementReviewInfosList);
    callback(list, listUserNotesModel, caseReviewModelList,
        caseMortgageAgreementReviewInfosList);
    listUserNotesModel = null;
    caseReviewModelList = null;
    caseMortgageAgreementReviewInfosList = null;
  }

  loadBadgeAPI4Verify(BuildContext context) async {
    await BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) async {
          if (model != null) {
            if (model.success) {
              final listBadgeModel = model.responseData.userBadges;
              for (final mod in listBadgeModel) {
                if (mod.type == "Mobile" && mod.isVerified) {
                  isMobileVerified.value = true;
                } else if (mod.type == "Email" && mod.isVerified) {
                  isEmailVerified.value = true;
                } else if (mod.type == "CustomerPrivacy" && mod.isVerified) {
                  isPrivacyAccepted.value = true;
                } else if (mod.type == "ElectricId" && mod.isVerified) {
                  isEIDVerified.value = true;
                }
              }
            }
          }
        });
  }

  privacyPolicyFetchAPI(context) async {
    var value = await PrefMgr.shared.getPrefStr("Accepted");
    if (value == null) {
      if (userData.userModel.isCustomerPrivacy == 0 ||
          userData.userModel.isCustomerPrivacy == 907) {
        await PrivacyPolicyAPIMgr().wsOnLoad(
          context: context,
          callback: (model) {
            if (model != null) {
              try {
                if (model.success) {
                  final termsPrivacyNoticeSetupsList =
                      model.responseData.termsPrivacyNoticeSetupsList;
                  for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                      in termsPrivacyNoticeSetupsList) {
                    if (termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Privacy Notice") {
                      Get.dialog(
                        PrivacyPolicyDialog(termsPrivacyPoliceSetups.webUrl),
                        barrierDismissible: false,
                        barrierColor: Colors.transparent,
                        transitionDuration: Duration(milliseconds: 400),
                      );
                      break;
                    }
                  }
                } else {}
              } catch (e) {}
            }
          },
        );
      }
    }
  }

  updateUserBadgesAPI(
      context, Function(List<DashBoardListType>) callback) async {
    await BadgeAPIMgr().wsGetUserBadge(
      context: context,
      userId: userData.userModel.id,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final List<UserBadgeModel> listUserBadgeModel =
                  model.responseData.userBadges;
              bool emailExitStatus;
              bool eIDExitStatus;
              for (UserBadgeModel userBaseModel in listUserBadgeModel) {
                if (userBaseModel.type == "Email" &&
                    userBaseModel.isVerified == true) {
                  emailExitStatus = true;
                }
                if (userBaseModel.type == "Email" &&
                    userBaseModel.isVerified == false) {
                  emailExitStatus = false;
                }
                if (userBaseModel.type == "ElectricId" &&
                    userBaseModel.isVerified == true) {
                  eIDExitStatus = true;
                }
                if (userBaseModel.type == "ElectricId" &&
                    userBaseModel.isVerified == false) {
                  eIDExitStatus = false;
                }
              }
              if (emailExitStatus == null || emailExitStatus == false) {
                if (!isEmailVerified.value)
                  listUserList.add(new DashBoardListType(type: "email"));
              }
              if (eIDExitStatus == null || eIDExitStatus == false) {
                if (!isEIDVerified.value)
                  listUserList.add(new DashBoardListType(type: "eid"));
              }
              callback(listUserList);
            } else {}
          } catch (e) {}
        } else {}
      },
    );
  }

  caseReviewAPI(
      context,
      Function(
              List<DashBoardListType>,
              List<MortgageCaseRecomendationInfos> caseDocumentReviewModelList,
              List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList)
          callback) async {
    await CaseReviewAPIMgr().wsOnLoad(
      context: context,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final caseReviewModelList =
                  model.responseData.caseDocumentReviewModelList;
              final caseMortgageAgreementReviewInfosList =
                  model.responseData.caseMortgageAgreementReviewInfosList;

              for (MortgageCaseRecomendationInfos info in caseReviewModelList) {
                listUserList.add(new DashBoardListType(
                    type: "caseReviewModelList",
                    taskID: info.taskId.toString()));
              }

              for (MortgageCaseInfos info2
                  in caseMortgageAgreementReviewInfosList) {
                listUserList.add(new DashBoardListType(
                    type: "caseMortgageAgreementReviewInfosList",
                    taskID: info2.taskId.toString()));
              }

              callback(listUserList, caseReviewModelList,
                  caseMortgageAgreementReviewInfosList);
            }
          } catch (e) {}
        }
      },
    );
  }

  userNotesAPI(context,
      Function(List<DashBoardListType>, List<dynamic>) callback) async {
    await UserCaseAPIMgr().wsUserNotesAPI(
      context: context,
      status: UserNotesCfg.PENDING,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final listUserNotesModel = model.responseData.userNotes;
              if (listUserNotesModel.length > 0) {
                for (UserNotesModel userNotesModel in listUserNotesModel) {
                  listUserList.add(new DashBoardListType(
                      type: "UserNotes",
                      taskID: userNotesModel.entityId.toString(),
                      title: userNotesModel.title,
                      initiatorName: userNotesModel.initiatorName,
                      comments: userNotesModel.comments));
                }
                callback(listUserList, listUserNotesModel);
              }
            }
          } catch (e) {}
        }
      },
    );
  }

  drawActionRequiredItems({
    BuildContext context,
    List<DashBoardListType> listUserList,
    List<dynamic> listUserNotesModel,
    List<MortgageCaseRecomendationInfos> caseReviewModelList,
    List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
    Function(Map<String, dynamic>) callbackUserBadges,
    Function(UserNotesModel) callbackUserNotes,
    Function callbackReload,
    Function callbackRefresh, //  only use setstate
    Function(List<DashBoardListType> listUserList) callbackMoreAR,
  }) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: Container(
            width: width,
            child: Column(
              children: [
                listUserList != null
                    ? ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        itemCount: listUserList.length,
                        itemBuilder: (context, index) {
                          if (listUserList[index].type == "email") {
                            return Obx(() => Container(
                                  color: Colors.transparent,
                                  width: width,
                                  child: drawListTile(
                                    isEmailVerified.value,
                                    Text(
                                      "Email verification",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15),
                                    ),
                                    () {
                                      callSendEmailAPi(callback: (val) async {
                                        await callbackUserBadges(
                                            {"email": val});
                                        isEmailVerified.value = true;
                                        //await loadBadgeAPI4Verify(context);
                                      });
                                    },
                                  ),
                                ));
                          } else if (listUserList[index].type == "eid") {
                            return Obx(() => Container(
                                  color: Colors.transparent,
                                  width: width,
                                  child: drawListTile(
                                    isEIDVerified.value,
                                    Text(
                                      "E-ID verification",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15),
                                    ),
                                    () async {
                                      await callbackUserBadges({"eid": true});
                                      //isEIDVerified.value = true;
                                      //await loadBadgeAPI4Verify(context);
                                    },
                                  ),
                                ));
                          } else if (listUserList[index].type ==
                              "caseReviewModelList") {
                            return Container(
                              color: Colors.transparent,
                              width: width,
                              child: drawListTile(
                                false,
                                RichText(
                                  text: new TextSpan(
                                    style: new TextStyle(
                                      fontSize: MyTheme.txtSize - .1,
                                      color: Colors.black,
                                    ),
                                    children: <TextSpan>[
                                      new TextSpan(
                                          text: 'Case no:',
                                          style: new TextStyle(
                                              fontSize: ResponsiveFlutter.of(
                                                      context)
                                                  .fontSize(
                                                      MyTheme.txtSize - .1))),
                                      new TextSpan(
                                          text:
                                              ' ${listUserList[index].taskID},',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blueAccent,
                                              fontSize: ResponsiveFlutter.of(
                                                      context)
                                                  .fontSize(
                                                      MyTheme.txtSize - .1))),
                                      new TextSpan(
                                          text:
                                              ' You have NOT Signed the document yet.',
                                          style: new TextStyle(
                                              fontSize: ResponsiveFlutter.of(
                                                      context)
                                                  .fontSize(
                                                      MyTheme.txtSize - .1))),
                                      new TextSpan(
                                          text: 'Click here ',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blueAccent,
                                              fontSize: ResponsiveFlutter.of(
                                                      context)
                                                  .fontSize(
                                                      MyTheme.txtSize - .1))),
                                      new TextSpan(
                                          text: 'to review',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: ResponsiveFlutter.of(
                                                      context)
                                                  .fontSize(
                                                      MyTheme.txtSize - .1))),
                                    ],
                                  ),
                                ),
                                () {
                                  dialogCaseReview(
                                    context,
                                    listUserList,
                                    caseReviewModelList,
                                    index,
                                    () {
                                      callbackReload();
                                    },
                                  );
                                },
                              ),
                            );
                          } else if (listUserList[index].type ==
                              "caseMortgageAgreementReviewInfosList") {
                            return drawListTile(
                              false,
                              RichText(
                                text: new TextSpan(
                                  style: new TextStyle(
                                    fontSize: MyTheme.txtSize - .1,
                                    color: Colors.black,
                                  ),
                                  children: <TextSpan>[
                                    new TextSpan(
                                        text: 'Case no:',
                                        style: new TextStyle(
                                            color: Colors.blueAccent,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: ' ${listUserList[index].taskID},',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blueAccent,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text:
                                            ' You have NOT signed Client Agreement yet.',
                                        style: new TextStyle(
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: 'Click here ',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blueAccent,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: 'to review it',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                  ],
                                ),
                              ),
                              () {
                                dialogCaseMortgageAgreementReview(
                                  context,
                                  listUserList,
                                  caseMortgageAgreementReviewInfosList,
                                  index,
                                  () {
                                    callbackReload();
                                  },
                                );
                              },
                            );
                          } else if (listUserList[index].type == "UserNotes") {
                            return drawListTile(
                              false,
                              RichText(
                                text: new TextSpan(
                                  style: new TextStyle(
                                    fontSize: MyTheme.txtSize - .1,
                                    color: Colors.black,
                                  ),
                                  children: <TextSpan>[
                                    new TextSpan(
                                        text: 'Assign Task: ',
                                        style: new TextStyle(
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: '${listUserList[index].title}',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: ' by ',
                                        style: new TextStyle(
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text:
                                            '${listUserList[index].initiatorName}.',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: 'Click here',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blueAccent,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                    new TextSpan(
                                        text: ' to complete it.',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                  ],
                                ),
                              ),
                              () {
                                dialogUserNotes(
                                    context,
                                    listUserList,
                                    listUserNotesModel,
                                    index,
                                    callbackUserNotes);
                              },
                            );
                          } else {
                            return Container(height: 20);
                          }
                        },
                      )
                    : SizedBox(),
              ],
            )),
      ),
    );
  }

  drawListTile(bool isVerified, Widget wid, Function() callback) {
    return Card(
      elevation: 2,
      child: ListTile(
        minLeadingWidth: 0,
        title: wid,
        trailing: isVerified
            ? BtnOutlineBadge(
                txt: "✓ Verified",
                txtColor: Color(0xFF00C938),
                borderColor: Color(0xFF263F5D),
                callback: () {})
            : BtnOutlineBadge(
                txt: "Verify",
                txtColor: Color(0xFF263F5D),
                borderColor: Color(0xFF263F5D),
                callback: () async {
                  callback();
                }),
      ),
    );

    /*SizedBox(
        width: 20,
        height: 20,
        child: Container(
            margin: const EdgeInsets.all(0),
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: MyTheme.statusBarColor, width: 1),
              borderRadius: BorderRadius.circular(0),
            ),
            child: Checkbox(
                splashRadius: 20,
                fillColor: MaterialStateProperty.all(Colors.transparent),
                focusColor: MyTheme.statusBarColor,
                checkColor: Colors.black,
                value: dx.isCaseChecked[index],
                onChanged: (value) {
                  callback(value);
                })));*/
  }

  void callSendEmailAPi({BuildContext context, Function(bool) callback}) {
    BadgeAPIMgr().wsPostEmailBadge(
      context: context,
      callback: (BadgeEmailAPIModel model) {
        if (model.success) {
          callback(true);
        } else {
          callback(false);
        }
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
  }

  /*void showEmailConfirmDialog(BuildContext context, int index,
      Function(Map<String, dynamic>) callback) {
    confirmDialog(
        msg: "Press \'Yes\' to verify your email.",
        title: "Email Verification !",
        callbackYes: () {
          actReqControler.isCaseChecked[index] = true;
          callSendEmailAPi(callback: (val) {
            callback({"email": val});
          });
          //Navigator.of(context, rootNavigator: true).pop();
        },
        callbackNo: () {
          actReqControler.isCaseChecked[index] = false;
          callback({"email": null});
          //Navigator.of(context, rootNavigator: true).pop();
        },
        context: context);
  }*/

  /*void showEIDConfirmDialog(BuildContext context, int index,
      Function(Map<String, dynamic>) callback) {
    confirmDialog(
        msg: "Press \'Yes\' to verify E-ID.",
        title: "E-ID Verification !",
        callbackYes: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = true;
          callback({"eid": true});
        },
        callbackNo: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = false;
          callback({"eid": false});
        },
        context: context);
  }*/

  dialogCaseReview(
      context,
      listUserList,
      List<MortgageCaseRecomendationInfos> caseReviewModelList,
      index,
      callbackReload) async {
    //confirmDialog(
    //callbackYes: () async {
    //Navigator.of(context, rootNavigator: true).pop();
    var url = Server.MORTGAGE_CASEINFO_LOCATIONTASKUSERID_GET;
    url = url.replaceAll("#caseId#", listUserList[index].taskID);
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    await APIViewModel().req<GetMortgageCaseInfoLocTaskidUseridAPIModel>(
      context: context,
      url: url,
      reqType: ReqType.Get,
      callback: (model) async {
        if (model != null) {
          if (model.success) {
            final url = model.responseData.mortgageCaseDocumentInfo.documentUrl;
            if (url != "") {
              Get.to(
                      () => PDFDocumentPage(
                            title: "Case Document",
                            url: url,
                            isRightArrow: true,
                          ),
                      transition: Transition.rightToLeft,
                      duration: Duration(
                          milliseconds: AppConfig.pageAnimationMilliSecond))
                  .then((value) {
                String url = CaseDigitalSignByCustomerURLHelper.getUrl(
                    caseID: listUserList[index].taskID.toString());
                Get.to(
                  () => WebScreen(
                    caseID: listUserList[index].taskID.toString(),
                    title: "Case ${listUserList[index].taskID} Signature",
                    // url: Server.CaseDigitalSignByCustomer + caseReviewModelList[index].taskId.toString(),
                    url: url,
                  ),
                ).then((value) => callbackReload);
              });
            } else {}
          }
        }
      },
    );

    /*},
        callbackNo: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = false;
        },
        context: context,
        msg: "Do you want to signature now ?",
        title: "Case ${listUserList[index].taskID}");*/
  }

  dialogCaseMortgageAgreementReview(
      context,
      listUserList,
      List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
      index,
      callbackReload) {
    //confirmDialog(
    //callbackYes: () {
    //Navigator.of(context, rootNavigator: true).pop();
    String url = ClientAgreementURLHelper.getUrl(
        caseID: listUserList[index].taskID.toString());
    Get.to(
      () => WebScreen(
        caseID: listUserList[index].taskID.toString(),
        title: "Case ${listUserList[index].taskID} Agreement",
        url: url,
      ),
    ).then((value) => callbackReload);

    /*},
        callbackNo: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = false;
        },
        context: context,
        msg: "Do you want to Review the Agreement?",
        title: "Case ${listUserList[index].taskID}");*/
  }

  dialogUserNotes(
      context,
      List<DashBoardListType> listUserList,
      List<dynamic> listUserNotesModel,
      int index,
      Function(UserNotesModel) callbackListUserNotes) {
    taskDetailsDialog(
      caseID: listUserList[index].taskID.toString(),
      caseTitle: listUserList[index].title,
      caseDescription: listUserList[index].comments,
      caseAssignedBy: listUserList[index].initiatorName,
      callbackYes: () {
        //Navigator.of(context, rootNavigator: true).pop();
        final userNotesModel = NewCaseHelper().getUserNotesModelByTitle(
            listUserNotesModel: listUserNotesModel,
            title: listUserList[index].title);
        if (userNotesModel != null) {
          //  api call
          UserCaseAPIMgr().wsCaseNoteDone(
            context: context,
            userNoteModel: userNotesModel,
            callback: (model) {
              if (model != null) {
                if (model.success) {
                  callbackListUserNotes(model.responseData.userNote);
                }
              }
            },
          );
        }
      },
      callbackNo: () {
        //Navigator.of(context, rootNavigator: true).pop();
      },
      context: context,
      msg:
          "For Completing the task, please click the \'Yes\' button otherwise click the \'No\' button",
      title: "Task Details",
    );
  }
}
