import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotesModel.dart';

class NewCaseHelper {
  getUrl({pageStart, pageCount, status}) {
    var url = Server.NEWCASE_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#status#", status);
    url = url.replaceAll("#UserCompanyId#", userData.userModel.userCompanyID.toString());
    return url;
  }

  getCreateCaseIconByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map["url"];
        }
      }
    } catch (e) {}
    return null;
  }

  getCaseByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map;
        }
      }
    } catch (e) {}
    return null;
  }

  getUserNotesModelByTitle({listUserNotesModel, String title}) {
    for (UserNotesModel userNotesModel in listUserNotesModel) {
      if (userNotesModel.title == title) {
        return userNotesModel;
      }
    }
    return null;
  }
}
