import 'dart:developer';

import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyHelper.dart';
import 'package:flutter/material.dart';
import 'dart:developer' as dev;

class PrivacyPolicyAPIMgr {
  static final PrivacyPolicyAPIMgr _shared = PrivacyPolicyAPIMgr._internal();

  factory PrivacyPolicyAPIMgr() {
    return _shared;
  }

  PrivacyPolicyAPIMgr._internal();

  wsOnLoad({
    BuildContext context,
    Function(PrivacyPolicyAPIModel) callback,
  }) async {
    try {
      final url = PrivacyPolicyHelper().getUrl();
      dev.log("PrivacyPolicyHelper= " + url);
      await NetworkMgr()
          .req<PrivacyPolicyAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      dev.log(e.toString());
    }
  }
}
