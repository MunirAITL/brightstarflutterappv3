import 'package:aitl/controller/helper/db_cus/tab_timeline/MessageTypeURlHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/MessageTypeAdvisorAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';

class MessageTypeAPIMgr with Mixin {
  static final MessageTypeAPIMgr _shared = MessageTypeAPIMgr._internal();

  factory MessageTypeAPIMgr() {
    return _shared;
  }

  MessageTypeAPIMgr._internal();

  wsOnMessageTypeLoad({
    BuildContext context,
    int adviserOrIntroducerId,
    Function(MessageTypeAdvisorAPIModel) callback,
  }) async {
    try {
      final url = MessageTypeURIHelper()
          .getUrl(adviserOrIntroducerId: adviserOrIntroducerId);
      myLog("Message Type url = " + url);
      await NetworkMgr()
          .req<MessageTypeAdvisorAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
