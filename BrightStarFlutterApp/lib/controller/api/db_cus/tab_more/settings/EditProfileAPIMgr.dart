import 'dart:convert';

import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';
import 'package:json_string/json_string.dart';

class EditProfileAPIMgr with Mixin {
  static final EditProfileAPIMgr _shared = EditProfileAPIMgr._internal();

  factory EditProfileAPIMgr() {
    return _shared;
  }

  EditProfileAPIMgr._internal();

  wsUpdateProfileAPI({
    BuildContext context,
    dynamic param,
    Function(UserProfileAPIModel) callback,
  }) async {
    final jsonString = JsonString(json.encode(param));
    myLog(jsonString.source);
    await NetworkMgr()
        .req<UserProfileAPIModel, Null>(
      context: context,
      url: Server.EDIT_PROFILE_URL,
      reqType: ReqType.Put,
      param: param,
    )
        .then((model) async {
      callback(model);
    });
  }

  wsUpdateProfileAPI2({
    BuildContext context,
    dynamic param,
    bool isPost,
    Function(UserProfileAPIModel) callback,
  }) async {
    await NetworkMgr()
        .req<UserProfileAPIModel, Null>(
      context: context,
      url: isPost
          ? Server.REG_USER_CASE_APPLICANT_LEAD_URL
          : Server.EDIT_PROFILE_URL,
      reqType: isPost ? ReqType.Post : ReqType.Put,
      param: param,
    )
        .then((model) async {
      callback(model);
    });
  }

  wsDeactivateProfileAPI({
    BuildContext context,
    String reason,
    Function(DeactivateProfileAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<DeactivateProfileAPIModel, Null>(
        context: context,
        url: Server.DEACTIVATE_PROFILE_URL,
        param: {'id': userData.userModel.id, 'CancelReason': reason},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
