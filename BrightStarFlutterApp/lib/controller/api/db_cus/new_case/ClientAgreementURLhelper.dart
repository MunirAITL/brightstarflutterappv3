import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class ClientAgreementURLHelper{



  static getUrl({String caseID}) {
    var url = Server.CLIENTAGREEMENTURL;
    url = url.replaceAll("#CaseID#", caseID.toString());

    return url;
  }


}