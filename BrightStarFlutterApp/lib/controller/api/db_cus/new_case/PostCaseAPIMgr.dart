import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/PostCaseAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';

import '../../../../model/json/db_intr/createlead/PostCaseAPIModel2.dart';

class PostCaseAPIMgr with Mixin {
  static final PostCaseAPIMgr _shared = PostCaseAPIMgr._internal();

  factory PostCaseAPIMgr() {
    return _shared;
  }

  PostCaseAPIMgr._internal();

  wsOnPostCase({
    BuildContext context,
    dynamic param,
    Function(PostCaseAPIModel) callback,
  }) async {
    try {
      myLog(param);
      await NetworkMgr()
          .req<PostCaseAPIModel, Null>(
        context: context,
        url: Server.POSTCASE_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  wsOnPostCase2({
    BuildContext context,
    dynamic param,
    Function(PostCaseAPIModel2) callback,
  }) async {
    try {
      myLog(param);
      await NetworkMgr()
          .req<PostCaseAPIModel2, Null>(
        context: context,
        url: Server.POSTCASE_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
