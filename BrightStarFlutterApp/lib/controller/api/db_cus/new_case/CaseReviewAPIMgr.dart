import 'dart:developer';
import 'package:aitl/controller/helper/db_cus/tab_newcase/CaseReviewHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseReviewAPIModel.dart';
import 'package:flutter/material.dart';
import 'dart:developer' as dev;

class CaseReviewAPIMgr {
  static final CaseReviewAPIMgr _shared = CaseReviewAPIMgr._internal();

  factory CaseReviewAPIMgr() {
    return _shared;
  }

  CaseReviewAPIMgr._internal();

  wsOnLoad({
    BuildContext context,
    Function(CaseReviewAPIModel) callback,
  }) async {
    try {
      final url = CaseReviewHelper().getUrl();
      dev.log("Case Review " + url);
      await NetworkMgr()
          .req<CaseReviewAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      dev.log(e.toString());
    }
  }
}
