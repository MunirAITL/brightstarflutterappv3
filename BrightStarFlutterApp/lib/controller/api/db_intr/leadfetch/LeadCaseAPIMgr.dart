import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/helper/db_intr/leadfetch/GetLeadHelperURL.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_intr/leadfetch/LeadecaseApiModel.dart';
import 'package:flutter/cupertino.dart';

import '../../../../config/ServerIntr.dart';

class LeadCaseAPIMgr with Mixin {
  static final LeadCaseAPIMgr _shared = LeadCaseAPIMgr._internal();

  factory LeadCaseAPIMgr() {
    return _shared;
  }

  LeadCaseAPIMgr._internal();

  getLeadCase({
    BuildContext context,
    Map<String, dynamic> param,
    Function(LeadCaseApiModel) callback,
  }) async {
    try {
      var url = ServerIntr.LEAD_LIST_GET_URL;
      param.forEach((key, value) {
        url += (key + "=" + value + "&");
      });
      url = url.substring(0, url.length - 1);
      print(url);
      //url =
      //"https://app.mortgage-magic.co.uk/api/resolution/get/getleadinfodatabyusercompanyid?UserCompanyId=1003&Criteria=1&AssigneeId=0&Status=&IsSpecificDate=&FromDateTime=03-Mar-2022%2021:04&ToDateTime=05-Mar-2022%2021:04&InitiatorId=0&Title=&Stage=&LeadStatus=&Page=1&Count=30&SearchText=&OrderBy=Task%20Due%20Date&ResolutionType=All&DateTimeSelectOption=2%20Days";
      await NetworkMgr()
          .req<LeadCaseApiModel, Null>(
        context: context,
        url: url,
        isLoading: false,
        reqType: ReqType.Get,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      print("Lead case fetch error = $e}");
    }
  }
}
