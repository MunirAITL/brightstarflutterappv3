import 'dart:convert';
import 'dart:math';

import 'package:aitl/config/Server.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_intr/createlead/CreateLeadAPIModel.dart';
import 'package:aitl/model/json/db_intr/createlead/CreateLeadParamHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:intl/intl.dart';
import 'package:json_string/json_string.dart';

import '../../../../model/json/db_intr/createlead/ChangeStageAPIModel.dart';
import '../../../../model/json/db_intr/createlead/PostLeadGenAPIModel.dart';
import '../../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../../model/json/db_intr/leadfetch/LeadecaseApiModel.dart';

class CreateLeadApiMgr with Mixin {
  static final CreateLeadApiMgr _shared = CreateLeadApiMgr._internal();

  factory CreateLeadApiMgr() {
    return _shared;
  }

  CreateLeadApiMgr._internal();

  createLeadAPI({
    BuildContext context,
    String titleText,
    int initiatorId,
    String firstName,
    String middleName,
    String lastName,
    String mobileNumber,
    String emailAddress,
    String dateOfBirth,
    int referenceId,
    int negotiatorId,
    String phoneNumber,
    String leadNote,
    Function(CreateLeadAPIModel) callback,
  }) async {
    try {
      CreateLeadParamHelper createLeadParamHelper = new CreateLeadParamHelper(
          id: 0,
          status: 101,
          title: titleText,
          description: "",
          remarks: "",
          initiatorId: initiatorId,
          serviceDate:
              DateFormat('dd-MM-yyyy').format(DateTime.now()).toString(),
          resolutionType: "Lead From Introducer",
          parentId: 0,
          assigneeId: 0,
          assigneeName: "",
          userCompanyId: userData.userModel.userCompanyID,
          leadStatus: "New",
          firstName: firstName,
          middleName: middleName,
          lastName: lastName,
          mobileNumber: mobileNumber,
          email: "",
          dateofBirth: dateOfBirth,
          referenceId: referenceId,
          stage: "Added",
          probability: 0,
          titleOthers: "",
          estimatedEarning: 0,
          isLockAutoCall: 0,
          supportAdminId: 0,
          userNoteEntityId: 0,
          userNoteEntityName: "",
          negotiatorId: negotiatorId,
          phoneNumber: phoneNumber,
          emailAddress: emailAddress,
          leadNote: leadNote);

      final jsonString =
          JsonString(json.encode(createLeadParamHelper.toJson()));
      myLog(jsonString.source);

      await NetworkMgr()
          .req<CreateLeadAPIModel, Null>(
        context: context,
        url: ServerIntr.POST_RESOLUTION_URL,
        reqType: ReqType.Post,
        param: createLeadParamHelper.toJson(),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  changeStage({
    BuildContext context,
    String leadStatus,
    String noteType,
    String note,
    ResolutionModel resolutions,
    Function(ChangeStageAPIModel) callback,
  }) async {
    final param = {
      "LeadStatus": leadStatus,
      "NoteType": noteType,
      "Note": note.trim(),
      "Stage": resolutions.stage,
    };
    await NetworkMgr()
        .req<ChangeStageAPIModel, Null>(
      context: context,
      url: ServerIntr.POST_LEAD_GEN_URL,
      reqType: ReqType.Post,
      param: param,
    )
        .then((model) async {
      callback(model);
    });
  }

  postLeadGenerator({
    BuildContext context,
    String caseType,
    String stage,
    String leadStatus,
    String title,
    String fName,
    String mName = "",
    String lName,
    String dob,
    String addr,
    String email,
    String mobile,
    String leadRefId,
    double loanAmount,
    String leadNote = "",
    Function(PostLeadGenAPIModel) callback,
  }) async {
    final param = {
      "CaseType": caseType,
      "Stage": stage,
      "LeadStatus": leadStatus,
      "Title": title,
      "FirstName": fName,
      "MiddleName": mName,
      "LastName": lName,
      "DateOfBirth": dob,
      "Address": addr,
      "EmailAddress": email,
      "PhoneNumber": mobile,
      "LeadReferenceId": leadRefId,
      "LoanAmount": loanAmount,
      "LeadNote": leadNote,
    };
    await NetworkMgr()
        .req<PostLeadGenAPIModel, Null>(
      context: context,
      url: ServerIntr.POST_LEAD_GEN_URL,
      reqType: ReqType.Post,
      param: param,
    )
        .then((model) async {
      callback(model);
    });
  }
}
