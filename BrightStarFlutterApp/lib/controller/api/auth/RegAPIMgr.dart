import 'dart:convert';

import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/auth/RegHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/auth/RegAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:json_string/json_string.dart';

class RegAPIMgr with Mixin {
  static final RegAPIMgr _shared = RegAPIMgr._internal();

  factory RegAPIMgr() {
    return _shared;
  }

  RegAPIMgr._internal();

  wsRegAPI({
    BuildContext context,
    String email,
    String pwd,
    String fname,
    String lname,
    String mobile,
    String dob,
    String dobDD,
    String dobMM,
    String dobYY,
    String companyName,
    String countryCode,
    String cohort,
    Function(RegAPIModel) callback,
  }) async {
    try {
      final param = RegHelper().getParam(
        email: email,
        pwd: pwd,
        fname: fname,
        lname: lname,
        phone: mobile,
        dob: dob,
        dobDD: dobDD,
        dobMM: dobMM,
        dobYY: dobYY,
        companyName: companyName,
        countryCode: countryCode,
        cohort: cohort,
      );
      final jsonString = JsonString(json.encode(param));
      myLog(jsonString.source);
      await NetworkMgr()
          .req<RegAPIModel, Null>(
        context: context,
        url: Server.REG_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
