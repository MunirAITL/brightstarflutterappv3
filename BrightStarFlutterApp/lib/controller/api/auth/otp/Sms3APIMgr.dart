import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/auth/otp/LoginMobOtpPutHelper.dart';
import 'package:aitl/controller/helper/auth/otp/LoginRegOtpFBHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOTPModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';

class Sms3APIMgr with Mixin {
  static final Sms3APIMgr _shared = Sms3APIMgr._internal();

  factory Sms3APIMgr() {
    return _shared;
  }

  Sms3APIMgr._internal();

  wsLoginMobileOtpPutAPI({
    BuildContext context,
    String otpCode,
    var userID,
    String mobile,
    Function(MobileUserOtpPutAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<MobileUserOtpPutAPIModel, Null>(
        context: context,
        url: Server.LOGIN_MOBILE_OTP_PUT_URL,
        reqType: ReqType.Put,
        param: LoginMobOtpPutHelper().getParam(
          mobileNumber: mobile,
          otpCode: otpCode,
            userId:userID
        ),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  wsLoginMobileFBPostAPI({
    BuildContext context,
    MobileUserOTPModel mobileUserOTPModel,
    String otpCode,
    Function(LoginRegOtpFBAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginRegOtpFBAPIModel, Null>(
        context: context,
        url: Server.LOGIN_REG_OTP_FB_URL,
        param: LoginRegOtpFBHelper().getParam(
          mobileNumber: mobileUserOTPModel.mobileNumber,
          otpCode: otpCode,
        ),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
