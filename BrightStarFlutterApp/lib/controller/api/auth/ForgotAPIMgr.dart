import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/auth/ForgotAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';

class ForgotAPIMgr with Mixin {
  static final ForgotAPIMgr _shared = ForgotAPIMgr._internal();

  factory ForgotAPIMgr() {
    return _shared;
  }

  ForgotAPIMgr._internal();

  wsForgotAPI({
    BuildContext context,
    String email,
    Function(ForgotAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<ForgotAPIModel, Null>(
        context: context,
        url: Server.FORGOT_URL,
        param: {'email': email},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
