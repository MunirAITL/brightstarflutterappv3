import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/comp_acc/CompAccAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';

class CompAccAPIMgr with Mixin {
  static final CompAccAPIMgr _shared = CompAccAPIMgr._internal();

  factory CompAccAPIMgr() {
    return _shared;
  }

  CompAccAPIMgr._internal();

  wsGetCompAccAPI({
    BuildContext context,
    String userId,
    Function(CompAccAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<CompAccAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: Server.COMP_ACC_GET_URL.replaceAll("#userId#", userId),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
